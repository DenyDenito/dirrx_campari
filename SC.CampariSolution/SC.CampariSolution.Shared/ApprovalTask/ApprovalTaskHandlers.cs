﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.ApprovalTask;

namespace SC.CampariSolution
{
	partial class ApprovalTaskSharedHandlers
	{

		public virtual void BrandManagerSSCChanged(SC.CampariSolution.Shared.ApprovalTaskBrandManagerSSCChangedEventArgs e)
		{
			_obj.ApprovalRule = _obj.ApprovalRule;
			_obj.State.Controls.Control.Refresh();
		}

		public virtual void NetworkAccountantSCChanged(SC.CampariSolution.Shared.ApprovalTaskNetworkAccountantSCChangedEventArgs e)
		{
			_obj.ApprovalRule = _obj.ApprovalRule;
			_obj.State.Controls.Control.Refresh();
		}

		public virtual void ResponsibleManagerSCChanged(SC.CampariSolution.Shared.ApprovalTaskResponsibleManagerSCChangedEventArgs e)
		{
			_obj.ApprovalRule = _obj.ApprovalRule;
			_obj.State.Controls.Control.Refresh();
		}

		public override void ApprovalRuleChanged(Sungero.Docflow.Shared.ApprovalTaskApprovalRuleChangedEventArgs e)
		{			
			_obj.CounterpartySC = null;
			var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
			
			//Если финансовый документ
			var accounting = Sungero.Docflow.AccountingDocumentBases.As(document);
			if (accounting != null)
				_obj.CounterpartySC = SC.CampariSolution.Companies.As(accounting.Counterparty);
			
			//Если договорной документ
			var contract = Sungero.Docflow.ContractualDocumentBases.As(document);
			if (contract != null)
				_obj.CounterpartySC = SC.CampariSolution.Companies.As(contract.Counterparty);
			
			var Rule = e.NewValue;
			if (Rule != null && e.OldValue != e.NewValue)
			{
				_obj.ResponsibleManagerSC = null;
				_obj.NetworkAccountantSC = null;
				_obj.BrandManagerSSC = null;
				
				_obj.State.Properties.NetworkAccountantSC.IsVisible = false;
				_obj.State.Properties.BrandManagerSSC.IsVisible = false;
				_obj.State.Properties.ResponsibleManagerSC.IsVisible = false;
				
				
				foreach (var stage in Rule.Stages)
				{
					var stage_sc = SC.CampariSolution.ApprovalStages.As(stage.Stage);
					if (stage_sc.ApprovalRoles.Count > 0) //для блоков с параллельными участниками
					{
						foreach (var role in stage_sc.ApprovalRoles)
						{
							if (role.ApprovalRole.Type == SC.Business.RolesCampari.Type.NetworkAccount && _obj.CounterpartySC != null)
							{
								_obj.NetworkAccountantSC = _obj.CounterpartySC.NetworkAccountantSC;
								_obj.State.Properties.NetworkAccountantSC.IsVisible = true;
								
							}
							if (role.ApprovalRole.Type == SC.Business.RolesCampari.Type.BrandManager && _obj.CounterpartySC != null)
							{
								_obj.BrandManagerSSC = _obj.CounterpartySC.BrandManagerSC;
								_obj.State.Properties.BrandManagerSSC.IsVisible = true;
								
							}
							if (role.ApprovalRole.Type == SC.Business.RolesCampari.Type.ResponsibleMana && _obj.CounterpartySC != null)
							{
								_obj.ResponsibleManagerSC = _obj.CounterpartySC.ResponsibleManagerSC;
								_obj.State.Properties.ResponsibleManagerSC.IsVisible = true;
								
							}
						}
					}
					else //для блоков с одним участников
					{
						if (stage_sc.ApprovalRole != null)
						{
							if (stage_sc.ApprovalRole.Type == SC.Business.RolesCampari.Type.NetworkAccount && _obj.CounterpartySC != null)
							{
								_obj.NetworkAccountantSC = _obj.CounterpartySC.NetworkAccountantSC;
								_obj.State.Properties.NetworkAccountantSC.IsVisible = true;
								
							}
							if (stage_sc.ApprovalRole.Type == SC.Business.RolesCampari.Type.BrandManager && _obj.CounterpartySC != null)
							{
								_obj.BrandManagerSSC = _obj.CounterpartySC.BrandManagerSC;
								_obj.State.Properties.BrandManagerSSC.IsVisible = true;
								
							}
							if (stage_sc.ApprovalRole.Type == SC.Business.RolesCampari.Type.ResponsibleMana && _obj.CounterpartySC != null)
							{
								_obj.ResponsibleManagerSC = _obj.CounterpartySC.ResponsibleManagerSC;
								_obj.State.Properties.ResponsibleManagerSC.IsVisible = true;
								
							}
						}
					}
					
				}
			}
			base.ApprovalRuleChanged(e);
		}

	}
}