﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.ApprovalStage;

namespace SC.CampariSolution.Shared
{
	partial class ApprovalStageFunctions
	{
		public override List<Enumeration?> GetPossibleRoles()
		{
			var baseRoles = base.GetPossibleRoles();	
//			if (_obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers ||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.SimpleAgr ||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Notice||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.CheckReturn||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Print||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Register||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Sending||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Sign||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.Execution||
//			    _obj.StageType == Sungero.Docflow.ApprovalStage.StageType.)
//			{
				baseRoles.Add(SC.Business.RolesCampari.Type.BrandManager);
				baseRoles.Add(SC.Business.RolesCampari.Type.ResponsibleMana);
				baseRoles.Add(SC.Business.RolesCampari.Type.NetworkAccount);
//			}
			return baseRoles;
		}

	}
}