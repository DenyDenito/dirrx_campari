﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.ApprovalStage;

namespace SC.CampariSolution
{
	partial class ApprovalStageSharedHandlers
	{

		public virtual void NoPurchasingDocumentSCChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
		{
			if (e.NewValue.HasValue && e.NewValue.Value)
			{
				_obj.NoBonusAgreementSC = false;
				_obj.State.Properties.NoBonusAgreementSC.IsEnabled = false;
			}
			else
			{
				_obj.State.Properties.NoBonusAgreementSC.IsEnabled = true;
			}
		}

		public virtual void NoBonusAgreementSCChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
		{
			if (e.NewValue.HasValue && e.NewValue.Value)
			{
				_obj.NoPurchasingDocumentSC = false;
				_obj.State.Properties.NoPurchasingDocumentSC.IsEnabled = false;
			}
			else
			{
				_obj.State.Properties.NoPurchasingDocumentSC.IsEnabled = true;
			}
		}

	}
}