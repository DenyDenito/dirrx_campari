﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.Docflow;
using Sungero.CoreEntities;
using SC.CampariSolution.ContractsApprovalRule;

namespace SC.CampariSolution.Server
{
  partial class ContractsApprovalRuleFunctions
  {
  			/// <summary>
		/// Получить дублирующие правила.
		/// </summary>
		/// <returns>Правила, конфликтующие с текущим.</returns>
		[Remote(IsPure = true), Public]
		public override List<IApprovalRuleBase> GetDoubleRules()
		{
			var conflictedRules = new List<IApprovalRuleBase>();
			
			return conflictedRules;
		}

  }
}