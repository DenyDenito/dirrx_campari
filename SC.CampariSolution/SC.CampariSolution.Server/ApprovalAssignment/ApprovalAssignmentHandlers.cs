﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.ApprovalAssignment;

namespace SC.CampariSolution
{
	partial class ApprovalAssignmentServerHandlers
	{

		public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
		{
//			if (_obj.Stage.StageType == Sungero.Docflow.ApprovalStage.StageType.Approvers)				
//			{
				string CS = check_contractSatment();
				string UT = check_universaltrasferdoc();
				string WB = check_waybill();
				string FD = check_financialdoc();
				string res = "";
				if (CS != "")
				{
					res += CS + "\n";
				}
				if (UT != "")
				{
					res += UT + "\n";
				}
				if (WB != "")
				{
					res += WB + "\n";
				}
				if (FD != "")
				{
					res += FD + "\n";
				}
				if (_obj.Result == SC.CampariSolution.ApprovalAssignment.Result.Approved  && !res.Equals(string.Empty))
				{
					e.AddError(res);
				}
//			}
			base.BeforeComplete(e);
		}
		
		public string check_contractSatment()
		{
			string result = "";
			var docs = _obj.AllAttachments.Where(x => SC.CampariSolution.ContractStatements.Is(x)) ;// || SC.CampariSolution.UniversalTransferDocuments.Is(x) || SC.CampariSolution.Waybills.Is(x) || SC.Business.FinancialDocuments.Is(x));
			if (docs.Count() > 0)
			{
				SC.CampariSolution.IApprovalStage stage =  SC.CampariSolution.ApprovalStages.As(_obj.Stage);
				foreach (SC.CampariSolution.IContractStatement doc in docs)
				{
					string text_doc = "У документа '" + doc.Name +"' не заполнены:" + "\n";
					bool check_doc = false;
					//№ Тип расхода
					if (stage.TypeOfFlowSC.HasValue && stage.TypeOfFlowSC.Value &&  doc.TypeOfFlowSC == null )
					{
						text_doc +=   " № Тип расхода,";
						check_doc = true;
					};
					//№ Бонус агримент
					if (stage.NoBonusAgreementSC.HasValue && stage.NoBonusAgreementSC.Value && doc.NoBonusAgreementSC == null)
					{
						text_doc +=   " № Бонус эгримент,";
						check_doc = true;
					};
					//№ FI документа
					if (stage.NoFIDocumentSC.HasValue && stage.NoFIDocumentSC.Value && doc.NoFIDocumentSC == null)
					{
						text_doc +=   " № FI документа,";
						check_doc = true;
					};
					//Код налога
					if (stage.TaxCodeSC.HasValue && stage.TaxCodeSC.Value && doc.TaxCodeSC == null)
					{
						text_doc +=   " Код налога,";
						check_doc = true;
					};
					//Текст
					if (stage.TextSC.HasValue && stage.TextSC.Value && doc.TextSC == null)
					{
						text_doc +=   " Текст,";
						check_doc = true;
					};
					//№ документа закупки
					if (stage.NoPurchasingDocumentSC.HasValue && stage.NoPurchasingDocumentSC.Value && doc.NoPurchasingDocumentSC == null)
					{
						text_doc +=   " № документа закупки,";
						check_doc = true;
					};
					//№ Линии
					if (stage.NoLineSC.HasValue && stage.NoLineSC.Value && doc.NoLineSC == null)
					{
						text_doc +=   " № Линии,";
						check_doc = true;
					};
					//Статус
					if (stage.StatusSC.HasValue && stage.StatusSC.Value && doc.StatusSC == null)
					{
						text_doc +=   " Статус,";
						check_doc = true;
					};
					//Реклама
					if (stage.AdvertisingSC.HasValue && stage.AdvertisingSC.Value && doc.AdvertisingSC == null)
					{
						text_doc +=   " Реклама,";
						check_doc = true;
					};
					//Дата отправки в SSC
					if (stage.DatetosscSC.HasValue &&  stage.DatetosscSC.Value && doc.DatetosscSC == null)
					{
						text_doc +=   " Дата отправки в SSC,";
						check_doc = true;
					};
					//ответственный
					if (stage.SenderSC.HasValue && stage.SenderSC.Value && doc.SenderSC == null)
					{
						text_doc +=   " Ответственный,";
						check_doc = true;
					};
					//№ поставщика в SAP
					if (stage.NoSAPVendorSC.HasValue && stage.NoSAPVendorSC.Value && doc.NoSAPVendorSC == null)
					{
						text_doc +=   " № поставщика в SAP,";
						check_doc = true;
					};
					if (check_doc == true)
					{
						result += text_doc.Substring(0,text_doc.Length-1) + "\n";
					}
				}
				
			}
			return result;
		}
		
		public string check_universaltrasferdoc()
		{
			string result = "";
			var docs = _obj.AllAttachments.Where(x => SC.CampariSolution.UniversalTransferDocuments.Is(x)) ;// || SC.CampariSolution.UniversalTransferDocuments.Is(x) || SC.CampariSolution.Waybills.Is(x) || SC.Business.FinancialDocuments.Is(x));
			if (docs.Count() > 0)
			{
				SC.CampariSolution.IApprovalStage stage =  SC.CampariSolution.ApprovalStages.As(_obj.Stage);
				foreach (SC.CampariSolution.IUniversalTransferDocument doc in docs)
				{
					string text_doc = "У документа '" + doc.Name +"' не заполнены:" + "\n";
					bool check_doc = false;
					//№ Тип расхода
					if (stage.TypeOfFlowSC.HasValue && stage.TypeOfFlowSC.Value &&  doc.TypeOfFlowSC == null )
					{
						text_doc +=   " № Тип расхода,";
						check_doc = true;
					};
					//№ Бонус агримент
					if (stage.NoBonusAgreementSC.HasValue && stage.NoBonusAgreementSC.Value && doc.NoBonusAgreementSC == null)
					{
						text_doc +=   " № Бонус эгримент,";
						check_doc = true;
					};
					//№ FI документа
					if (stage.NoFIDocumentSC.HasValue && stage.NoFIDocumentSC.Value && doc.NoFIDocumentSC == null)
					{
						text_doc +=   " № FI документа,";
						check_doc = true;
					};
					//Код налога
					if (stage.TaxCodeSC.HasValue && stage.TaxCodeSC.Value && doc.TaxCodeSC == null)
					{
						text_doc +=   " Код налога,";
						check_doc = true;
					};
					//Текст
					if (stage.TextSC.HasValue && stage.TextSC.Value && doc.TextSC == null)
					{
						text_doc +=   " Текст,";
						check_doc = true;
					};
					//№ документа закупки
					if (stage.NoPurchasingDocumentSC.HasValue && stage.NoPurchasingDocumentSC.Value && doc.NoPurchasingDocumentSC == null)
					{
						text_doc +=   " № документа закупки,";
						check_doc = true;
					};
					//№ Линии
					if (stage.NoLineSC.HasValue && stage.NoLineSC.Value && doc.NoLineSC == null)
					{
						text_doc +=   " № Линии,";
						check_doc = true;
					};
					//Статус
					if (stage.StatusSC.HasValue && stage.StatusSC.Value && doc.StatusSC == null)
					{
						text_doc +=   " Статус,";
						check_doc = true;
					};
					//Реклама
					if (stage.AdvertisingSC.HasValue && stage.AdvertisingSC.Value && doc.AdvertisingSC == null)
					{
						text_doc +=   " Реклама,";
						check_doc = true;
					};
					//Дата отправки в SSC
					if (stage.DatetosscSC.HasValue &&  stage.DatetosscSC.Value && doc.DatetosscSC == null)
					{
						text_doc +=   " Дата отправки в SSC,";
						check_doc = true;
					};
					//ответственный
					if (stage.SenderSC.HasValue && stage.SenderSC.Value && doc.SenderSC == null)
					{
						text_doc +=   " Ответственный,";
						check_doc = true;
					};
					//№ поставщика в SAP
					if (stage.NoSAPVendorSC.HasValue && stage.NoSAPVendorSC.Value && doc.NoSAPVendorSC == null)
					{
						text_doc +=   " № поставщика в SAP,";
						check_doc = true;
					};
					if (check_doc == true)
					{
						result += text_doc.Substring(0,text_doc.Length-1) + "\n";
					}
				}
				
			}
			return result;
		}
		
		public string check_waybill()
		{
			string result = "";
			var docs = _obj.AllAttachments.Where(x => SC.CampariSolution.Waybills.Is(x)) ;// || SC.CampariSolution.UniversalTransferDocuments.Is(x) || SC.CampariSolution.Waybills.Is(x) || SC.Business.FinancialDocuments.Is(x));
			if (docs.Count() > 0)
			{
				SC.CampariSolution.IApprovalStage stage =  SC.CampariSolution.ApprovalStages.As(_obj.Stage);
				foreach (SC.CampariSolution.IWaybill doc in docs)
				{
					string text_doc = "У документа '" + doc.Name +"' не заполнены:" + "\n";
					bool check_doc = false;
					//№ Тип расхода
					if (stage.TypeOfFlowSC.HasValue && stage.TypeOfFlowSC.Value &&  doc.TypeOfFlowSC == null )
					{
						text_doc +=   " № Тип расхода,";
						check_doc = true;
					};
					//№ Бонус агримент
					if (stage.NoBonusAgreementSC.HasValue && stage.NoBonusAgreementSC.Value && doc.NoBonusAgreementSC == null)
					{
						text_doc +=   " № Бонус эгримент,";
						check_doc = true;
					};
					//№ FI документа
					if (stage.NoFIDocumentSC.HasValue && stage.NoFIDocumentSC.Value && doc.NoFIDocumentSC == null)
					{
						text_doc +=   " № FI документа,";
						check_doc = true;
					};
					//Код налога
					if (stage.TaxCodeSC.HasValue && stage.TaxCodeSC.Value && doc.TaxCodeSC == null)
					{
						text_doc +=   " Код налога,";
						check_doc = true;
					};
					//Текст
					if (stage.TextSC.HasValue && stage.TextSC.Value && doc.TextSC == null)
					{
						text_doc +=   " Текст,";
						check_doc = true;
					};
					//№ документа закупки
					if (stage.NoPurchasingDocumentSC.HasValue && stage.NoPurchasingDocumentSC.Value && doc.NoPurchasingDocumentSC == null)
					{
						text_doc +=   " № документа закупки,";
						check_doc = true;
					};
					//№ Линии
					if (stage.NoLineSC.HasValue && stage.NoLineSC.Value && doc.NoLineSC == null)
					{
						text_doc +=   " № Линии,";
						check_doc = true;
					};
					//Статус
					if (stage.StatusSC.HasValue && stage.StatusSC.Value && doc.StatusSC == null)
					{
						text_doc +=   " Статус,";
						check_doc = true;
					};
					//Реклама
					if (stage.AdvertisingSC.HasValue && stage.AdvertisingSC.Value && doc.AdvertisingSC == null)
					{
						text_doc +=   " Реклама,";
						check_doc = true;
					};
					//Дата отправки в SSC
					if (stage.DatetosscSC.HasValue &&  stage.DatetosscSC.Value && doc.DatetosscSC == null)
					{
						text_doc +=   " Дата отправки в SSC,";
						check_doc = true;
					};
					//ответственный
					if (stage.SenderSC.HasValue && stage.SenderSC.Value && doc.SenderSC == null)
					{
						text_doc +=   " Ответственный,";
						check_doc = true;
					};
					//№ поставщика в SAP
					if (stage.NoSAPVendorSC.HasValue && stage.NoSAPVendorSC.Value && doc.NoSAPVendorSC == null)
					{
						text_doc +=   " № поставщика в SAP,";
						check_doc = true;
					};
					if (check_doc == true)
					{
						result += text_doc.Substring(0,text_doc.Length-1) + "\n";
					}
				}
				
			}
			return result;
		}
		
		public string check_financialdoc()
		{
			string result = "";
			var docs = _obj.AllAttachments.Where(x => SC.Business.FinancialDocuments.Is(x)) ;// || SC.CampariSolution.UniversalTransferDocuments.Is(x) || SC.CampariSolution.Waybills.Is(x) || SC.Business.FinancialDocuments.Is(x));
			if (docs.Count() > 0)
			{
				SC.CampariSolution.IApprovalStage stage =  SC.CampariSolution.ApprovalStages.As(_obj.Stage);
				foreach (SC.Business.IFinancialDocument doc in docs)
				{
					string text_doc = "У документа '" + doc.Name +"' не заполнены:" + "\n";
					bool check_doc = false;
					//№ Тип расхода
					if (stage.TypeOfFlowSC.HasValue && stage.TypeOfFlowSC.Value &&  doc.TypeOfFlowSC == null )
					{
						text_doc +=   " № Тип расхода,";
						check_doc = true;
					};
					//№ Бонус агримент
					if (stage.NoBonusAgreementSC.HasValue && stage.NoBonusAgreementSC.Value && doc.NoBonusAgreementSC == null)
					{
						text_doc +=   " № Бонус эгримент,";
						check_doc = true;
					};
					//№ FI документа
					if (stage.NoFIDocumentSC.HasValue && stage.NoFIDocumentSC.Value && doc.NoFIDocumentSC == null)
					{
						text_doc +=   " № FI документа,";
						check_doc = true;
					};
					//Код налога
					if (stage.TaxCodeSC.HasValue && stage.TaxCodeSC.Value && doc.TaxCodeSC == null)
					{
						text_doc +=   " Код налога,";
						check_doc = true;
					};
					//Текст
					if (stage.TextSC.HasValue && stage.TextSC.Value && doc.TextSC == null)
					{
						text_doc +=   " Текст,";
						check_doc = true;
					};
					//№ документа закупки
					if (stage.NoPurchasingDocumentSC.HasValue && stage.NoPurchasingDocumentSC.Value && doc.NoPurchasingDocumentSC == null)
					{
						text_doc +=   " № документа закупки,";
						check_doc = true;
					};
					//№ Линии
					if (stage.NoLineSC.HasValue && stage.NoLineSC.Value && doc.NoLineSC == null)
					{
						text_doc +=   " № Линии,";
						check_doc = true;
					};
					//Статус
					if (stage.StatusSC.HasValue && stage.StatusSC.Value && doc.StatusSC == null)
					{
						text_doc +=   " Статус,";
						check_doc = true;
					};
					//Реклама
					if (stage.AdvertisingSC.HasValue && stage.AdvertisingSC.Value && doc.AdvertisingSC == null)
					{
						text_doc +=   " Реклама,";
						check_doc = true;
					};
					//Дата отправки в SSC
					if (stage.DatetosscSC.HasValue &&  stage.DatetosscSC.Value && doc.DatetosscSC == null)
					{
						text_doc +=   " Дата отправки в SSC,";
						check_doc = true;
					};
					//ответственный
					if (stage.SenderSC.HasValue && stage.SenderSC.Value && doc.SenderSC == null)
					{
						text_doc +=   " Ответственный,";
						check_doc = true;
					};
					//№ поставщика в SAP
					if (stage.NoSAPVendorSC.HasValue && stage.NoSAPVendorSC.Value && doc.NoSAPVendorSC == null)
					{
						text_doc +=   " № поставщика в SAP,";
						check_doc = true;
					};
					if (check_doc == true)
					{
						result += text_doc.Substring(0,text_doc.Length-1) + "\n";
					}
				}
				
			}
			return result;
		}
		
	}

}