﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Docflow;
using SC.CampariSolution.ApprovalRule;

namespace SC.CampariSolution.Server
{
	partial class ApprovalRuleFunctions
	{

				/// <summary>
		/// Получить дублирующие правила.
		/// </summary>
		/// <returns>Правила, конфликтующие с текущим.</returns>
		[Remote(IsPure = true), Public]
		public override List<IApprovalRuleBase> GetDoubleRules()
		{
			var conflictedRules = new List<IApprovalRuleBase>();
			
			return conflictedRules;
		}

	}
}