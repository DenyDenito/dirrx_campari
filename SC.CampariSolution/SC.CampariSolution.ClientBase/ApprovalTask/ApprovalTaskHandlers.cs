﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.ApprovalTask;

namespace SC.CampariSolution
{
	partial class ApprovalTaskClientHandlers
	{

		public override void Showing(Sungero.Presentation.FormShowingEventArgs e)
		{
			base.Showing(e);
			
			if (_obj.State.IsInserted)
			{
				var document = _obj.DocumentGroup.OfficialDocuments.FirstOrDefault();
				var rulesCount = Sungero.Docflow.PublicFunctions.ApprovalRuleBase.Remote.GetAvailableRulesByDocument(document).Count();
				if (rulesCount > 1)
					_obj.ApprovalRule = null;
			}
		}

	}
}