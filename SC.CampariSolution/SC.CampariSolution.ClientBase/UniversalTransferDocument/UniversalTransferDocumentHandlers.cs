﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.CampariSolution.UniversalTransferDocument;

namespace SC.CampariSolution
{
	partial class UniversalTransferDocumentClientHandlers
	{

		public virtual void DatetosscSCValueInput(Sungero.Presentation.DateTimeValueInputEventArgs e)
		{
			if (e.NewValue.HasValue && Calendar.Today.AddDays(10) < e.NewValue.Value)
				e.AddError(e.Property, "Дата отправки SSC не может быть позднее 10 дней от текущей");
		}

	}
}