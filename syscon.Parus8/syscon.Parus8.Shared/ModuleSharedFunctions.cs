﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.Parus8.Shared
{
	public class ModuleFunctions
	{
		
		/// <summary>
		/// Проверка валидности RN
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static bool CheckRN(string RN)
		{
			decimal rn = 0;
			var result = false;
			
			if (Decimal.TryParse(RN, out rn))
				result = true;
			
			return result;
		}

	}
}