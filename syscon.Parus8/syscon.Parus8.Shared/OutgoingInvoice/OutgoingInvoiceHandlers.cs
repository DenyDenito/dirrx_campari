using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.OutgoingInvoice;

namespace syscon.Parus8
{
  partial class OutgoingInvoiceSharedHandlers
  {

    public virtual void DateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
    {
      
    }

    public virtual void NumberChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
    {

    }

  }
}