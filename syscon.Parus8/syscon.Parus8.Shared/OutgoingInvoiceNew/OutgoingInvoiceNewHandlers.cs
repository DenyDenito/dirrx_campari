using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.OutgoingInvoiceNew;

namespace syscon.Parus8
{
	partial class OutgoingInvoiceNewSharedHandlers
	{

		public virtual void ContractChanged(syscon.Parus8.Shared.OutgoingInvoiceNewContractChangedEventArgs e)
		{
			if (Equals(e.NewValue, e.OldValue))
				return;
			
			if (e.NewValue != null)
			{
				var contract = e.NewValue;
				_obj.Counterparty = contract.Counterparty;
				_obj.BusinessUnit = contract.BusinessUnit;
			}

			_obj.Relations.AddFromOrUpdate(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName, e.OldValue, e.NewValue);
		}

		public override void DateChanged(Sungero.Domain.Shared.DateTimePropertyChangedEventArgs e)
		{
			base.DateChanged(e);
			_obj.RegistrationDate = e.NewValue;
		}

		public override void NumberChanged(Sungero.Domain.Shared.StringPropertyChangedEventArgs e)
		{
			base.NumberChanged(e);
			_obj.RegistrationNumber = e.NewValue;
		}

		public override void CounterpartyChanged(Sungero.Docflow.Shared.AccountingDocumentBaseCounterpartyChangedEventArgs e)
		{
			base.CounterpartyChanged(e);
			
			FillName();
			
			// Очистить договор при изменении контрагента.
			if (_obj.Contract == null || Equals(e.NewValue, _obj.Contract.Counterparty))
				return;
			if (!Equals(e.NewValue, e.OldValue))
				_obj.Contract = null;
		}

	}
}