using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.OutgoingInvoiceNew;

namespace syscon.Parus8.Shared
{
	partial class OutgoingInvoiceNewFunctions
	{
		/// <summary>
		/// Изменение состояния документа для ненумеруемых документов.
		/// </summary>
		public override void SetLifeCycleState()
		{
			// Счет должен создаваться со статусом "Новый", если только это не смена типа устаревшего документа.
			if (_obj.LifeCycleState != Sungero.Docflow.OfficialDocument.LifeCycleState.Obsolete)
				_obj.LifeCycleState = Sungero.Docflow.OfficialDocument.LifeCycleState.Draft;
		}
		
		public override void ChangeRegistrationPaneVisibility(bool needShow, bool repeatRegister)
		{
			base.ChangeRegistrationPaneVisibility(needShow, repeatRegister);
			var notNumerable = _obj.DocumentKind != null && _obj.DocumentKind.NumberingType == Sungero.Docflow.DocumentKind.NumberingType.NotNumerable;
			var canRegister = _obj.AccessRights.CanRegister();
			var caseIsEnabled = notNumerable || !notNumerable && canRegister;
			// Может быть уже закрыто от редактирования, если документ зарегистрирован и в формате номера журнала
			// присутствует индекс файла.
			caseIsEnabled = caseIsEnabled && _obj.State.Properties.CaseFile.IsEnabled;
			
			_obj.State.Properties.InternalApprovalState.IsVisible = needShow;
			_obj.State.Properties.ExecutionState.IsVisible = false;
			_obj.State.Properties.ControlExecutionState.IsVisible = false;
			_obj.State.Properties.CaseFile.IsEnabled = caseIsEnabled;
			_obj.State.Properties.PlacedToCaseFileDate.IsEnabled = caseIsEnabled;
			_obj.State.Properties.Tracking.IsEnabled = true;
		}
		
		/// <summary>
		/// Заполнить имя.
		/// </summary>
		public override void FillName()
		{
			if (_obj != null &&
			    _obj.DocumentKind != null &&
			    !_obj.DocumentKind.GenerateDocumentName.Value &&
			    _obj.Name == Sungero.Docflow.OfficialDocuments.Resources.DocumentNameAutotext)
				_obj.Name = string.Empty;
			
			if (_obj.DocumentKind == null || !_obj.DocumentKind.GenerateDocumentName.Value)
				return;
			
			var name = string.Empty;
			
			/* Имя в формате:
        <Вид документа> №<номер> от <дата> от <контрагент> "<содержание>".
        
        В общем случае _obj.Number != _obj.RegistrationNumber.
        В общем случае _obj.Date != _obj.RegistrationDate.
			 */
			using (TenantInfo.Culture.SwitchTo())
			{
				if (_obj.Number != null)
					name += Sungero.Docflow.OfficialDocuments.Resources.Number + _obj.Number;
				
				if (_obj.Date != null)
					name += Sungero.Docflow.OfficialDocuments.Resources.DateFrom + _obj.Date.Value.ToString("d");

				if (_obj.Counterparty != null)
					name += " на " + _obj.Counterparty.DisplayValue;
				
				if (!string.IsNullOrWhiteSpace(_obj.Subject))
					name += " \"" + _obj.Subject + "\"";
			}
			
			if (string.IsNullOrWhiteSpace(name))
				name = Sungero.Docflow.OfficialDocuments.Resources.DocumentNameAutotext;
			else if (_obj.DocumentKind != null)
				name = _obj.DocumentKind.ShortName + name;
			
			name = Sungero.Docflow.PublicFunctions.Module.TrimSpecialSymbols(name);
			
			_obj.Name = Sungero.Docflow.PublicFunctions.OfficialDocument.AddClosingQuote(name, _obj);
			
		}

		/// <summary>
		/// Добавить связанные с входящим счетом документы в группу вложений.
		/// </summary>
		/// <param name="group">Группа вложений.</param>
		public override void AddRelatedDocumentsToAttachmentGroup(Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
		{
			// Получить бухгалтерские документы.
			var accountingDocuments = _obj.Relations.GetRelatedFrom(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName);
			
			var documentsToAdd = accountingDocuments.Where(d => !group.All.Contains(d)).ToList();
			foreach (var document in documentsToAdd)
				group.All.Add(document);
		}
		
		/// <summary>
		/// Удалить связанные с входящим счетом документы из группы вложений.
		/// </summary>
		/// <param name="group">Группа вложений.</param>
		public override void RemoveRelatedDocumentsFromAttachmentGroup(Sungero.Workflow.Interfaces.IWorkflowEntityAttachmentGroup group)
		{
			// Получить бухгалтерские документы.
			var accountingDocuments = _obj.Relations.GetRelatedFrom(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName);
			
			// Удалить документы.
			var documentsToRemove = accountingDocuments.Where(d => group.All.Contains(d)).ToList();
			foreach (var document in documentsToRemove)
				group.All.Remove(document);
		}
		
		public override void UpdateLifeCycle(Enumeration? registrationState,
		                                     Enumeration? approvalState,
		                                     Enumeration? counterpartyApprovalState)
		{
			// Не проверять статусы для пустых параметров.
			if (_obj == null || _obj.DocumentKind == null)
				return;
			
			var lifeCycleMustByActive = _obj.LifeCycleState == Sungero.Docflow.OfficialDocument.LifeCycleState.Draft &&
				approvalState == Sungero.Docflow.OfficialDocument.InternalApprovalState.Signed;
			
			if (lifeCycleMustByActive)
				_obj.LifeCycleState = Sungero.Docflow.OfficialDocument.LifeCycleState.Active;
		}
	}
}