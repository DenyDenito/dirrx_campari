﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.OutgoingInvoiceNew;

namespace syscon.Parus8
{
	partial class OutgoingInvoiceNewClientHandlers
	{

		public override void CurrencyValueInput(Sungero.Docflow.Client.AccountingDocumentBaseCurrencyValueInputEventArgs e)
		{
			base.CurrencyValueInput(e);
//			if (Functions.OutgoingInvoiceNew.HaveDuplicates(_obj, _obj.DocumentKind, _obj.Number, _obj.Date, _obj.TotalAmount, e.NewValue, _obj.Counterparty))
//				e.AddWarning(OutgoingInvoiceNew.Resources.DuplicateDetected,
//				             _obj.Info.Properties.DocumentKind,
//				             _obj.Info.Properties.Number,
//				             _obj.Info.Properties.Date,
//				             _obj.Info.Properties.TotalAmount,
//				             _obj.Info.Properties.Currency,
//				             _obj.Info.Properties.Counterparty);
		}

		public override void CounterpartyValueInput(Sungero.Docflow.Client.AccountingDocumentBaseCounterpartyValueInputEventArgs e)
		{
			base.CounterpartyValueInput(e);
//			if (Functions.OutgoingInvoiceNew.HaveDuplicates(_obj, _obj.DocumentKind, _obj.Number, _obj.Date, _obj.TotalAmount, _obj.Currency, e.NewValue))
//				e.AddWarning(OutgoingInvoiceNew.Resources.DuplicateDetected,
//				             _obj.Info.Properties.DocumentKind,
//				             _obj.Info.Properties.Number,
//				             _obj.Info.Properties.Date,
//				             _obj.Info.Properties.TotalAmount,
//				             _obj.Info.Properties.Currency,
//				             _obj.Info.Properties.Counterparty);
		}

		public override void DocumentKindValueInput(Sungero.Docflow.Client.OfficialDocumentDocumentKindValueInputEventArgs e)
		{
			base.DocumentKindValueInput(e);
//			if (Functions.OutgoingInvoiceNew.HaveDuplicates(_obj, e.NewValue, _obj.Number, _obj.Date, _obj.TotalAmount, _obj.Currency, _obj.Counterparty))
//				e.AddWarning(OutgoingInvoiceNew.Resources.DuplicateDetected,
//				             _obj.Info.Properties.DocumentKind,
//				             _obj.Info.Properties.Number,
//				             _obj.Info.Properties.Date,
//				             _obj.Info.Properties.TotalAmount,
//				             _obj.Info.Properties.Currency,
//				             _obj.Info.Properties.Counterparty);
		}

		public override void TotalAmountValueInput(Sungero.Presentation.DoubleValueInputEventArgs e)
		{
			if (e.NewValue <= 0)
				e.AddError(Sungero.Contracts.IncomingInvoices.Resources.TotalAmountMustBePositive);
			
			base.TotalAmountValueInput(e);
		}

	}
}