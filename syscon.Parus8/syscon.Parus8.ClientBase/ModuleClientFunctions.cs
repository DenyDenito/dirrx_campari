﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using System.IO;
using ProgressBarLibrary;
using Excel = Microsoft.Office.Interop.Excel;
using System.Text;

namespace syscon.Parus8.Client
{
	public class ModuleFunctions
	{

		/// <summary>
		/// 
		/// </summary>
//		public virtual void Function1()
//		{
//			var b = Dialogs.CreateInputDialog("ИД");
//			var a = b.AddInteger("ИД", true);
//
//			if (b.Show() == DialogButtons.Ok)
//				Dialogs.ShowMessage(Parus8.Functions.Module.Remote.TrySinc(a.Value.Value));
//		}
		
		/// <summary>
		/// 
		/// </summary>
		public virtual void ImportFormalizedDoc()
		{
			var dlg = Dialogs.CreateInputDialog("Выберите формализованный документ в формате XML");
//			var file = dlg.AddFileSelect("Файл", true);
			var file = dlg.AddString("Пусть", true, @"C:\ks\upd\DP_IZVPOL_2BM_2BM-4510000164-2013022202562104209890000000000_20190703_4bb275f7-e67d-4b40-b.xml");
			if (dlg.Show() == DialogButtons.Ok)
			{
				Sungero.FinancialArchive.PublicFunctions.Module.ImportFormalizedDocument(file.Value, true).Show();
			}
			
//			string tempPath = System.IO.Path.GetTempPath();
//			string fileName = string.Format(@"{0}{1}",tempPath,fileSelect.Value.Name);
//			File.WriteAllBytes(fileName, fileSelect.Value.Content);
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Test_Excel()
		{
			Excel.Application xls = new Excel.Application();
			//Отобразить Excel
			xls.Visible = true;
			xls.Interactive = false;

			//Количество листов в рабочей книге
			xls.SheetsInNewWorkbook = 2;
			//Добавить рабочую книгу
//			Excel.Workbook workBook = xls.Workbooks.Add(Type.Missing);
			//Отключить отображение окон с сообщениями
			xls.DisplayAlerts = false;
			//Получаем первый лист документа (счет начинается с 1)
			Excel.Worksheet sheet = (Excel.Worksheet)xls.Worksheets.get_Item(1);
			//Название листа (вкладки снизу)
			sheet.Name = "Отчет за 13.12.2017";
			//Пример заполнения ячеек
			for (int i = 1; i <= 9; i++)
			{
				for (int j = 1; j < 9; j++)
					sheet.Cells[i, j] = String.Format("Boom {0} {1}", i, j);
			}
			
			xls.Interactive = true;
			
//			var file = @"C:\DRX\1.xlsx";
//			Excel.Application xls = new Excel.Application();
//			xls.Visible = true
//				Excel.Workbook wb = xls.Workbooks.Open(file);
//			Excel.Worksheet sh = (Excel.Worksheet)xls.Worksheets.get_Item(1);
//			Excel.Range data= sh.UsedRange;
//			for (int i = 2; i < 200; i++)
//			{
//				if (data.Cells[i,2].ToString() != null)
//				{
//					var cat = SC.ContractCategories.Create();
//					cat.Name = data.Cells[i,1].ToString();
//					cat.MainCategorysyscon = data.Cells[i,2].ToString();
//					cat.Save();
//				}
//			}
			
//			syscon.Parus8.Functions.Module.Remote.ImportContractCatFromExcel();
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Sync_Organizations_RX()
		{
			if (Users.Current.IncludedIn(Sungero.CoreEntities.Roles.Administrators))
			{
				var cntLoad = 0;
				var cntError = 0;
				var cntAll = 0;
				
				var logsFolder = @"C:\DRX\logs\";

				if (!Directory.Exists(logsFolder))
					Directory.CreateDirectory(logsFolder);
				
				var listOrgRN = PublicFunctions.Module.Remote.GetChangedObjects_Parus8("AGNLIST");
				
				System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
				sw.Start();
				
				ProgressBarDRX progressBar = new ProgressBarDRX(listOrgRN.Count); //объект прогресс бар
				progressBar.Show();
				progressBar.Text("Подключение к базе...");
				
				foreach(var orgRN in listOrgRN)
				{
					try
					{
						Functions.Module.Remote.Sync_Organization_RX(orgRN);

						using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
						{
							file.WriteLine(string.Format("Организация c RN {0} загружена.", orgRN));
						}

						cntLoad++;
					}
					catch (Exception ex)
					{
						using (var file = File.AppendText(string.Format(logsFolder + "log_error_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
						{
							file.WriteLine(string.Format("Организация c RN {0} не загружена.\r\n{1}", orgRN, ex.Message));
						}

//								ListOrgRN.Remove(reader["RN"].ToString());
						cntError++;
					}
					cntAll++;
					
					progressBar.Text(string.Format("Загрузка организаций: {0}/{1}...\r\nКол-во ошибок: {2}.", cntLoad, listOrgRN.Count, cntError));
					progressBar.Next();
				}
				
				progressBar.Hide();
				progressBar = null;

				sw.Stop();
				
				//				RemoveChangedObject_Parus8(ListOrgRN);

				Sungero.Core.Dialogs.ShowMessage(string.Format("Загрузка завершена.\r\n"
				                                               + "---------------------------------------------------------\r\n"
				                                               + "Количество загруженных организаций: {0}\r\n"
				                                               + "Количество ошибок при загрузке: {1}\r\n"
				                                               + "---------------------------------------------------------\r\n"
				                                               + "Количество просмотренных организаций: {2}\r\n"
				                                               + "---------------------------------------------------------\r\n"
				                                               + "Время выполнения: {3}",
				                                               cntLoad, cntError, cntAll,
				                                               sw.Elapsed.ToString(@"d\.hh\:mm\:ss")));
				
				sw = null;
			}
			else
			{
				Dialogs.NotifyMessage("Необходимо быть администратором системы");
			}
		}
		
		/// <summary>
		/// Синхронизация договоров (массовая)
		/// </summary>
		public virtual void Sync_Contracts_RX()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listOrgRN = PublicFunctions.Module.Remote.GetChangedObjects_Parus8("Contracts");
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			ProgressBarDRX progressBar = new ProgressBarDRX(listOrgRN.Count); //объект прогресс бар
			progressBar.Show();
			progressBar.Text("Подключение к базе...");
			
			foreach(var orgRN in listOrgRN)
			{
				try
				{
					Functions.Module.Remote.Sync_Organization_RX(orgRN);

					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Организация c RN {0} загружена.", orgRN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Организация c RN {0} не загружена.\r\n{1}", orgRN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
				
				progressBar.Text(string.Format("Загрузка организаций: {0}/{1}...\r\nКол-во ошибок: {2}.", cntLoad, listOrgRN.Count, cntError));
				progressBar.Next();
			}
			
			progressBar.Hide();
			progressBar = null;

			sw.Stop();
			
			//				RemoveChangedObject_Parus8(ListOrgRN);

			Sungero.Core.Dialogs.ShowMessage(string.Format("Загрузка завершена.\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Количество загруженных организаций: {0}\r\n"
			                                               + "Количество ошибок при загрузке: {1}\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Количество просмотренных организаций: {2}\r\n"
			                                               + "---------------------------------------------------------\r\n"
			                                               + "Время выполнения: {3}",
			                                               cntLoad, cntError, cntAll,
			                                               sw.Elapsed.ToString(@"d\.hh\:mm\:ss")));
			
			sw = null;
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Test_Create_Contract()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("Запрос параметров");
			var p1 = dlg.AddString("RN",true,"101");
			var p2 = dlg.AddString("Категория",true,"Договор Аренды");
			var p3 = dlg.AddString("Рег Номер",true,"123");
			var p4 = dlg.AddString("Дата документа",true,"06.12.2009");
			var p5 = dlg.AddString("Примечание",true,"договор");
			var p6 = dlg.AddString("Сумма",true,"1250,50");
			var p7 = dlg.AddString("Дата с",true,"01.01.2018");
			var p8 = dlg.AddString("Дата по",true,"31.12.2018");
			var p9 = dlg.AddString("RN орг",true,"3");
			var p10 = dlg.AddString("RN наш орг",true,"2");
			var p11 = dlg.AddString("ИНН подписанта",true,"482608013231");
			var p12 = dlg.AddString("ИНН ответственного",true,"482608013231");
			var p13 = dlg.AddString("Ссылка на файл",true,"C:\\123.txt");
			var p14 = dlg.AddString("Открыть карточку?",true, "true");
			if (dlg.Show() == DialogButtons.Ok)
			{
				Create_Contract_RX(p1.Value,p2.Value,p3.Value,p4.Value,p5.Value, p6.Value, p7.Value, p8.Value, p9.Value, p10.Value, p11.Value, p12.Value, p13.Value,p14.Value);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void Test_Create_ORG()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("Запрос параметров");
			var p1 = dlg.AddString("RN",true,"2");
			var p2 = dlg.AddString("ORG",true,"Org");
			var p3 = dlg.AddString("INN",true,"7811417616");
			var p4 = dlg.AddString("KPP",true,"781301001");
			var p5 = dlg.AddBoolean("Открыть карточку?",true);
			if (dlg.Show() == DialogButtons.Ok)
			{
				Create_ORG_RX(p1.Value,p2.Value,p3.Value,p4.Value,"true");
			}
		}
		
		public static void OpenREf()
		{
			var dlg = Sungero.Core.Dialogs.CreateInputDialog("hallo");
			dlg.Show();
		}
		
		[Hyperlink]
		public static void Create_ORG_RX(string RN, string Name, string INN, string KPP, string NeedOpenCard)
		{
//			Encoding utf8 = Encoding.GetEncoding("utf-8");
//			Encoding win1251 = Encoding.GetEncoding("windows-1251");
//			byte[] utf8Bytes = win1251.GetBytes(Name);
//			byte[] win1251Bytes = Encoding.Convert(win1251, utf8, utf8Bytes);
//			var name2 = win1251.GetString(win1251Bytes);
			
//			Encoding utf8 = Encoding.UTF8;
//			Encoding win1251 = Encoding.GetEncoding("Windows-1251");
//			byte[] utf8Bytes = utf8.GetBytes(Name);
//			byte[] win1251Bytes = Encoding.Convert(utf8, win1251, utf8Bytes);
//			var name2 = win1251.GetString(win1251Bytes);
			
			Encoding utf8 = Encoding.UTF8;
			Encoding win1251 = Encoding.GetEncoding("windows-1251");
			byte[] utf8Bytes = win1251.GetBytes(Name);
			//byte[] win1251Bytes = Encoding.Convert(win1251, utf8, utf8Bytes);
			var name2 = utf8.GetString(utf8Bytes);
			
			var flag = false;

			var org = syscon.Parus8.Functions.Module.Remote.Sync_Organization_RX(RN, name2, INN, KPP);
			if (org != null && bool.TryParse(NeedOpenCard, out flag) && flag)
			{
				org.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_ORG_RX(string RN, string NeedOpenCard)
		{
			var needOpenCard = false;

			var org = syscon.Parus8.Functions.Module.Remote.Sync_Organization_RX(RN);
			if (org != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				org.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_OURORG_RX(string RN, string NeedOpenCard)
		{
			var needOpenCard = false;

			var org = syscon.Parus8.Functions.Module.Remote.Sync_OurOrganization_RX(RN);
			if (org != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				org.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_Contract_RX(string RN, string Category, string RegNum, string DateDoc, string Note, string Sum, string DateFrom, string DateTo, string ORG, string OurOrg, string Signer, string Responsible, string File, string NeedOpenCard)
		{
			var needOpenCard = false;
			var contract = Functions.Module.Remote.Sync_Contract_RX(RN, Category, RegNum, DateDoc, Note, Sum, DateFrom, DateTo, ORG, OurOrg, Signer, Responsible, File);
			if (contract != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				contract.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_Contract_RX(string RN, string File, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var contract = Functions.Module.Remote.Sync_Contract_RX(RN, File);
			if (contract != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				contract.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_OutgoingTaxInvoice_RX(string RN, string File, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var outgoingTaxInvoice = Functions.Module.Remote.Sync_OutgoingTaxInvoice_RX(RN, File);
			if (outgoingTaxInvoice != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				outgoingTaxInvoice.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_OutgoingInvoice_RX(string RN, string File, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var outgoingInvoice = Functions.Module.Remote.Sync_OutgoingInvoice_RX(RN, File);
			if (outgoingInvoice != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				outgoingInvoice.Show();
			}
		}
		
		[Hyperlink]
		public static void Create_UniversalTransferDocument_RX(string RN, string File, string NeedOpenCard)
		{
//			var needOpenCard = false;
//
//			var universalTransferDocument = Functions.Module.Remote.Sync_UniversalTransferDocument_RX(RN, File);
//			if (universalTransferDocument != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
//			{
//				universalTransferDocument.Show();
//			}
		}
		
		[Hyperlink]
		public static void Create_AccountingDocument_RX(string RN, string File, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var contractStatementAndWaybill = Functions.Module.Remote.Sync_AccountingDocument_RX(RN, File);
			if (contractStatementAndWaybill != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				contractStatementAndWaybill.Show();
			}
		}
		
		[Hyperlink]
		public static void Open_RX(string RN)
		{
			var entity = Functions.Module.Remote.GetEntityByRN(RN);
			if (entity != null)
				entity.Show();
			else
				Dialogs.ShowMessage("Документ не найден");
		}
		
		[Hyperlink]
		public virtual void FormalizedDocument(string RN, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var xml = Functions.Module.Remote.DownloadFormalizedDocumentParus8(RN, true);
			
			var ru = System.Text.Encoding.GetEncoding(1251);
			var content = ru.GetBytes(xml);
			var array = Sungero.Docflow.Structures.Module.ByteArray.Create(content);
			var formDoc = Sungero.FinancialArchive.PublicFunctions.Module.ImportFormalizedDocument(array, true);
			
//			var formDoc = Sungero.FinancialArchive.PublicFunctions.Module.ImportFormalizedDocument(filePath, true);
			
			if (formDoc != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				formDoc.Show();
			}
			
//			string tempPath = System.IO.Path.GetTempPath();
//			string fileName = string.Format(@"{0}{1}",tempPath,fileSelect.Value.Name);
//			File.WriteAllBytes(fileName, fileSelect.Value.Content);
		}
		
		[Hyperlink]
		public virtual void Create_FormDoc_RX(string xml, string NeedOpenCard)
		{
			var needOpenCard = false;
			
			var byteArray = Encoding.UTF8.GetBytes(xml);
			var array = Sungero.Docflow.Structures.Module.ByteArray.Create(byteArray);
//			var filePath = Functions.Module.Remote.DownloadFormalizedFileParus8(xml);
			
			var formDoc = Sungero.FinancialArchive.PublicFunctions.Module.ImportFormalizedDocument(array, true);
			
			if (formDoc != null && bool.TryParse(NeedOpenCard, out needOpenCard) && needOpenCard)
			{
				formDoc.Show();
			}
			
//			string tempPath = System.IO.Path.GetTempPath();
//			string fileName = string.Format(@"{0}{1}",tempPath,fileSelect.Value.Name);
//			File.WriteAllBytes(fileName, fileSelect.Value.Content);
		}

	}
}