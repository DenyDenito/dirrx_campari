﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace syscon.Parus8.Server
{
	public class ModuleJobs
	{

		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncOrganizationsToParus8Job()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;
			decimal rn = 0;
			
			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			var companies = Functions.Module.GetChangedOrganizations_RX();
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_p8_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				
				foreach (var company in companies)
				{
					try
					{
						var message = Functions.Module.Sync_Organization_Parus8(company, false, connection);
						
						if (Decimal.TryParse(message, out rn))
							file.WriteLine(string.Format("{3} Организация {1} ({0}) загружена c RN {2}", company.Id, company.Name, message, Calendar.Now.ToString()));
						else
							file.WriteLine(string.Format("{3} Возникла ошибка на стороне Парус 8, организация {1} ({0}):\r\n{2}", company.Id, company.Name, message, Calendar.Now.ToString()));
						
						cntLoad++;
					}
					catch (Exception ex)
					{
						file.WriteLine(string.Format("{3} Ошибка при запуске синхронизации организации {1} ({0}):\r\n{2}", company.Id, company.Name, ex.Message, Calendar.Now.ToString()));
						
						cntError++;
					}
					cntAll++;
				}
				
				sw.Stop();
				
				var result = string.Format("{4} Загрузка завершена.\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Количество загруженных организаций: {0}\r\n"
				                           + "Количество ошибок при загрузке: {1}\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Количество просмотренных организаций: {2}\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Время выполнения: {3}",
				                           cntLoad, cntError, cntAll,
				                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"),
				                           Calendar.Now.ToString());
				
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();
			
			sw = null;
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncOutgoingInvoiceJob()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;
			
			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listRN = Functions.Module.GetChangedObjects_Parus8("PaymentAccounts", false, connection);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			foreach(var RN in listRN)
			{
				try
				{
					Functions.Module.Sync_OutgoingInvoice_RX(RN, "true", false, connection);
					
					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_oi_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} загружен.", RN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_oi_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} не загружен.\r\n{1}", RN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
			}

			sw.Stop();
			
			var result = string.Format("Загрузка завершена.\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество загруженных документов: {0}\r\n"
			                           + "Количество ошибок при загрузке: {1}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество просмотренных документов: {2}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Время выполнения: {3}",
			                           cntLoad, cntError, cntAll,
			                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_error_oi_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();

			sw = null;
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncAccountingDocumentsJob()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listRN = Functions.Module.GetChangedObjects_Parus8("GoodsTransInvoicesToConsumers", false, connection);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			foreach(var RN in listRN)
			{
				try
				{
					Functions.Module.Sync_AccountingDocument_RX(RN, "true", false, connection);

					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_ad_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} загружен.", RN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_ad_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} не загружен.\r\n{1}", RN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
			}

			sw.Stop();
			
			var result = string.Format("Загрузка завершена.\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество загруженных документов: {0}\r\n"
			                           + "Количество ошибок при загрузке: {1}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество просмотренных документов: {2}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Время выполнения: {3}",
			                           cntLoad, cntError, cntAll,
			                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_error_ad_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();
			
			sw = null;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncContractsJob()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listRN = Functions.Module.GetChangedObjects_Parus8("Contracts", false, connection);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			foreach(var RN in listRN)
			{
				try
				{
					Functions.Module.Sync_Contract_RX(RN, "true", false, connection);

					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_ct_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} загружен.", RN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_ct_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} не загружен.\r\n{1}", RN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
			}

			sw.Stop();
			
			var result = string.Format("Загрузка завершена.\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество загруженных документов: {0}\r\n"
			                           + "Количество ошибок при загрузке: {1}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество просмотренных документов: {2}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Время выполнения: {3}",
			                           cntLoad, cntError, cntAll,
			                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_error_ct_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();
			
			sw = null;
		}
		
		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncOutgoingTaxInvoicesJob()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listRN = Functions.Module.GetChangedObjects_Parus8("AccountFactOutput", false, connection);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			foreach(var RN in listRN)
			{
				try
				{
					Functions.Module.Sync_OutgoingTaxInvoice_RX(RN, "true", false, connection);

					using (var file = File.AppendText(string.Format(logsFolder + "log_migration_oti_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} загружен.", RN));
					}

					cntLoad++;
				}
				catch (Exception ex)
				{
					using (var file = File.AppendText(string.Format(logsFolder + "log_error_oti_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
					{
						file.WriteLine(string.Format("Документ c RN {0} не загружен.\r\n{1}", RN, ex.Message));
					}

//								ListOrgRN.Remove(reader["RN"].ToString());
					cntError++;
				}
				cntAll++;
			}

			sw.Stop();
			
			var result = string.Format("Загрузка завершена.\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество загруженных документов: {0}\r\n"
			                           + "Количество ошибок при загрузке: {1}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Количество просмотренных документов: {2}\r\n"
			                           + "---------------------------------------------------------\r\n"
			                           + "Время выполнения: {3}",
			                           cntLoad, cntError, cntAll,
			                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_error_oti_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();
			
			sw = null;
		}

		/// <summary>
		/// 
		/// </summary>
		public virtual void SyncOrganizationsJob()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var connection = new OracleConnection();
			connection.ConnectionString = syscon.Parus8.Functions.Module.GetSettingString("ConnectionString");
			connection.Open();
			
			Functions.Module.StartSession_Parus8(connection);
			
			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);
			
			var listOrgRN = Functions.Module.GetChangedObjects_Parus8("AGNLIST", false, connection);
			
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			
			using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
			{
				
				foreach(var orgRN in listOrgRN)
				{
					try
					{
						var company = Functions.Module.Sync_Organization_RX(orgRN, false, connection);

						file.WriteLine(string.Format("{3} Организация c RN {0} загружена {1} ({2}).", orgRN, company.Name, company.Id, Calendar.Now.ToString()));
						
						cntLoad++;
					}
					catch (Exception ex)
					{
						file.WriteLine(string.Format("{2} Организация c RN {0} не загружена:\r\n{1}", orgRN, ex.Message, Calendar.Now.ToString()));

						cntError++;
					}
					cntAll++;
				}

				sw.Stop();
				
				var result = string.Format("{4} Загрузка завершена.\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Количество загруженных организаций: {0}\r\n"
				                           + "Количество ошибок при загрузке: {1}\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Количество просмотренных организаций: {2}\r\n"
				                           + "---------------------------------------------------------\r\n"
				                           + "Время выполнения: {3}",
				                           cntLoad, cntError, cntAll,
				                           sw.Elapsed.ToString(@"d\.hh\:mm\:ss"),
				                           Calendar.Now.ToString());
				
				file.WriteLine(result);
			}
			
			Functions.Module.StopSession_Parus8(connection);
			
			OracleConnection.ClearPool(connection);
			connection.Close();
			connection.Dispose();
			
			sw = null;
		}
		
	}
}