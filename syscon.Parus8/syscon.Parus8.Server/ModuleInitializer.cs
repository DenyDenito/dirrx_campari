﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;
using SC.Settings;

namespace syscon.Parus8.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			CreateSetting("ConnectionString", "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.67)(PORT=1521)))(CONNECT_DATA=(SID=kse)));User Id=directum;Password=$$$$$$aP@;Pooling=False;");
			CreateSetting("LOGON_WIN", "PKG_SESSION.LOGON_WIN('Designer', 'ELEV', NULL)");
			
			CreateApprovalRole(syscon.Parus8.Role.Type.DivisionHead, "Распорядитель бюджетных средств");
			CreateApprovalRole(syscon.Parus8.Role.Type.HeadSecurity, "Руководитель службы безопасности");
			CreateApprovalRole(syscon.Parus8.Role.Type.Accountant, "Бухгалтер");
			CreateApprovalRole(syscon.Parus8.Role.Type.LegalDep, "Юридический отдел");
			CreateApprovalRole(syscon.Parus8.Role.Type.PersonnelDel, "Отдел кадров");
			CreateApprovalRole(syscon.Parus8.Role.Type.FinHead, "Руководитель финансовой дирекции");
			CreateApprovalRole(syscon.Parus8.Role.Type.RtnController, "Контролер возврата документов");

			GrantRightsOnFolder();
			
			CreateDocumentTypes();
			CreateDocumentKinds();
		}
		
		/// <summary>
		/// Создание настройки
		/// </summary>
		/// <param name="name">Наименование</param>
		/// <param name="paramValue">Значение</param>
		/// <param name="Note">Примечание</param>
		public static void CreateSetting(string name, string paramValue, string Note = "")
		{
			if (Settings.GetAll().Where(e => e.Name == name).Count() == 0)
			{
				var setting = Settings.Create();
				
				setting.Name = name;
				setting.ParamValue = paramValue;
				setting.Note = Note;
				
				setting.Save();
				
				InitializationLogger.DebugFormat("Создана настройка: {0}", name);
			}			
		}

		/// <summary>
		/// 
		/// </summary>
		public void CreateApprovalRole(Enumeration roleType, string description)
		{
			var role = Roles.GetAll().Where(r => Equals(r.Type,roleType)).FirstOrDefault();
			// Проверяет наличие роли.
			if (role == null)
			{
				role = Roles.Create();
			}
			
			role.Type = roleType;
			role.Description = description;
			role.Save();
		}

		/// <summary>
		/// Функция инициализации для выдачи прав на вычисляемую папку.
		/// </summary>
		public static void GrantRightsOnFolder()
		{
//			var user = Sungero.CoreEntities.Recipients.Get(95);
//			syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.RevokeAll(user);
//			syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.Grant(user, DefaultAccessRightsTypes.Read);
//			syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.Save();
			
			try
			{
				var allUsers = Sungero.CoreEntities.Roles.AllUsers;
				syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.RevokeAll(allUsers);
				syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.Save();
			}
			catch
			{
			}
			
			try
			{
				var administartors = Sungero.CoreEntities.Roles.Administrators;
				syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.Grant(administartors, DefaultAccessRightsTypes.FullAccess);
				syscon.Parus8.SpecialFolders.ModuleRoles.AccessRights.Save();
			}
			catch
			{
			}

			InitializationLogger.Debug("Выданы права на вычисляемую папку 'Роли модуля'");
		}

		/// <summary>
		/// Создание типа документа "Договор закупки" в служебном справочнике "Типы документов".
		/// </summary>
		public static void CreateDocumentTypes()
		{
			Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Финансовый документ выставленный", OutgoingInvoiceNew.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Outgoing, true);
			Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Финансовый документ", ActToWriteOff.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
//			Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Тестовый тип договорных", Document2.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
			
//			InitializationLogger.Debug("Создан тип документов для выставленных Финансовых документов");
		}
		
		/// <summary>
		/// Создание видов документов для договора закупки.
		/// </summary>
		public static void CreateDocumentKinds()
		{
			try
			{
				Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentKind("Исходящий счет на оплату", "Исх. счет на оплату",
				                                                                        Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
				                                                                        Sungero.Docflow.DocumentType.DocumentFlow.Outgoing,
				                                                                        true, false, OutgoingInvoiceNew.ClassTypeGuid,
				                                                                        new Sungero.Domain.Shared.IActionInfo[] { Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval },
				                                                                        Constants.Module.OutgoingInvoiceKind);
				InitializationLogger.Debug("Создан вид документов для Исходящих счетов на оплату");
			}
			catch
			{
				InitializationLogger.Debug("Ошибка при создании вида документов для Исходящих счетов на оплату");
			}
			
			try
			{
				Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentKind("Акт на списание", "Акт на списание",
				                                                                        Sungero.Docflow.DocumentKind.NumberingType.NotNumerable,
				                                                                        Sungero.Docflow.DocumentType.DocumentFlow.Contracts,
				                                                                        true, false, ActToWriteOff.ClassTypeGuid,
				                                                                        new Sungero.Domain.Shared.IActionInfo[] { Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval },
				                                                                        Constants.Module.ActToWriteOffKind);
				InitializationLogger.Debug("Созданы виды документов для Актов на списание");
			}
			catch
			{
				InitializationLogger.Debug("Ошибка при создании вида документов для Актов на списание");
			}
			
//			try
//			{
//				Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentKind("Исходящий тест", "Исходящий тест",
//				                                                                        Sungero.Docflow.DocumentKind.NumberingType.Numerable,
//				                                                                        Sungero.Docflow.DocumentType.DocumentFlow.Contracts,
//				                                                                        true, false, Document2.ClassTypeGuid,
//				                                                                        new Sungero.Domain.Shared.IActionInfo[] { Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval },
//				                                                                        Constants.Module.OtherTestKind);
//				InitializationLogger.Debug("Созданы виды документов для Исходящий тест");
//			}
//			catch
//			{
//				InitializationLogger.Debug("Ошибка при создании вида документов для Исходящий тест");
//			}
		}
		
	}
}
