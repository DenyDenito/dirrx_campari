﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.RoleCalcKS;

namespace syscon.Parus8
{
  partial class RoleCalcKSServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
       _obj.Name = _obj.RoleKS.DisplayValue + " - " + _obj.RoleRecKS.DisplayValue;
    }
  }

}