﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.Parus8.OutgoingInvoiceNew;

namespace syscon.Parus8
{

	partial class OutgoingInvoiceNewCreatingFromServerHandler
	{

		public override void CreatingFrom(Sungero.Domain.CreatingFromEventArgs e)
		{
			base.CreatingFrom(e);
			
			if (_source.Contract == null || !_source.Contract.AccessRights.CanRead())
				e.Without(_info.Properties.Contract);
		}
	}

	partial class OutgoingInvoiceNewServerHandlers
	{

		public override void Created(Sungero.Domain.CreatedEventArgs e)
		{
			base.Created(e);
			
			if (_obj.State.IsInserted && _obj.Contract != null)
				_obj.Relations.AddFrom(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName, _obj.Contract);
			
			_obj.ResponsibleEmployee = null;
		}

		public override void BeforeSaveHistory(Sungero.Content.DocumentHistoryEventArgs e)
		{
			var isCreateAction = e.Action == Sungero.CoreEntities.History.Action.Create;
			var isChangeTypeAction = e.Action == Sungero.CoreEntities.History.Action.ChangeType;
			
			if (!isCreateAction && !isChangeTypeAction)
				base.BeforeSaveHistory(e);
			else
			{
				// Изменение суммы или валюты.
				var sumWasChanged = _obj.State.Properties.TotalAmount.IsChanged || (_obj.State.Properties.Currency.IsChanged && _obj.TotalAmount.HasValue);
				if (sumWasChanged)
				{
					// Локализация для операции в ресурсах OfficialDocument.
					var operation = new Enumeration(Sungero.Docflow.Constants.OfficialDocument.Operation.TotalAmountChange);
					var operationDetailed = _obj.TotalAmount.HasValue ? operation : new Enumeration(Sungero.Docflow.Constants.OfficialDocument.Operation.TotalAmountClear);
					var currency = (_obj.Currency == null) ? string.Empty : _obj.Currency.AlphaCode;
					var comment = _obj.TotalAmount.HasValue ? string.Join("|", _obj.TotalAmount.Value, currency) : string.Empty;
					
					e.Write(operation, operationDetailed, comment);
				}
			}
		}

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
			base.BeforeSave(e);
			
			if (_obj.Contract != null && _obj.Contract.AccessRights.CanRead() && !_obj.Relations.GetRelatedFrom(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName).Contains(_obj.Contract))
				_obj.Relations.AddFromOrUpdate(Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName, _obj.State.Properties.Contract.OriginalValue, _obj.Contract);
		}
	}

	partial class OutgoingInvoiceNewContractPropertyFilteringServerHandler<T>
	{

		public virtual IQueryable<T> ContractFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
		{
			if (_obj.Counterparty != null)
				query = query.Where(c => Equals(c.Counterparty, _obj.Counterparty));
			
			query = query.Where(c => !Equals(c.LifeCycleState, Sungero.Contracts.ContractBase.LifeCycleState.Obsolete));
			
			return query;
		}
	}

}