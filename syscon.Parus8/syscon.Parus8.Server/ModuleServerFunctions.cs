﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Contracts;
using Sungero.Company;
using Sungero.Parties;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Sungero.FinancialArchive;
using System.Data;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Sungero.Domain.Shared;
using System.Text.RegularExpressions;
using Sungero.Docflow;
using SC.Settings;

namespace syscon.Parus8.Server
{
	public class ModuleFunctions
	{
		
		/// <summary>
		/// Достаем строку коннекта
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static string GetSettingString(string paramName)
		{
			var settingString = string.Empty;
			var defaultString = string.Empty;
			
			if (paramName == "ConnectionString")
				defaultString = Constants.Module.ConnectionString;
			
			if (paramName == "LOGON_WIN")
				defaultString = Constants.Module.LOGON_WIN;
			
			AccessRights.AllowRead(
				() =>
				{
					var setting = Settings.GetAll().FirstOrDefault(e => e.Name == paramName);
					settingString = setting != null ? setting.ParamValue : defaultString;
				});
			
			return settingString;
		}
		
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote]
		public static string DownloadFormalizedDocumentParus8(string RN, bool InSession)
		{
			return DownloadFormalizedDocumentParus8(RN, InSession, null);
		}
		
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static string DownloadFormalizedDocumentParus8(string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("DownloadFormalizedDocumentParus8: {0} from RN {1}", "Start function synchronization", RN);
			string fileName = string.Empty;
			string stringXML = string.Empty;
			string ident = string.Empty;
//			string datetimeNow = Calendar.Now.ToString("yyyy_MM_dd_HH_mm_ss");
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			var filesFolder = @"C:\DRX\BLOB\";
			
			if (!Directory.Exists(filesFolder))
				Directory.CreateDirectory(filesFolder);
			
			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandType = System.Data.CommandType.StoredProcedure;
				command.CommandText = "UP_RX_EXTRACT_FORMDOC_RX";
				
				command.Parameters.Add("nDOCUMENT", RN);
				command.Parameters.Add("sFILENAME", OracleDbType.Varchar2, System.Data.ParameterDirection.Output);
				command.Parameters.Add("cXML", OracleDbType.Clob, System.Data.ParameterDirection.Output);
				
//				command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
				
//				try
//				{
				command.ExecuteNonQuery();
				
				fileName = command.Parameters["sFILENAME"].Value.ToString();
				var clob = (OracleClob) command.Parameters["cXML"].Value;
				stringXML = clob != null ? clob.Value : string.Empty;
				
				Logger.DebugFormat("DownloadFormalizedDocumentParus8: Document Name {0}", fileName);
				Logger.DebugFormat("DownloadFormalizedDocumentParus8: Document Body {0}", stringXML);
				
//					ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
//
				////						Document.Note = RN + " | " + ident;
				////						Document.Save();
//
//					if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
//					{
//						var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
//						var regex = new Regex(@".*Не найден.* для обновления RN=.*");
//						if (regex.IsMatch(errMsg))
//							Sync_ExternalEntityLink(Document.Id, type == "Акт" ? "26fd4043-40d2-457c-ad0e-35cefa05d8dd" : "167d0859-1aa3-4d7b-934a-bfc4b0a8c7a1", RN, type, true);
//						RN = errMsg;
//					}
//					else
//						Sync_ExternalEntityLink(Document.Id, type == "Акт" ? "26fd4043-40d2-457c-ad0e-35cefa05d8dd" : "167d0859-1aa3-4d7b-934a-bfc4b0a8c7a1", RN, type);
//				}
//				catch (Exception ex)
//				{
//					RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
//				}
				
//				var reader = command.ExecuteReader();
//				if (reader.HasRows)
//				{
//					while (reader.Read())
//					{
//						filePath = string.Format(@"{3}{0}({1}){2}", datetimeNow, RN, reader["sFileName"].ToString(), filesFolder);
//						var bDate = reader["bDate"];
//
//						using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
//						{
//							byte[] bytes = new byte[0];
//							bytes = (byte[])bDate;
//							int ArraySize = new int();
//							ArraySize = bytes.GetUpperBound(0);
//							fs.Write(bytes, 0, ArraySize);
//						}
//						break;
//					}
//				}
				
//				command.Connection = connection;
//				command.CommandType = System.Data.CommandType.StoredProcedure;
//				command.CommandText = "up_rx_get_blob";
//				command.Parameters.Add("sRN", RN);
//				command.Parameters.Add("sFileName", OracleDbType.Varchar2, ParameterDirection.Output);
//				command.Parameters["sFileName"].Size = 240;
//				command.Parameters.Add("bDate", OracleDbType.Blob, ParameterDirection.Output);
//				command.ExecuteNonQuery();
//				filePath = string.Format(@"C:\BLOB\{0}({1}){2}", datetimeNow, RN, command.Parameters["sFileName"].Value.ToString());
				////				filePath += command.Parameters["sFileName"].Value.ToString();
//				var bDate = command.Parameters["bDate"].Value;
//
//				using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
//				{
//					byte[] bytes = new byte[0];
//					bytes = (byte[])((OracleBlob)bDate).Value;
//					int ArraySize = new int();
//					ArraySize = bytes.GetUpperBound(0);
//					fs.Write(bytes, 0, ArraySize);
//				}
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
			}
			
			if (string.IsNullOrEmpty(stringXML))
				Logger.ErrorFormat("Document Body from RN {0} not found", RN);
			
			return stringXML;
		}
		
		/// <summary>
		/// Создание или обновление контрагента в Парус 8
		/// </summary>
		/// <param name="Counterpaty"></param>
		/// <returns>RN</returns>
		[Remote, Public]
		public static string Sync_FormalizedDocument_Parus8(Sungero.Docflow.IAccountingDocumentBase Document, bool InSession)
		{
			return Sync_FormalizedDocument_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// 
		/// </summary>
		public static string Sync_FormalizedDocument_Parus8(Sungero.Docflow.IAccountingDocumentBase Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("FormalizedDocument_Parus8: {0} from ID {1}", "Start function synchronization", Document.Id.ToString());
			var RN = string.Empty;
			var ident = string.Empty;
			var byteArray = new byte[0];
//			var guid = string.Empty;
//			var type = string.Empty;
			OracleConnection connection = null;
			
			if (Document.IsFormalized.HasValue && Document.IsFormalized.Value)
			{
				var sellerTitle = Document.Versions
					.Where(x => x.Id == Document.SellerTitleId)
					.FirstOrDefault();
				
				if (sellerTitle != null)
				{
//					var guid = ContractStatements.Is(Document) ? "26fd4043-40d2-457c-ad0e-35cefa05d8dd" : string.Empty;
//					if (ContractStatements.Is(Document))
//					{
//						guid = "26fd4043-40d2-457c-ad0e-35cefa05d8dd";
//						type = "Акт";
//					}
//					else if (Waybills.Is(Document))
//					{
//						guid = "167d0859-1aa3-4d7b-934a-bfc4b0a8c7a1";
//						type = "Накладная";
//					} else if (IncomingTaxInvoices.Is(Document))
//					{
//						guid = "042a0bbc-88e9-49ed-887c-24fc30d1d4f3";
//						type = "Входящий счет на оплату";
//					}
					
					using (var memory = new System.IO.MemoryStream())
					{
						sellerTitle.Body.Read().CopyTo(memory);
						byteArray = memory.ToArray();
					}
					
					if (Connection != null)
					{
						connection = Connection;
					}
					else
					{
						connection = new OracleConnection();
						connection.ConnectionString = GetSettingString("ConnectionString");
						connection.Open();
						Logger.DebugFormat("FormalizedDocument_Parus8: {0}", "Open connection");
					}
					
					if (InSession)
					{
						StartSession_Parus8(connection);
					}
					
					try
					{
						using (OracleCommand command = new OracleCommand())
						{
							command.Connection = connection;
							command.CommandType = System.Data.CommandType.StoredProcedure;
							command.CommandText = "UP_RX_CREATE_FORMDOC_PARUS";
							
//						OracleClob clob = new OracleClob(connection);
							
							// Установить позицию для записи;
//						clob.Position = 0;
							
							// Начинаем ChunkWrite для повышения производительности
							// Обновления индекса происходят только один раз после EndChunkWrite
//						clob.BeginChunkWrite();
							
							var ru = System.Text.Encoding.GetEncoding(1251);
							var stringRU = ru.GetString(byteArray);
							
//						var stringUTF16 = System.Text.Encoding.Unicode.GetString(byteArray);
//						var charUTF16 = stringUTF16.ToCharArray();
							
							// Пишем в OracleClob
//						clob.Write(charUTF16, 0, charUTF16.Length);
							
//						clob.EndChunkWrite();
							
							command.Parameters.Add("bXML", stringRU);
							
							if (TryRN_By_Entity(Document, out RN))
								command.Parameters.Add("nRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
							else
								command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
							
							command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
							
							try
							{
								command.ExecuteNonQuery();
								
								RN = command.Parameters["nRN"].Value.ToString();
								
								ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
								
								if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
								{
									var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
									var regex = new Regex(@". * Not found. * from update RN =. *");
									if (regex.IsMatch(errMsg))
										Sync_ExternalEntityLink(Document, RN, true);
									RN = errMsg;
								}
								else
									Sync_ExternalEntityLink(Document, RN);
							}
							catch (Exception ex)
							{
								RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
							}
							
//						clob.Close();
//						clob.Dispose();
						}
					}
					finally
					{
						if (InSession)
						{
							StopSession_Parus8(connection);
						}
						
						if (Connection == null)
						{
							OracleConnection.ClearPool(connection);
							connection.Close();
							connection.Dispose();
							Logger.DebugFormat("FormalizedDocument_Parus8: {0}", "Clearing the connection pool and closing it");
						}
					}
				}
				else
				{
					RN = "У документа нет формализованной версии титула продавца";
				}
			}
			else
			{
				RN = "Документ не является формализованным";
			}
			
			return RN;
		}
		
		[Remote(IsPure = true)]
		public static Sungero.Domain.Shared.IEntity GetEntityByRN(string RN)
		{
			var link = GetLinkByRN(RN);

			if (link != null)
			{
				var entityType = new System.Guid(link.EntityType).GetTypeByGuid();
				if (link.EntityId.HasValue)
				{
					using (var session = new Sungero.Domain.Session())
					{
						return session.Get(entityType, link.EntityId.Value);
					}
				}
			}
			
			return link;
		}
		
		public static Sungero.Commons.IExternalEntityLink GetLinkByRN(string RN)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().Where(e => e.ExtEntityId == RN).OrderByDescending(e => e.Id).FirstOrDefault();
			
			return link;
		}
		
		public static Sungero.Commons.IExternalEntityLink GetLink(IEntity entity)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll()
				.Where(e => e.EntityId == entity.Id)
				.ToList()
				.Where(e => Equals(entity.GetEntityMetadata().BaseRootEntityMetadataGuid, new System.Guid(e.EntityType).GetBaseRootGuid()))
				.FirstOrDefault();
			
			return link;
		}
		
		public static Nullable<DateTime> GetLastUpdate(IEntity entity)
		{
			var lastHistoryUpdate = entity.History.GetAll()
				.Where(e => e.Action == Sungero.CoreEntities.History.Action.Update || e.Action == Sungero.CoreEntities.History.Action.Create)
				.OrderByDescending(e => e.HistoryDate)
				.FirstOrDefault();
			
			return lastHistoryUpdate != null ? lastHistoryUpdate.HistoryDate : null;
		}
		
		public static Nullable<DateTime> GetLastSync(IEntity entity)
		{
			var link = GetLink(entity);
			
			return link != null ? link.SyncDate : null;
		}
		
		public static Boolean IsNeedSync(IEntity entity)
		{
			return GetLastSync(entity) == null || GetLastSync(entity) < GetLastUpdate(entity);
		}
		
		/// <summary>
		/// Создание договора (Create_Contract_RX)
		/// </summary>
		/// <param name="RN">RN договора</param>
		/// <param name="Category">Имя категории договора</param>
		/// <param name="RegNum">Рег №</param>
		/// <param name="DateDoc">Дата договора</param>
		/// <param name="Note">Содержание</param>
		/// <param name="Sum">Сумма</param>
		/// <param name="DateFrom">Действует С</param>
		/// <param name="DateTo">Действует По</param>
		/// <param name="ORG">RN контрагента</param>
		/// <param name="OurOrg">RN нашей организации</param>
		/// <param name="Signer">ИНН подписанта</param>
		/// <param name="Responsible">ИНН ответственного</param>
		/// <param name="File">Наличие тела документа</param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static Sungero.Contracts.IContract Sync_Contract_RX(string RN, string Category, string RegNum, string DateDoc,
		                                                           string Note, string Sum, string DateFrom, string DateTo,
		                                                           string Org, string OurOrg, string Signer, string Responsible, string File)
		{
			Logger.DebugFormat("Contract_RX: RN - {0}, RegNum - {1} DateDoc - {2}", RN, RegNum, DateDoc);
			DateTime date0;
			Nullable<DateTime> date = Calendar.TryParseDate(DateDoc, out date0) ? date0 : (DateTime?)null;
			syscon.SC.IContract contract = syscon.SC.Contracts.GetAll().Where(e => e.RegistrationNumber == RegNum && Equals(e.RegistrationDate, date)).FirstOrDefault();
			int ID;
			
			if (TryID_By_RN(RN, out ID) || contract != null) //Если есть договор обновляем, иначе создаём нового
			{
				contract = contract ?? syscon.SC.Contracts.Get(ID);
				Logger.DebugFormat("Contract_RX: {0} {1}", "Record found ", contract.Id.ToString());
			}
			else
			{
				contract = syscon.SC.Contracts.Create();
				Logger.DebugFormat("Contract_RX: {0} {1}", "Record created ", contract.Id.ToString());
			}
			
			var emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN, Signer, StringComparison.CurrentCultureIgnoreCase));
			if (emps != null)
			{
				contract.OurSignatory = emps;
			}
			
			emps = syscon.SC.Employees.GetAll().FirstOrDefault(x => String.Equals(x.Person.TIN, Responsible, StringComparison.CurrentCultureIgnoreCase));
			if (emps != null)
			{
				contract.ResponsibleEmployee = emps;
			}
			
			if (!contract.RegistrationState.HasValue || contract.RegistrationState.Value == Sungero.Contracts.Contract.RegistrationState.NotRegistered)
			{
				if (emps != null)
				{
					contract.Department = emps.Department;
				}
				
				contract.DocumentGroup = Create_Category(Category);
				contract.RegistrationNumber = RegNum;
				
				if (date.HasValue)
					contract.RegistrationDate = date.Value;
				
				if (!contract.InternalApprovalState.HasValue
				    || contract.InternalApprovalState.Value == Sungero.Contracts.Contract.InternalApprovalState.Aborted
				    || contract.InternalApprovalState.Value == Sungero.Contracts.Contract.InternalApprovalState.OnRework)
					contract.Counterparty = GetOrg_By_RN(Org);
				
				contract.BusinessUnit = GetOurOrg_By_RN(OurOrg);
				
				contract.Subject = Note;
			}
			
			DateTime date1;
			if (Calendar.TryParseDate(DateFrom, out date1))
				contract.ValidFrom = date1;
			
			DateTime date2;
			if (Calendar.TryParseDate(DateTo, out date2))
				contract.ValidTill = date2;
			
			double sum;
			if (Double.TryParse(Sum, out sum))
				contract.TotalAmount =  sum;
			
			contract.SyncDateKS = Calendar.Now;

			contract.Save();
			Logger.DebugFormat("Contract_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(contract, RN);
			
			return contract;
		}
		
		/// <summary>
		/// Создание договора (Create_Contract_RX)
		/// </summary>
		/// <param name="RN">RN договора</param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static Sungero.Contracts.IContract Sync_Contract_RX(string RN, string File)
		{
			return Sync_Contract_RX(RN, File, true, null);
		}
		
		/// <summary>
		/// Создание договора (Create_Contract_RX)
		/// </summary>
		/// <param name="RN">RN договора</param>
		/// <returns></returns>
		public static Sungero.Contracts.IContract Sync_Contract_RX(string RN, string File, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("Contract: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			IContract contract = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("Contract: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
//				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
					command.CommandText = string.Format(@"select a.name Catalog,
				                                    nvl(v.sext_number, trim(v.sdoc_pref) || '-' || trim(v.sdoc_numb)) RegNum,
				                                    v.ddoc_date DateDoc,
				                                    v.ssubject Note,
				                                    0 Sum,
				                                    v.dbegin_date DateFrom,
				                                    v.dend_date DateTo,
				                                    v.nagent Org, -- RN контрагента
				                                    v.njur_pers_agent OurOrg, -- RN контрагента нашей организации
				                                    (select aa.agnidnumb from agnlist aa where aa.rn = v.nexecutive) Signer, -- ИНН сотрудника, который подписал
				                                    (select M.agnidnumb
				                                     from V_AGNLIST M
				                                     where VERSION = 1701001
				                                     and exists (select *
				                                                 from V_DOCS_PROPS_VALS_SHADOW DPC
				                                                 where (DPC.UNIT_RN = M.RN)
				                                                 and (DPC.DOCS_PROP_RN = 6547387386)
				                                                 and (DPC.UNITCODE = 'AGNLIST')
				                                                 and upper(DPC.STR_VALUE) = 'PARUS')) Responsible, -- ИНН сотрудника, который исполнитель
				                                    (select count(m1.nrn)
				                                     from V_FILELINKS M1
				                                     where nCOMPANY = 1590001
				                                     and M1.NRN in (select NFILELINKS_PRN
				                                                    from V_FILELINKSUNITS
				                                                    where NTABLE_PRN = v.nrn
				                                                    and sUNITCODE in
				                                                    (select sUNITCODE
				                                                     from V_UNITS
				                                                     where sUNITNAME = 'Договоры'))) nFile
				                                    from v_contracts v, acatalog a
				                                    where v.nrn = {0} -- RN договора
				                                    and a.rn = v.ncrn", RN);
//
					var reader = command.ExecuteReader();
					Logger.DebugFormat("Contract: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("Contract: {0}", "Value found");
						while (reader.Read())
						{
							var category = reader["Catalog"].ToString();
							var regNum = reader["RegNum"].ToString();
							var dateDoc = reader["DateDoc"].ToString();
							var note = reader["Note"].ToString();
							var sum = reader["Sum"].ToString();
							var dateFrom = reader["DateFrom"].ToString();
							var dateTo = reader["DateTo"].ToString();
							var org = reader["Org"].ToString();
							var ourOrg = reader["OurOrg"].ToString();
							var signer = reader["Signer"].ToString();
							var responsible = reader["Responsible"].ToString();

							contract = Sync_Contract_RX(RN, category, regNum, dateDoc,
							                            note, sum, dateFrom, dateTo,
							                            org, ourOrg, signer, responsible, File);
						}
					}
				}
				
				var file = false;
				if (bool.TryParse(File, out file) && file)
				{
					ImportDocumentBodyParus8(contract, RN, false, connection);
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("Contract: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return contract;
		}

		/// <summary>
		/// Создание исходещего счета на оплату
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static IOutgoingInvoiceNew Sync_OutgoingInvoice_RX(string RN, string File)
		{
			return Sync_OutgoingInvoice_RX(RN, File, true, null);
		}
		
		/// <summary>
		/// Создание исходещего счета на оплату
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		public static IOutgoingInvoiceNew Sync_OutgoingInvoice_RX(string RN, string File, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("OutgoingInvoice: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			IOutgoingInvoiceNew outgoingInvoice = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("OutgoingInvoice: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
//				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
					command.CommandText = string.Format(@"select trim(p.pref) || '-' || trim(p.numb) RegNum,
														       p.accdate DateDoc,
														       p.summwithnds Sum,
														       p.agent Org,
														       j.agent OurOrg
														  from payacc p, jurpersons j
														  where p.jur_pers=j.rn and p.rn={0}", RN);
					
					var reader = command.ExecuteReader();
					Logger.DebugFormat("OutgoingInvoice: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("OutgoingInvoice: {0}", "Value found");
						while (reader.Read())
						{
							var regNum = reader["RegNum"].ToString();
							var dateDoc = reader["DateDoc"].ToString();
							var orgRN = reader["Org"].ToString();
							var sum = reader["Sum"].ToString();
							var ourOrgRN = reader["OurOrg"].ToString();
//							var contractRN = reader["Contract"].ToString();

							outgoingInvoice = Sync_OutgoingInvoice_RX(RN, regNum, dateDoc,
							                                          orgRN, sum, ourOrgRN, File);
						}
					}
				}
				
				var file = false;
				if (bool.TryParse(File, out file) && file)
				{
					ImportDocumentBodyParus8(outgoingInvoice, RN, false, connection);
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("OutgoingInvoice: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return outgoingInvoice;
		}
		
		/// <summary>
		/// Создание исходещей счет-фактуры
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static IOutgoingTaxInvoice Sync_OutgoingTaxInvoice_RX(string RN, string File)
		{
			return Sync_OutgoingTaxInvoice_RX(RN, File, true, null);
		}
		
		/// <summary>
		/// Создание исходещей счет-фактуры
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		public static IOutgoingTaxInvoice Sync_OutgoingTaxInvoice_RX(string RN, string File, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("OutgoingTaxInvoice: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			IOutgoingTaxInvoice outgoingTaxInvoice = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("OutgoingTaxInvoice: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
//				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
					command.CommandText = string.Format(@"SELECT
														 TRIM(T.SPREF_NUMB)||'-'||TRIM(T.SREG_NUMB) RegNum,
														 T.DACC_DATE                        DateDoc,
														 T.CS_CODE_RN                       Org,
														 T.nall_sum_total                   Sum,
														 T.PR_CODE_RN                       OurOrg,
														 (SELECT S.nprn FROM V_STAGES S
														   WHERE S.nfaceacc=T.nCS_FACEACC)  Contract
														FROM V_DICACCFO T WHERE T.NRN = {0}", RN);
					
					var reader = command.ExecuteReader();
					Logger.DebugFormat("OutgoingTaxInvoice: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("OutgoingTaxInvoice: {0}", "Value found");
						while (reader.Read())
						{
							var regNum = reader["RegNum"].ToString();
							var dateDoc = reader["DateDoc"].ToString();
							var orgRN = reader["Org"].ToString();
							var sum = reader["Sum"].ToString();
							var ourOrgRN = reader["OurOrg"].ToString();
							var contractRN = reader["Contract"].ToString();

							outgoingTaxInvoice = Sync_OutgoingTaxInvoice_RX(RN, regNum, dateDoc,
							                                                orgRN, sum, ourOrgRN, contractRN, File);
						}
					}
				}
				
				var file = false;
				if (bool.TryParse(File, out file) && file)
				{
					ImportDocumentBodyParus8(outgoingTaxInvoice, RN, false, connection);
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("OutgoingTaxInvoice: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return outgoingTaxInvoice;
		}
		
//		/// <summary>
//		/// Создание или обновление УПД в системе Directum RX
//		/// </summary>
//		/// <param name="Document"></param>
//		[Remote(PackResultEntityEagerly = true)]
//		public static IUniversalTransferDocument Sync_UniversalTransferDocument_RX(string RN, string File)
//		{
//			IUniversalTransferDocument universalTransferDocument = null;
//
//			OracleConnection connection = new OracleConnection();
//			connection.ConnectionString = GetSettingString("ConnectionString");
//			connection.Open();
//
//			using (OracleCommand command = new OracleCommand())
//			{
//				command.Connection = connection;
		////				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
//				command.CommandText = string.Format("select v.rn \"RN\", v.agnname \"Name\", v.agnidnumb \"INN\", v.sreason_code \"KPP\" from v_agnlist v where v.rn={0}", RN);
		////
//				var reader = command.ExecuteReader();
//				if (reader.HasRows)
//				{
//					while (reader.Read())
//					{
//						var regNum = reader["RegNum"].ToString();
//						var dateDoc = reader["DateDoc"].ToString();
//						var orgRN = reader["OrgRN"].ToString();
//						var ourOrgRN = reader["OurOrgRN"].ToString();
//						var contractRN = reader["ContractRN"].ToString();
//
//						universalTransferDocument = Sync_UniversalTransferDocument_RX(RN, regNum, dateDoc,
//						                                                              orgRN, ourOrgRN, contractRN, File);
//					}
//				}
//			}
//
//			OracleConnection.ClearPool(connection);
//			connection.Close();
//			connection.Dispose();
//
//			var file = false;
//			if (bool.TryParse(File, out file) && file)
//			{
//				ImportDocumentBodyParus8(universalTransferDocument, RN, false, connection);
//			}
//
//			RemoveChangedObject_Parus8(RN, false, connection);
//
//			return universalTransferDocument;
//		}
		
		/// <summary>
		/// Создание акта выполненных работ в Directum RX
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static Sungero.Docflow.IAccountingDocumentBase Sync_AccountingDocument_RX(string RN, string File)
		{
			return Sync_AccountingDocument_RX(RN, File, true, null);
		}
		
		/// <summary>
		/// Создание акта выполненных работ в Directum RX
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="File"></param>
		/// <returns></returns>
		public static Sungero.Docflow.IAccountingDocumentBase Sync_AccountingDocument_RX(string RN, string File, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("AccountingDocument: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			Sungero.Docflow.IAccountingDocumentBase document = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("AccountingDocument: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
//				command.CommandText = string.Format("SELECT RN, AGNNAME, AGNIDNUMB, REASON_CODE FROM PARUS.AGNLIST WHERE RN = {0}", RN);
					command.CommandText = string.Format(@"SELECT
														 TRIM(T.sindocpref)||'-'||TRIM(T.sindocnumb) RegNum,
														 T.dindocdate                                DateDoc,
														 T.nseller                                   Org,
														 T.nfaceacc                                  PersAcc,
														 (SELECT J.nagent FROM V_JURPERSONS J
														   WHERE J.NRN=T.njur_pers)                  OurOrg,
														 (CASE
														   WHEN INSTR(UPPER(T.sindoctype),'АКТ') > 0
														   THEN 'Акт'
														    ELSE 'Накладная'
														  END )                                      Type,
														 (SELECT S.nprn FROM V_STAGES S
														   WHERE S.nfaceacc=T.nfaceacc)              Contract
														FROM V_INORDERS T WHERE T.NRN = {0}", RN);
//
					var reader = command.ExecuteReader();
					Logger.DebugFormat("AccountingDocument: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("AccountingDocument: {0}", "Value found");
						while (reader.Read())
						{
							var regNum = reader["RegNum"].ToString();
							var dateDoc = reader["DateDoc"].ToString();
							var orgRN = reader["Org"].ToString();
							var type = reader["Type"].ToString();
							var ourOrgRN = reader["OurOrg"].ToString();
							var contractRN = reader["Contract"].ToString();

							if (string.Equals(type, "Акт", StringComparison.OrdinalIgnoreCase))
								document = Sync_ContractStatement_RX(RN, regNum, dateDoc,
								                                     orgRN, ourOrgRN, contractRN, File);
							else
								document = Sync_Waybill_RX(RN, regNum, dateDoc,
								                           orgRN, ourOrgRN, contractRN, File);
						}
					}
				}
				
				var file = false;
				if (bool.TryParse(File, out file) && file)
				{
					ImportDocumentBodyParus8(document, RN, false, connection);
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("AccountingDocument: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return document;
		}
		
//		/// <summary>
//		/// 
//		/// </summary>
//		[Remote]
//		public virtual void GetStagesSC()
//		{
//			Sungero.Docflow.IOfficialDocument document = Sungero.Docflow.OfficialDocuments.Get(91);
//			Sungero.Docflow.IApprovalTask task = Sungero.Docflow.ApprovalTasks.As(Sungero.Workflow.Tasks.Get(111));
//			var rule = Sungero.Docflow.ApprovalRuleBases.Get(11);
//			var stages = new List<Sungero.Docflow.Structures.Module.DefinedApprovalStageLite>() { };
//			bool canDefineConditions = false;
//
//			// Вычисление первого этапа.
//			var currentStageNumber = rule.Transitions.Select(x => x.SourceStage).FirstOrDefault(s => !rule.Transitions.Any(t => t.TargetStage.Equals(s)));
//
//
//			if (rule.Stages.Any(x => x.Number == currentStageNumber))
//			{
//				var stage = rule.Stages.Single(s => s.Number == currentStageNumber);
//				stages.Add(Sungero.Docflow.Structures.Module.DefinedApprovalStageLite.Create(stage.Stage, stage.Number, stage.StageType));
//				canDefineConditions = true;
//			}
//
//			Sungero.Docflow.Structures.ApprovalRuleBase.NextStageNumber nextStageNumber = null;// = SC.PublicFunctions .ApprovalRule.GetNextStageNumber(rule, document, currentStageNumber, task);
//			var error = nextStageNumber  //.Message;
//
//			if (nextStageNumber != null)
//				canDefineConditions = nextStageNumber.Number != -1;
//
//			while (nextStageNumber.Number != null && nextStageNumber.Number >= 0)
//			{
//				currentStageNumber = nextStageNumber.Number.Value;
//				nextStageNumber = null; // SC.PublicFunctions.Module.Remote.GetNextStageNumber(rule, document, currentStageNumber, task);
//
//				if (nextStageNumber.Number == -1)
//				{
//					error = nextStageNumber.Message;
//					canDefineConditions = false;
//					break;
//				}
//
//				var stage = rule.Stages.Single(s => s.Number == currentStageNumber);
//				stages.Add(Sungero.Docflow.Structures.Module.DefinedApprovalStageLite.Create(stage.Stage, stage.Number, stage.StageType));
//			}
//
//			//return Structures.Module.DefinedApprovalStages.Create(stages, canDefineConditions, error);
//		}
		
		
		
		[Remote, Public]
		public syscon.Parus8.IRoleCalcKS ReturnEmpForRole3(Sungero.Docflow.IApprovalRoleBase role, syscon.SC.IApprovalTask task)
		{
			var maindoc = task.DocumentGroup.OfficialDocuments.First();
			var emp = syscon.Parus8.RoleCalcKSes.GetAll().FirstOrDefault(x => Equals(role,x.RoleKS) && Equals(x.OurOrgKS, maindoc.BusinessUnit) && Equals(x.RegGroupKS,task.RegGroupKS) && Equals(x.AuthorDepKS, maindoc.Department));
			return emp;
		}
		
		[Remote, Public]
		public syscon.Parus8.IRoleCalcKS ReturnEmpForRole2(Sungero.Docflow.IApprovalRoleBase role, Sungero.Docflow.IOfficialDocument maindoc, Sungero.Docflow.IRegistrationGroup RG)
		{
			var emp = syscon.Parus8.RoleCalcKSes.GetAll().FirstOrDefault(x => Equals(role,x.RoleKS) && Equals(x.OurOrgKS, maindoc.BusinessUnit) && Equals(x.RegGroupKS,RG) && Equals(x.AuthorDepKS, maindoc.Department));
			return emp;
		}
		
		[Remote, Public]
		public void ImportContractCatFromExcel()
		{
			Excel.Application xls = new Excel.Application();
			//Отобразить Excel
			xls.Visible = true;
			//Количество листов в рабочей книге
			xls.SheetsInNewWorkbook = 2;
			//Добавить рабочую книгу
			Excel.Workbook workBook = xls.Workbooks.Add(Type.Missing);
			//Отключить отображение окон с сообщениями
			xls.DisplayAlerts = false;
			//Получаем первый лист документа (счет начинается с 1)
			Excel.Worksheet sheet = (Excel.Worksheet)xls.Worksheets.get_Item(1);
			//Название листа (вкладки снизу)
			sheet.Name = "Отчет за 13.12.2017";
			//Пример заполнения ячеек
			for (int i = 1; i <= 9; i++)
			{
				for (int j = 1; j < 9; j++)
					sheet.Cells[i, j] = String.Format("Boom {0} {1}", i, j);
			}
			
//			var file = @"C:\DRX\1.xlsx";
//			Excel.Application xls = new Excel.Application();
//			xls.Visible = true
//				Excel.Workbook wb = xls.Workbooks.Open(file);
//			Excel.Worksheet sh = (Excel.Worksheet)xls.Worksheets.get_Item(1);
//			Excel.Range data= sh.UsedRange;
//			for (int i = 2; i < 200; i++)
//			{
//				if (data.Cells[i,2].ToString() != null)
//				{
//					var cat = SC.ContractCategories.Create();
//					cat.Name = data.Cells[i,1].ToString();
//					cat.MainCategorysyscon = data.Cells[i,2].ToString();
//					cat.Save();
//				}
//			}
		}
		
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote]
		public static string DownloadFormalizedFileParus8(string XMLString)
		{
			var filesFolder = @"C:\DRX\BLOB\";
			
			if (!Directory.Exists(filesFolder))
				Directory.CreateDirectory(filesFolder);
			
			var filePath = @"C:\DRX\BLOB\XMLFile.xml";
			
			File.WriteAllText(filePath, XMLString);
			
			return filePath;
		}
		
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote]
		public static List<string> DownloadFileParus8(string RN, bool InSession)
		{
			return DownloadFileParus8(RN, InSession, null);
		}
		
		/// <summary>
		/// Загрузка файла из Парус8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static List<string> DownloadFileParus8(string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			var filesList = new List<string>();
			string datetimeNow = Calendar.Now.ToString("yyyy_MM_dd_HH_mm_ss");
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			var filesFolder = @"C:\DRX\BLOB\";
			
			if (!Directory.Exists(filesFolder))
				Directory.CreateDirectory(filesFolder);
			
			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format(@"select M.sfile_path sFileName, m.bdata bDate, m.sfile_type sFileType
														from V_FILELINKS M
														where nCOMPANY = 1590001
														and M.NRN in
														(select NFILELINKS_PRN
														from V_FILELINKSUNITS
														where NTABLE_PRN = {0}
														and sUNITCODE in (select sUNITCODE
														from V_UNITS
														where sUNITNAME = 'Договоры'))
														order by NRN
														", RN);
				var reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						var filePath = string.Format(@"{3}({1}){2}", datetimeNow, RN, reader["sFileName"].ToString(), filesFolder);
//						var filePath = string.Format(@"{3}{0}({1}){2}", datetimeNow, RN, reader["sFileName"].ToString(), filesFolder);
						Logger.DebugFormat("DownloadFileParus8: {0} {1}", "Загружается файл", filePath);
						var bDate = reader["bDate"];

						using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
						{
							byte[] bytes = new byte[0];
							bytes = (byte[])bDate;
							int ArraySize = new int();
							ArraySize = bytes.GetUpperBound(0);
							fs.Write(bytes, 0, ArraySize);
							fs.Close();
							fs.Dispose();
						}
						
						filesList.Add(filePath);
					}
				}
				
//				command.Connection = connection;
//				command.CommandType = System.Data.CommandType.StoredProcedure;
//				command.CommandText = "up_rx_get_blob";
//				command.Parameters.Add("sRN", RN);
//				command.Parameters.Add("sFileName", OracleDbType.Varchar2, ParameterDirection.Output);
//				command.Parameters["sFileName"].Size = 240;
//				command.Parameters.Add("bDate", OracleDbType.Blob, ParameterDirection.Output);
//				command.ExecuteNonQuery();
//				filePath = string.Format(@"C:\BLOB\{0}({1}){2}", datetimeNow, RN, command.Parameters["sFileName"].Value.ToString());
				////				filePath += command.Parameters["sFileName"].Value.ToString();
//				var bDate = command.Parameters["bDate"].Value;
//
//				using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
//				{
//					byte[] bytes = new byte[0];
//					bytes = (byte[])((OracleBlob)bDate).Value;
//					int ArraySize = new int();
//					ArraySize = bytes.GetUpperBound(0);
//					fs.Write(bytes, 0, ArraySize);
//				}
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
			}
			
			return filesList;
		}
		
		/// <summary>
		/// Импорт тела документа в Directum RX
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static void ImportDocumentBodyParus8(Sungero.Content.IElectronicDocument document, string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			var isFirst = true;
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandText = string.Format(@"select M.sfile_path sFileName, m.bdata bDate, m.sfile_type sFileType
														from V_FILELINKS M
														where nCOMPANY = 1590001
														and M.NRN in
														(select NFILELINKS_PRN
														from V_FILELINKSUNITS
														where NTABLE_PRN = {0}
														and sUNITCODE in (select sUNITCODE
														from V_UNITS
														where sUNITNAME = 'Договоры'))
														order by NRN
														", RN);
					var reader = command.ExecuteReader();
					if (reader.HasRows)
					{
						while (reader.Read())
						{
							var fileName = reader["sFileName"].ToString();
							Logger.DebugFormat("ImportDocumentBodyParus8: {0} RN {1} Name {2}", "Download file", RN, fileName);
							var bDate = (byte[])reader["bDate"];

							using (System.IO.MemoryStream ms = new System.IO.MemoryStream(bDate))
							{
								try
								{
									if (isFirst)
									{
										if (document.HasVersions)
										{
											document.LastVersion.Body.Write(ms);
											document.LastVersion.AssociatedApplication = Sungero.Exchange.PublicFunctions.Module.Remote.GetOrCreateAssociatedApplicationByDocumentName(fileName);
										}
										else
										{
											document.CreateVersionFrom(ms, Path.GetExtension(fileName));
										}
										document.Save();
									}
									else
									{
										var simpleDoc = document.Relations.GetRelated().Where(e => e.Name == Path.GetFileNameWithoutExtension(fileName)).FirstOrDefault();
										if (simpleDoc != null && simpleDoc.HasVersions)
										{
											simpleDoc.LastVersion.Body.Write(ms);
											simpleDoc.LastVersion.AssociatedApplication = Sungero.Exchange.PublicFunctions.Module.Remote.GetOrCreateAssociatedApplicationByDocumentName(fileName);
											simpleDoc.Save();
										}
										else
										{
											simpleDoc = Sungero.Docflow.SimpleDocuments.Create();
											simpleDoc.Name = Path.GetFileNameWithoutExtension(fileName);
											simpleDoc.CreateVersionFrom(ms, Path.GetExtension(fileName));
										}
										simpleDoc.Save();
										
										simpleDoc.Relations.AddFrom(Sungero.Docflow.Constants.Module.AddendumRelationName, document);
										simpleDoc.Relations.Save();
									}
								}
								catch {
									Logger.DebugFormat("ImportDocumentBodyParus8: {0} RN {1} Name {2}", "File import error", RN, fileName);
								}
								
								ms.Close();
								ms.Dispose();
							}
							
							isFirst = false;
						}
					}
				}
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
				}
			}

//				if (filesList.Count > 0)
//				{
//					var filePath = filesList.First();
//
//					if (!string.IsNullOrEmpty(filePath))
//					{
//						try
//						{
//							using (System.IO.FileStream fs = new System.IO.FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Read))
//							{
//								if (document.HasVersions)
//								{
			////								document.Versions.Last().Import(filePath);
//									document.LastVersion.Body.Write(fs);
//									document.LastVersion.AssociatedApplication = Sungero.Exchange.PublicFunctions.Module.Remote.GetOrCreateAssociatedApplicationByDocumentName(filePath);
//								}
//								else
//								{
			////								document.CreateVersionFrom(filePath);
//									document.CreateVersionFrom(fs, Path.GetExtension(filePath));
//								}
//								document.Save();
//							}
//						}
//						catch { }
//					}
//
//					foreach (var file in filesList.Skip(1))
//					{
//						try
//						{
//							using (System.IO.FileStream fs = new System.IO.FileStream(file, FileMode.OpenOrCreate, FileAccess.Read))
//							{
//								var simpleDoc = document.Relations.GetRelated().Where(e => e.Name == Path.GetFileNameWithoutExtension(file)).FirstOrDefault();
//								if (simpleDoc != null && simpleDoc.HasVersions)
//								{
			////								simpleDoc.Versions.Last().Import(file);
//									simpleDoc.LastVersion.Body.Write(fs);
//									simpleDoc.LastVersion.AssociatedApplication = Sungero.Exchange.PublicFunctions.Module.Remote.GetOrCreateAssociatedApplicationByDocumentName(file);
//									simpleDoc.Save();
//								}
//								else
//								{
			////								simpleDoc = Sungero.Docflow.SimpleDocuments.CreateFrom(file);
//									simpleDoc = Sungero.Docflow.SimpleDocuments.Create();
//									simpleDoc.Name = Path.GetFileNameWithoutExtension(file);
//									simpleDoc.CreateVersionFrom(fs, Path.GetExtension(file));
//								}
//								simpleDoc.Save();
//
//								simpleDoc.Relations.AddFrom(Sungero.Docflow.Constants.Module.AddendumRelationName, document);
//								simpleDoc.Relations.Save();
//								simpleDoc = null;
//
//								fs.Close();
//								fs.Dispose();
//							}
//						}
//						catch { }
//					}
//					document = null;
//
//					foreach (var file in filesList)
//					{
//						try
//						{
//							File.Delete(file);
//						}
//						catch {	}
//					}
//				}
		}
		
		/// <summary>
		/// Вернуть ошибку из Парус 8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static string GetErrMsgLog_Parus8(string Ident, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			var message = string.Empty;
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("select T.TEXT Subject from V_RX_ERRMSGLOG T WHERE T.TRANSACTION_ID = {0}", Ident);
				var reader = command.ExecuteReader();
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						message = reader["Subject"].ToString();
					}
				}
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
			}
			
			return message;
		}
		
		/// <summary>
		/// Массовая загрузка объектов из Парус 8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote, Public]
		public static List<string> GetChangedObjects_Parus8(string Type)
		{
			return GetChangedObjects_Parus8(Type, true, null);
		}
		
		/// <summary>
		/// Массовая загрузка объектов из Парус 8
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static List<string> GetChangedObjects_Parus8(string Type, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			var ListRN = new List<string>();
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("select RN, DATE_SYNC, DATE_UPDATE from PARUS.UT_RX_LASTSYNCTIME where UNIT = '{0}'", Type);
				
				var reader = command.ExecuteReader();
				
				if (reader.HasRows)
				{
					while (reader.Read())
					{
						ListRN.Add(reader["RN"].ToString());
					}
				}
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
			}
			
			return ListRN;
		}
		
		/// <summary>
		/// Получить измененные организации
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		[Remote, Public]
		public static List<ICompany> GetChangedOrganizations_RX()
		{
//			var companies = Companies.GetAll().Where(e => IsNeedSync(e)).ToList();
			
			var companies = Companies.GetAll().Take(20).ToList();
			var companies2 = new List<ICompany>();
			
			foreach (var com in companies)
			{
				if (IsNeedSync(com))
				{
					companies2.Add(com);
				}
			}
			
			return companies2;
		}
		
		/// <summary>
		/// Очистка таблицы измененных объектов
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static void RemoveChangedObject_Parus8(List<string> ListRN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("DELETE from PARUS.UT_RX_NEWDOC WHERE RN_DOC in {0}", string.Join(",", ListRN));
				command.ExecuteNonQuery();
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
			}
		}
		
		/// <summary>
		/// Очистка таблицы измененных объектов
		/// </summary>
		/// <param name="RN"></param>
		/// <returns></returns>
		public static void RemoveChangedObject_Parus8(string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("RemoveChangedObject_Parus8: {0}", "Start function");
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}

			using (OracleCommand command = new OracleCommand())
			{
				command.Connection = connection;
				command.CommandText = string.Format("DELETE from PARUS.UT_RX_NEWDOC WHERE RN_DOC = {0}", RN);
				command.ExecuteNonQuery();
				Logger.DebugFormat("RemoveChangedObject_Parus8: {0}", "Objects deleted");
			}
			
			if (InSession)
			{
				StopSession_Parus8(connection);
			}
			
			if (Connection == null)
			{
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();
				Logger.DebugFormat("RemoveChangedObject_Parus8: {0}", "Clearing the connection pool and closing it");
			}
			Logger.DebugFormat("RemoveChangedObject_Parus8: {0}", "Function completion");
		}
		
		/// <summary>
		/// Создание контрагента (Create_ORG_RX)
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <param name="Name">Наименование контрагента</param>
		/// <param name="INN">ИНН контрагента</param>
		/// <param name="KPP">КПП контрагента</param>
		[Remote(PackResultEntityEagerly = true)]
		public static Sungero.Parties.ICompany Sync_Organization_RX(string RN, string Name, string INN, string KPP)
		{
			Logger.DebugFormat("Organization_RX: RN - {0}, INN - {1} KPP - {2}", RN, INN, KPP);
			IBusinessUnit ourOrg = GetOurOrg_By_RN(RN);
			
			if (ourOrg != null)
			{
				return Sync_OurOrganization_RX(RN, Name, INN, KPP).Company;
			}
			
			ICompany org = GetOrg_By_RN(RN);
			
			if (org == null)
			{
				org = Companies.GetAll().Where(e => e.TIN == INN && e.TRRC == KPP).FirstOrDefault();
			}
			
			if (org != null) //Если есть контрагент обновляем, иначе создаём нового
			{
				Logger.DebugFormat("Organization_RX: {0} {1}", "Record found ID", org.Id.ToString());
			}
			else
			{
				org = Sungero.Parties.Companies.Create();
				Logger.DebugFormat("Organization_RX: {0} {1}", "Record created ID", org.Id.ToString());
			}
			
			org.Name = Name;
			org.TIN = INN;
			org.TRRC = KPP;
			org.Save();
			
			Logger.DebugFormat("Organization_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(org, RN);
			
			return org;
		}
		
		/// <summary>
		/// Загрузка организации из Парус 8 по RN
		/// </summary>
		[Remote(PackResultEntityEagerly = true), Public]
		public static Sungero.Parties.ICompany Sync_Organization_RX(string RN)
		{
			return Sync_Organization_RX(RN, true, null);
		}
		
		/// <summary>
		/// Загрузка организации из Парус 8 по RN
		/// </summary>
		public static Sungero.Parties.ICompany Sync_Organization_RX(string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("Organization: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			ICompany org = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("Organization: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandText = string.Format("select v.rn \"RN\", v.agnname \"Name\", v.agnidnumb \"INN\", v.sreason_code \"KPP\" from v_agnlist v where v.rn={0}", RN);

					var reader = command.ExecuteReader();
					Logger.DebugFormat("Organization: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("Organization: {0}", "Value found");
						while (reader.Read())
						{
							var nameOrg = reader["Name"].ToString();
							var inn = reader["INN"].ToString();
							var kpp = reader["KPP"].ToString();
							org = Sync_Organization_RX(RN, nameOrg, inn, kpp);
						}
					}
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("Organization: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return org;
		}
		
		/// <summary>
		/// Массовая загрузка организаций из Парус 8
		/// </summary>
		[Remote, Public]
		public static string MultipleSync_Organizations_RX()
		{
			var cntLoad = 0;
			var cntError = 0;
			var cntAll = 0;

			var logsFolder = @"C:\DRX\logs\";

			if (!Directory.Exists(logsFolder))
				Directory.CreateDirectory(logsFolder);

			// Передаем тип для возвращаемых объектов - 1 (Организации)
			var ListOrgRN = GetChangedObjects_Parus8("AGNLIST");

			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

			if (ListOrgRN.Count > 0)
			{
				sw.Start();

				OracleConnection connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();

				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandText = "SELECT Org.RN, Org.AGNNAME, Org.AGNIDNUMB, Org.REASON_CODE FROM PARUS.AGNLIST Org INNER JOIN PARUS.UT_RX_NEWDOC Change ON Org.RN = Change.RN_DOC WHERE Change.TYPE_DOC = 1";
					var reader = command.ExecuteReader();
					if (reader.HasRows)
					{
						while (reader.Read())
						{
							var rn = reader["RN"].ToString();
							var nameOrg = reader["AGNNAME"].ToString();
							var inn = reader["AGNIDNUMB"].ToString();
							var kpp = reader["REASON_CODE"].ToString();

							try
							{
								Sync_Organization_RX(rn, nameOrg, inn, kpp);

								using (var file = File.AppendText(string.Format(logsFolder + "log_migration_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
								{
									file.WriteLine(string.Format("Организация {0} (RN {1}) загружена: INN {2}, KPP {3}", nameOrg, rn, inn, kpp));
								}

								cntLoad++;
							}
							catch (Exception ex)
							{
								using (var file = File.AppendText(string.Format(logsFolder + "log_error_org_{0}.txt", Calendar.Now.ToString("dd_MM_yyyy"))))
								{
									file.WriteLine(string.Format("Организация c RN {0} не загружена загружена.\r\n{1}", rn, ex.Message));
								}

//								ListOrgRN.Remove(reader["RN"].ToString());
								cntError++;
							}
							cntAll++;
						}
					}
				}
				
				OracleConnection.ClearPool(connection);
				connection.Close();
				connection.Dispose();

				sw.Stop();
				//				RemoveChangedObject_Parus8(ListOrgRN);
			}

			string resultMessage = string.Format("Загрузка завершена.\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество загруженных организаций: {0}\r\n"
			                                     + "Количество ошибок при загрузке: {1}\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Количество просмотренных организаций: {2}\r\n"
			                                     + "---------------------------------------------------------\r\n"
			                                     + "Время выполнения: {3}",
			                                     cntLoad, cntError, cntAll,
			                                     sw.Elapsed.ToString(@"d\.hh\:mm\:ss"));
			//			                                     "0");

			sw = null;

			return resultMessage;
		}

		/// <summary>
		/// Создание или обновление документа входящего счета на оплату в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static string Sync_IncomingInvoice_Parus8(IIncomingInvoice Document, bool InSession)
		{
			return Sync_IncomingInvoice_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// Создание или обновление документа входящего счета на оплату в системе Парус 8
		/// </summary>
		/// <param name="Document"></param>
		public static string Sync_IncomingInvoice_Parus8(IIncomingInvoice Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("IncomingInvoice_Parus8: {0} from ID {1}", "Start function synchronization ", Document.Id.ToString());
			var RN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			var ident = string.Empty;
			OracleConnection connection = null;
			
			if (TryRN_By_Entity(Document, out RN) || string.IsNullOrEmpty(RN))
			{
				if (Connection != null)
				{
					connection = Connection;
				}
				else
				{
					connection = new OracleConnection();
					connection.ConnectionString = GetSettingString("ConnectionString");
					connection.Open();
					Logger.DebugFormat("IncomingInvoice_Parus8: {0}", "Open connection");
				}
				
				if (InSession)
				{
					StartSession_Parus8(connection);
				}
				
				try
				{
					var incInvoice = syscon.SC.IncomingInvoices.As(Document);
					
					if (Document.Contract != null && !TryRN_By_Entity(Document.Contract, out contractRN))
					{
						contractRN = Sync_Contract_Parus8(Document.Contract, false, connection);
//					Document.Note += "\r\nДоговор: " + contractRN;
						if (!Functions.Module.CheckRN(contractRN))
							return contractRN;
					}
					
					if (!TryRN_By_Entity(Document.Counterparty, out counterpartyRN))
					{
						counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
//					Document.Note += "\r\nКонтрагент: " + counterpartyRN;
						if (!Functions.Module.CheckRN(counterpartyRN))
							return counterpartyRN;
					}
					
					if (!TryRN_By_Entity(Document.BusinessUnit.Company, out businessUnitRN))
					{
						businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company, false, connection);
//					Document.Note += "\r\nНаша организация: " + businessUnitRN;
						if (!Functions.Module.CheckRN(businessUnitRN))
							return businessUnitRN;
					}
					
//				Document.Save();

					using (OracleCommand command = new OracleCommand())
					{
						command.Connection = connection;
						command.CommandType = System.Data.CommandType.StoredProcedure;
						command.CommandText = "UP_RX_ADD_PAYACCIN";
						
						command.Parameters.Add("NUM", Document.Number);
						command.Parameters.Add("DATEDOC", Document.Date);
						command.Parameters.Add("ORG", counterpartyRN);
						command.Parameters.Add("OURORG", businessUnitRN);
						command.Parameters.Add("CONTRACT", contractRN);
						command.Parameters.Add("PERSACC", incInvoice.PersonAccountKS);
						command.Parameters.Add("NSUM ", Document.TotalAmount);
						command.Parameters.Add("NSUMTAX", incInvoice.AmountVATKS);
						command.Parameters.Add("DPAY", incInvoice.PaymentDateKS);
						command.Parameters.Add("SCOM", Document.Subject);

						if (!string.IsNullOrEmpty(RN))
							command.Parameters.Add("NRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
						else
							command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						try
						{
							command.ExecuteNonQuery();
							
							RN = command.Parameters["NRN"].Value.ToString();
							
							ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
							
//						Document.Note = RN + " | " + ident + "\r\n" + Document.Number
//							+ " | " + Document.Date.ToString()
//							+ " | " + counterpartyRN
//							+ " | " + businessUnitRN
//							+ " | " +  contractRN
//							+ " | " + incInvoice.PersonAccountKS
//							+ " | " +  Document.TotalAmount.ToString()
//							+ " | " + incInvoice.AmountVATKS.ToString()
//							+ " | " +  incInvoice.PaymentDateKS.ToString()
//							+ " | " + 	 Document.Subject;
//						Document.Save();
							
							if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
							{
								var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
								Document.Note += "\r\n" + errMsg;
								Document.Save();
								var regex = new Regex(@". * Not found. * from update RN =. *");
								if (regex.IsMatch(errMsg))
									Sync_ExternalEntityLink(Document, RN, true);
								RN = errMsg;
							}
							else
								Sync_ExternalEntityLink(Document, RN);
						}
						catch (Exception ex)
						{
							RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
						}
					}
				}
				finally
				{
					if (InSession)
					{
						StopSession_Parus8(connection);
					}
					
					if (Connection == null)
					{
						OracleConnection.ClearPool(connection);
						connection.Close();
						connection.Dispose();
						Logger.DebugFormat("IncomingInvoice_Parus8: {0}", "Clearing the connection pool and closing it");
					}
				}
			}
			
			return RN;
		}
		
		
		/// <summary>
		/// Старт сессии в Парус 8
		/// </summary>
		/// <param name="connection"></param>
		/// <returns></returns>
		public static void StartSession_Parus8(Oracle.DataAccess.Client.OracleConnection Connection)
		{
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = Connection;
					command.CommandText = string.Format("begin {0}; end;", GetSettingString("LOGON_WIN"));
					command.ExecuteNonQuery();
					Logger.DebugFormat("Session: {0}", "Start session");
				}
			}
			catch { }
		}
		
		/// <summary>
		/// Остановка сессии в Парус 8
		/// </summary>
		/// <param name="connection"></param>
		/// <returns></returns>
		public static void StopSession_Parus8(Oracle.DataAccess.Client.OracleConnection Connection)
		{
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = Connection;
					command.CommandText = "begin PKG_SESSION.LOGOFF; PKG_SESSION.DROP_SESSION; end;";
					command.ExecuteNonQuery();
					Logger.DebugFormat("Session: {0}", "Close session");
				}
			}
			catch { }
		}

		/// <summary>
		/// Создание или обновление Договора в Парус 8
		/// </summary>
		/// <param name="Contract"></param>
		/// <returns></returns>
		public static string Sync_Contract_Parus8(Sungero.Docflow.IContractualDocumentBase Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("Contract_Parus8: {0} from ID {1}", "Start function synchronization ", Document.Id.ToString());
			var RN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			var ident = string.Empty;
			OracleConnection connection = null;
			
			if (TryRN_By_Entity(Document, out RN) || string.IsNullOrEmpty(RN))
			{
				if (Connection != null)
				{
					connection = Connection;
				}
				else
				{
					connection = new OracleConnection();
					connection.ConnectionString = GetSettingString("ConnectionString");
					connection.Open();
					Logger.DebugFormat("Contract_Parus8: {0}", "Open connection");
				}
				
				if (InSession)
				{
					StartSession_Parus8(connection);
				}
				
				try
				{
					if (!TryRN_By_Entity(Document.Counterparty, out counterpartyRN))
						counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
					
					if (!TryRN_By_Entity(Document.BusinessUnit.Company, out businessUnitRN))
						businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company, false, connection);
					
					var signerINN = (Document.OurSignatory != null && Document.OurSignatory.Person != null) ? Document.OurSignatory.Person.TIN : string.Empty;
					var responsibleINN = string.Empty;
					Nullable<DateTime> dateFrom = null;
					Nullable<DateTime> dateTo = null;
					var category = string.Empty;
					
					if (Sungero.Contracts.ContractualDocuments.Is(Document))
					{
						var contractualDocument = Sungero.Contracts.ContractualDocuments.As(Document);
						responsibleINN = (contractualDocument.ResponsibleEmployee != null && contractualDocument.ResponsibleEmployee.Person != null) ? contractualDocument.ResponsibleEmployee.Person.TIN : string.Empty;
						dateFrom = contractualDocument.ValidFrom;
						dateTo = contractualDocument.ValidTill;
						category = contractualDocument.DocumentGroup != null ? contractualDocument.DocumentGroup.Name : string.Empty;
					}
					
//			Document.Note = counterpartyRN + " | " + businessUnitRN;
//			Document.Save();
					
					using (OracleCommand command = new OracleCommand())
					{
						command.Connection = connection;
						command.CommandType = System.Data.CommandType.StoredProcedure;
						command.CommandText = "UP_RX_ADD_CONTRACTS";
						
						command.Parameters.Add("REGNUM", Document.RegistrationNumber);
						command.Parameters.Add("DATEDOC", Document.RegistrationDate);
						command.Parameters.Add("NOTE", Document.Subject);
						command.Parameters.Add("DATEFROM", dateFrom);
						command.Parameters.Add("DATETO", dateTo);
						command.Parameters.Add("ORG", counterpartyRN);
						command.Parameters.Add("OURORG", businessUnitRN);
						command.Parameters.Add("USER1", signerINN);
						command.Parameters.Add("USER2", responsibleINN);
						command.Parameters.Add("CATEGORY", category);
						
						if (!string.IsNullOrWhiteSpace(RN))
							command.Parameters.Add("NRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
						else
							command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						try
						{
							command.ExecuteNonQuery();
							
							RN = command.Parameters["NRN"].Value.ToString();
							
							ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
							
							if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
							{
								var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
								var regex = new Regex(@". * Not found. * from update RN =. *");
								if (regex.IsMatch(errMsg))
									Sync_ExternalEntityLink(Document, RN, true);
								RN = errMsg;
							}
							else
								Sync_ExternalEntityLink(Document, RN);
						}
						catch (Exception ex)
						{
							RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
						}
					}
				}
				finally
				{
					if (InSession)
					{
						StopSession_Parus8(connection);
					}
					
					if (Connection == null)
					{
						OracleConnection.ClearPool(connection);
						connection.Close();
						connection.Dispose();
						Logger.DebugFormat("Contract_Parus8: {0}", "Clearing the connection pool and closing it");
					}
				}
			}
			
			return RN;
		}
		
		/// <summary>
		/// Создание или обновление Договора в Парус 8
		/// </summary>
		/// <param name="Contract"></param>
		/// <returns></returns>
		[Remote, Public]
		public static string Sync_Contract_Parus8(Sungero.Docflow.IContractualDocumentBase Document, bool InSession)
		{
			return Sync_Contract_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// Создание или обновление контрагента в Парус 8
		/// </summary>
		/// <param name="Counterpaty"></param>
		/// <returns>RN</returns>
		public static string Sync_Organization_Parus8(ICounterparty Counterpaty, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("Organization_Parus8: {0} from ID {1}", "Start function synchronization ", Counterpaty.Id.ToString());
			var RN = string.Empty;
			var name = string.Empty;
			var inn = string.Empty;
			var kpp = string.Empty;
			var ident = string.Empty;
			OracleConnection connection = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("Organization_Parus8: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			try
			{
				if (Companies.Is(Counterpaty))
				{
					var company = Companies.As(Counterpaty);
					
					name = company.Name;
					inn = company.TIN;
					kpp = company.TRRC;
				}
				else
				{
					name = Counterpaty.Name;
					inn = Counterpaty.TIN;
				}

				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandType = System.Data.CommandType.StoredProcedure;
					command.CommandText = "UP_RX_ADD_AGNLIST";
					
					command.Parameters.Add("sName", name);
					command.Parameters.Add("sINN", inn);
					command.Parameters.Add("sKPP", kpp);
					
					if (TryRN_By_Entity(Counterpaty, out RN))
						command.Parameters.Add("nRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
					else
						command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
					
					command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
					
					try
					{
						command.ExecuteNonQuery();
						
						RN = command.Parameters["nRN"].Value.ToString();
						
						ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
						
						if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
						{
							var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
							var regex = new Regex(@". * Not found. * from update RN =. *");
							if (regex.IsMatch(errMsg))
								Sync_ExternalEntityLink(Counterpaty, RN, true);
							RN = errMsg;
						}
						else
							Sync_ExternalEntityLink(Counterpaty, RN);
					}
					catch (Exception ex)
					{
						RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
					}
				}
			}
			finally
			{
				
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("Organization_Parus8: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return RN;
		}
		
		/// <summary>
		/// Создание или обновление контрагента в Парус 8
		/// </summary>
		/// <param name="Counterpaty"></param>
		/// <returns>RN</returns>
		[Remote, Public]
		public static string Sync_Organization_Parus8(ICounterparty Counterpaty, bool InSession)
		{
			return Sync_Organization_Parus8(Counterpaty, InSession, null);
		}
		
		/// <summary>
		/// Загрузка организации из Парус 8 по RN
		/// </summary>
		[Remote(PackResultEntityEagerly = true), Public]
		public static IBusinessUnit Sync_OurOrganization_RX(string RN)
		{
			return Sync_OurOrganization_RX(RN, true, null);
		}
		
		/// <summary>
		/// Загрузка организации из Парус 8 по RN
		/// </summary>
		public static IBusinessUnit Sync_OurOrganization_RX(string RN, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("OurOrganization: {0} from RN {1}", "Start function synchronization", RN);
			OracleConnection connection = null;
			IBusinessUnit org = null;
			
			if (Connection != null)
			{
				connection = Connection;
			}
			else
			{
				connection = new OracleConnection();
				connection.ConnectionString = GetSettingString("ConnectionString");
				connection.Open();
				Logger.DebugFormat("OurOrganization: {0}", "Open connection");
			}
			
			if (InSession)
			{
				StartSession_Parus8(connection);
			}
			
			try
			{
				using (OracleCommand command = new OracleCommand())
				{
					command.Connection = connection;
					command.CommandText = string.Format("select v.rn \"RN\", v.agnname \"Name\", v.agnidnumb \"INN\", v.sreason_code \"KPP\" from v_agnlist v where v.rn={0}", RN);

					var reader = command.ExecuteReader();
					Logger.DebugFormat("OurOrganization: {0}", "Execution of a query to search for an object in the database");
					if (reader.HasRows)
					{
						Logger.DebugFormat("OurOrganization: {0}", "Value found");
						while (reader.Read())
						{
							var nameOrg = reader["Name"].ToString();
							var inn = reader["INN"].ToString();
							var kpp = reader["KPP"].ToString();
							org = Sync_OurOrganization_RX(RN, nameOrg, inn, kpp);
						}
					}
				}
				
				RemoveChangedObject_Parus8(RN, false, connection);
			}
			finally
			{
				if (InSession)
				{
					StopSession_Parus8(connection);
				}
				
				if (Connection == null)
				{
					OracleConnection.ClearPool(connection);
					connection.Close();
					connection.Dispose();
					Logger.DebugFormat("OurOrganization: {0}", "Clearing the connection pool and closing it");
				}
			}
			
			return org;
		}
		
		/// <summary>
		/// Создание контрагента (Create_ORG_RX)
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <param name="Name">Наименование контрагента</param>
		/// <param name="INN">ИНН контрагента</param>
		/// <param name="KPP">КПП контрагента</param>
		[Remote(PackResultEntityEagerly = true)]
		public static IBusinessUnit Sync_OurOrganization_RX(string RN, string Name, string INN, string KPP)
		{
			Logger.DebugFormat("OurOrganization_RX: RN - {0}, INN - {1} KPP - {2}", RN, INN, KPP);
			IBusinessUnit org = GetOurOrg_By_RN(RN);
			
			if (org == null) //Если есть контрагент обновляем, иначе создаём нового
			{
				org = BusinessUnits.GetAll().Where(e => e.TIN == INN && e.TRRC == KPP).FirstOrDefault();
			}
			
			if (org != null) //Если есть контрагент обновляем, иначе создаём нового
			{
				Logger.DebugFormat("OurOrganization_RX: {0} {1}", "Record found ID", org.Id.ToString());
			}
			else
			{
				org = BusinessUnits.Create();
				Logger.DebugFormat("OurOrganization_RX: {0} {1}", "Record created ID", org.Id.ToString());
			}
			
			org.Name = Name;
			org.TIN = INN;
			org.TRRC = KPP;
			org.Save();
			
			Logger.DebugFormat("OurOrganization_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(org.Company, RN);
			
			return org;
		}
		
		/// <summary>
		/// Создание исходещей счет-фактуры
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="Num">Номер регистрации</param>
		/// <param name="DateDoc">Дата регистрации</param>
		/// <param name="OrgRN">Идентификатор контрагента в Парус 8</param>
		/// <param name="Sum">Сумма</param>
		/// <param name="OurOrgRN"></param>
		/// <param name="ContractRN"></param>
		/// <param name="File"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static IOutgoingTaxInvoice Sync_OutgoingTaxInvoice_RX(string RN, string RegNum, string DateDoc, string OrgRN, string Sum, string OurOrgRN, string ContractRN, string File)
		{
			Logger.DebugFormat("OutgoingTaxInvoice_RX: RN - {0}, RegNum - {1} DateDoc - {2}", RN, RegNum, DateDoc);
			DateTime date0;
			Nullable<DateTime> date = Calendar.TryParseDate(DateDoc, out date0) ? date0 : (DateTime?)null;
			IOutgoingTaxInvoice taxInvoice = OutgoingTaxInvoices.GetAll().Where(e => e.RegistrationNumber == RegNum && Equals(e.RegistrationDate, date)).FirstOrDefault();
			int ID;
			
			if (TryID_By_RN(RN, out ID) || taxInvoice != null) //Если есть контрагент обновляем, иначе создаём нового
			{
				taxInvoice = taxInvoice ?? OutgoingTaxInvoices.Get(ID);
				Logger.DebugFormat("OutgoingTaxInvoice_RX: {0} {1}", "Record found", taxInvoice.Id.ToString());
			}
			else
			{
				taxInvoice = OutgoingTaxInvoices.Create();
				Logger.DebugFormat("OutgoingTaxInvoice_RX: {0} {1}", "Record created", taxInvoice.Id.ToString());
			}
			
			if (!taxInvoice.RegistrationState.HasValue || taxInvoice.RegistrationState.Value == Sungero.FinancialArchive.OutgoingTaxInvoice.RegistrationState.NotRegistered)
			{
				taxInvoice.RegistrationNumber = RegNum;
				
				if (date.HasValue)
					taxInvoice.RegistrationDate = date.Value;
				
				if (!taxInvoice.InternalApprovalState.HasValue
				    || taxInvoice.InternalApprovalState.Value == Sungero.FinancialArchive.OutgoingTaxInvoice.InternalApprovalState.Aborted
				    || taxInvoice.InternalApprovalState.Value == Sungero.FinancialArchive.OutgoingTaxInvoice.InternalApprovalState.OnRework)
					taxInvoice.Counterparty = GetOrg_By_RN(OrgRN);
				
				taxInvoice.BusinessUnit = GetOurOrg_By_RN(OurOrgRN);
			}
			
			int totalAmount;
			if (int.TryParse(Sum, out totalAmount))
				taxInvoice.TotalAmount = totalAmount;
			
			taxInvoice.LeadingDocument = GetContract_By_RN(ContractRN);
			taxInvoice.Save();
			
			Logger.DebugFormat("OutgoingTaxInvoice_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(taxInvoice, RN);
			
			return taxInvoice;
		}
		
		/// <summary>
		/// Создание исходещего счета на оплату
		/// </summary>
		/// <param name="RN">Идентификатор окумента в Парус 8</param>
		/// <param name="Num">Номер регистрации</param>
		/// <param name="DateDoc">Дата регистрации</param>
		/// <param name="OrgRN">Идентификатор контрагента в Парус 8</param>
		/// <param name="Sum">Сумма</param>
		/// <param name="OurOrgRN"></param>
		/// <param name="ContractRN"></param>
		/// <param name="File"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static IOutgoingInvoiceNew Sync_OutgoingInvoice_RX(string RN, string RegNum, string DateDoc, string OrgRN, string Sum, string OurOrgRN, string File)
		{
			Logger.DebugFormat("OutgoingInvoice_RX: RN - {0}, RegNum - {1} DateDoc - {2}", RN, RegNum, DateDoc);
			DateTime date0;
			Nullable<DateTime> date = Calendar.TryParseDate(DateDoc, out date0) ? date0 : (DateTime?)null;
			IOutgoingInvoiceNew invoice = OutgoingInvoiceNews.GetAll().Where(e => e.RegistrationNumber == RegNum && Equals(e.RegistrationDate, date)).FirstOrDefault();
			int ID;
			
			if (TryID_By_RN(RN, out ID) || invoice != null) //Если есть контрагент обновляем, иначе создаём нового
			{
				invoice = invoice ?? OutgoingInvoiceNews.Get(ID);
				Logger.DebugFormat("OutgoingInvoice_RX: {0} {1}", "Record found", invoice.Id.ToString());
			}
			else
			{
				invoice = OutgoingInvoiceNews.Create();
				Logger.DebugFormat("OutgoingInvoice_RX: {0} {1}", "Record created", invoice.Id.ToString());
			}
			
			if (!invoice.RegistrationState.HasValue || invoice.RegistrationState.Value == Sungero.Contracts.Contract.RegistrationState.NotRegistered)
			{
//				invoice.RegistrationNumber = RegNum;
//
//				if (date.HasValue)
//					invoice.RegistrationDate = date.Value;
				
				invoice.Number = RegNum;
				
				if (date.HasValue)
					invoice.Date = date.Value;
				
//				if (date.HasValue)
//					invoice.RegistrationDate = date.Value;
				
//				invoice.Subject = "_";
				
				if (!invoice.InternalApprovalState.HasValue
				    || invoice.InternalApprovalState.Value == syscon.Parus8.OutgoingInvoice.InternalApprovalState.Aborted
				    || invoice.InternalApprovalState.Value == syscon.Parus8.OutgoingInvoice.InternalApprovalState.OnRework)
				{
					invoice.Counterparty = GetOrg_By_RN(OrgRN);
					
					int totalAmount;
					if (int.TryParse(Sum, out totalAmount))
						invoice.TotalAmount = totalAmount;
				}
				
				invoice.BusinessUnit = GetOurOrg_By_RN(OurOrgRN);
			}
			
//			invoice.Contract = GetContract_By_RN(ContractRN);
			invoice.Save();
			
			Logger.DebugFormat("OutgoingInvoice_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(invoice, RN);
			
			return invoice;
		}
		
		/// <summary>
		/// Создание или обновление Входящей счет-фактуры в Парус 8
		/// </summary>
		/// <param name="Document">Объект документа</param>
		/// <returns>RN</returns>
		[Remote, Public]
		public static string Sync_IncomingTaxInvoice_Parus8(syscon.SC.IIncomingTaxInvoice Document, bool InSession)
		{
			return Sync_IncomingTaxInvoice_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// Создание или обновление Входящей счет-фактуры в Парус 8
		/// </summary>
		/// <param name="Document">Объект документа</param>
		/// <returns>RN</returns>
		public static string Sync_IncomingTaxInvoice_Parus8(syscon.SC.IIncomingTaxInvoice Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("IncomingTaxInvoice_Parus8: {0} from ID {1}", "Start function synchronization ", Document.Id.ToString());
			var RN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			var personAccount = string.Empty;
			var ident = string.Empty;
			OracleConnection connection = null;
			
			if (TryRN_By_Entity(Document, out RN) || string.IsNullOrEmpty(RN))
			{
				if (Connection != null)
				{
					connection = Connection;
				}
				else
				{
					connection = new OracleConnection();
					connection.ConnectionString = GetSettingString("ConnectionString");
					connection.Open();
					Logger.DebugFormat("IncomingTaxInvoice_Parus8: {0}", "Open connection");
				}
				
				if (InSession)
				{
					StartSession_Parus8(connection);
				}
				
				try
				{
					if (Document.LeadingDocument != null && !TryRN_By_Entity(Document.LeadingDocument, out contractRN))
					{
						contractRN = Sync_Contract_Parus8(Document.LeadingDocument, false, connection);
						if (!Functions.Module.CheckRN(contractRN))
							return contractRN;
					}
					
					if (!TryRN_By_Entity(Document.Counterparty, out counterpartyRN))
					{
						counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
						if (!Functions.Module.CheckRN(counterpartyRN))
							return counterpartyRN;
					}
					
					if (!TryRN_By_Entity(Document.BusinessUnit.Company, out businessUnitRN))
					{
						businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company, false, connection);
						if (!Functions.Module.CheckRN(businessUnitRN))
							return businessUnitRN;
					}
					
					if (Document.IncInvoiceTIKS != null)
					{
						var incInvoice = syscon.SC.IncomingInvoices.As(Document.IncInvoiceTIKS);
						personAccount = incInvoice.PersonAccountKS;
					}

					using (OracleCommand command = new OracleCommand())
					{
						command.Connection = connection;
						command.CommandType = System.Data.CommandType.StoredProcedure;
						command.CommandText = "UP_RX_ADD_DICACCFI";
						
						command.Parameters.Add("NUM", Document.RegistrationNumber);
						command.Parameters.Add("DATEDOC", Document.RegistrationDate);
						command.Parameters.Add("SUM1", Document.TotalAmount);
						command.Parameters.Add("ORG", counterpartyRN);
						command.Parameters.Add("OURORG", businessUnitRN);
						command.Parameters.Add("CONTRACT", contractRN);
						command.Parameters.Add("PERSACC", personAccount);

						if (!string.IsNullOrWhiteSpace(RN))
							command.Parameters.Add("NRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
						else
							command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						try
						{
							command.ExecuteNonQuery();
							
							RN = command.Parameters["NRN"].Value.ToString();
							
							ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
							
							if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
							{
								var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
								var regex = new Regex(@". * Not found. * from update RN =. *");
								if (regex.IsMatch(errMsg))
									Sync_ExternalEntityLink(Document, RN, true);
								RN = errMsg;
							}
							else
								Sync_ExternalEntityLink(Document, RN);
						}
						catch (Exception ex)
						{
							RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
						}
					}
				}
				finally
				{
					if (InSession)
					{
						StopSession_Parus8(connection);
					}
					
					if (Connection == null)
					{
						OracleConnection.ClearPool(connection);
						connection.Close();
						connection.Dispose();
						
						Logger.DebugFormat("IncomingTaxInvoice_Parus8: {0}", "Clearing the connection pool and closing it");
					}
				}
			}
			
			return RN;
		}
		
		/// <summary>
		/// Создание или обновление УПД в Парус 8
		/// </summary>
		/// <param name="Document">Объект документа</param>
		/// <returns>RN</returns>
		[Remote, Public]
		public static string Sync_UniversalTransferDocument_Parus8(syscon.SC.IUniversalTransferDocument Document, bool InSession)
		{
			return Sync_UniversalTransferDocument_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// Создание или обновление УПД в Парус 8
		/// </summary>
		/// <param name="Document">Объект документа</param>
		/// <returns>RN</returns>
		public static string Sync_UniversalTransferDocument_Parus8(syscon.SC.IUniversalTransferDocument Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("Sync_UniversalTransferDocument_Parus8: {0} from ID {1}", "Start function synchronization ", Document.Id.ToString());
			var RN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			var personAccount = string.Empty;
			var ident = string.Empty;
			OracleConnection connection = null;
			
			if (TryRN_By_Entity(Document, out RN) || string.IsNullOrEmpty(RN))
			{
				if (Connection != null)
				{
					connection = Connection;
				}
				else
				{
					connection = new OracleConnection();
					connection.ConnectionString = GetSettingString("ConnectionString");
					connection.Open();
					Logger.DebugFormat("Sync_UniversalTransferDocument_Parus8: {0}", "Open connection");
				}
				
				if (InSession)
				{
					StartSession_Parus8(connection);
				}
				
				try
				{
					if (Document.LeadingDocument != null && !TryRN_By_Entity(Document.LeadingDocument, out contractRN))
					{
						contractRN = Sync_Contract_Parus8(Document.LeadingDocument, false, connection);
						if (!Functions.Module.CheckRN(contractRN))
							return contractRN;
					}
					
					if (!TryRN_By_Entity(Document.Counterparty, out counterpartyRN))
					{
						counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
						if (!Functions.Module.CheckRN(counterpartyRN))
							return counterpartyRN;
					}
					
					if (!TryRN_By_Entity(Document.BusinessUnit.Company, out businessUnitRN))
					{
						businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company, false, connection);
						if (!Functions.Module.CheckRN(businessUnitRN))
							return businessUnitRN;
					}
					
					if (Document.IncInvoiceUTKS != null)
					{
						var incInvoice = syscon.SC.IncomingInvoices.As(Document.IncInvoiceUTKS);
						personAccount = incInvoice.PersonAccountKS;
					}

					using (OracleCommand command = new OracleCommand())
					{
						command.Connection = connection;
						command.CommandType = System.Data.CommandType.StoredProcedure;
						command.CommandText = "UP_RX_ADD_DICACCFI";
						
						command.Parameters.Add("NUM", Document.RegistrationNumber);
						command.Parameters.Add("DATEDOC", Document.RegistrationDate);
						command.Parameters.Add("SUM1", Document.TotalAmount);
						command.Parameters.Add("ORG", counterpartyRN);
						command.Parameters.Add("OURORG", businessUnitRN);
						command.Parameters.Add("CONTRACT", contractRN);
						command.Parameters.Add("PERSACC", personAccount);

						if (!string.IsNullOrWhiteSpace(RN))
							command.Parameters.Add("NRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
						else
							command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						try
						{
							command.ExecuteNonQuery();
							
							RN = command.Parameters["NRN"].Value.ToString();
							
							ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
							
							if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
							{
								var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
								var regex = new Regex(@". * Not found. * from update RN =. *");
								if (regex.IsMatch(errMsg))
									Sync_ExternalEntityLink(Document, RN, true);
								RN = errMsg;
							}
							else
								Sync_ExternalEntityLink(Document, RN);
						}
						catch (Exception ex)
						{
							RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
						}
					}
				}
				finally
				{
					if (InSession)
					{
						StopSession_Parus8(connection);
					}
					
					if (Connection == null)
					{
						OracleConnection.ClearPool(connection);
						connection.Close();
						connection.Dispose();
						
						Logger.DebugFormat("Sync_UniversalTransferDocument_Parus8: {0}", "Clearing the connection pool and closing it");
					}
				}
			}
			
			return RN;
		}
		
		/// <summary>
		/// Возвращает контрагента по его RN
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <returns></k>
		public static ICompany GetOrg_By_RN(string RN)
		{
			int ID = ID_By_RN(RN);
			ICompany company = ID > 0 ? Companies.Get(ID) : null;
			
			if (company != null)
				Logger.DebugFormat("GetOrg_By_RN: {0} {1}", "Record found", ID.ToString());
			else
				Logger.DebugFormat("GetOrg_By_RN: {0}", "Record not found");
			
			return company;
		}
		
		/// <summary>
		/// Делает поиск нашей организации по организации
		/// </summary>
		/// <param name="ID">ИД организации</param>
		/// <returns>Ссылка на запись справочника "наши организации"</returns>
		public static IBusinessUnit GetOurOrg_By_RN(string RN)
		{
			var company = GetOrg_By_RN(RN);
			IBusinessUnit org = company != null ? BusinessUnits.GetAll().Where(e => Equals(e.Company, company)).FirstOrDefault() : null;
			
			if (org != null)
				Logger.DebugFormat("GetOurOrg_By_RN: {0} {1}", "Record found", org.Id.ToString());
			else
				Logger.DebugFormat("GetOurOrg_By_RN: {0}", "Record not found");
			
			return org;
			
//			Sungero.Parties.ICompany company = GetOrg_By_RN(RN);
//			return Sungero.Company.BusinessUnits.GetAll().FirstOrDefault(x => Equals(x.Company,company));
		}
		
		/// <summary>
		/// Возвращает контрагента по его RN
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		/// <returns></k>
		public static IContract GetContract_By_RN(string RN)
		{
			int ID = ID_By_RN(RN);
			var contract = ID > 0 ? Contracts.Get(ID) : null;
			
			Logger.DebugFormat("GetContract_By_RN: {0} {1}", "Record found", ID.ToString());
			
			return contract;
		}

//		/// <summary>
//		/// Запрашивает данные контрагента по его RN из Парус 8
//		/// </summary>
//		/// <param name="RN">RN контрагента
//		/// </param>
//		/// <returns></returns>
//		public static Sungero.Parties.ICompany return_org_Parus8(string RN)
//		{
//			//В Парус 8 будет хранимая процедур, которая возвращает реквизиты контрагента по его RN
//			return null;
//		}

		/// <summary>
		/// Возвоазает ID по RN из таблицы ExternalLink
		/// </summary>
		/// <param name="RN"></param>
		/// <returns>ИД записи</returns>
		public static int ID_By_RN(string RN)
		{
			int result = 0;
			var links = Sungero.Commons.ExternalEntityLinks
				.GetAll()
				.Where(e => e.ExtEntityId == RN)
				.FirstOrDefault();
			
			if (links != null)
			{
				result = links.EntityId.Value;
			}
			
			return result;
		}
		
		/// <summary>
		/// Возвращает ID нашей оорганизации по RN из таблицы ExternalLink
		/// </summary>
		/// <param name="RN"></param>
		/// <returns>ИД записи</returns>
		public static int OurOrgID_By_RN(string RN)
		{
			int result = 0;
			var links = Sungero.Commons.ExternalEntityLinks
				.GetAll()
				.Where(e => e.ExtEntityId == RN
				       && e.EntityType == "eff95720-181f-4f7d-892d-dec034c7b2ab")
				.OrderByDescending(e => e.Id)
				.FirstOrDefault();
			
			if (links != null)
			{
				result = links.EntityId.Value;
			}
			return result;
		}
		
		public static string RN_By_Entity(IEntity entity)
		{
			var link = GetLink(entity);
			
			return link != null ? link.ExtEntityId : string.Empty;
		}

		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <returns> RN записи</returns>
		public static string RN_By_ID_Ref(int ID)
		{
			var result = "0";
			var links = Sungero.Commons.ExternalEntityLinks.GetAll()
				.Where(e => e.EntityId == ID)
				.ToList();
			
			foreach (var link in links)
			{
				if (new System.Guid(link.EntityType).GetBaseRootGuid().GetTypeByGuid().Name != "IElectronicDocument")
				{
					if (link.IsDeleted.Value)
						result = "Запись справочника была удалена на стороне Парус 8. Дальнейшая синхронизация возможна только после снятия отметки об удалении в справочнике синхронизации";
					else
						result = link.ExtEntityId;
					break;
				}
			}
			
			return !string.IsNullOrWhiteSpace(result) ? result : string.Empty;
		}
		
		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <returns> RN записи</returns>
		public static string RN_By_ID_Doc(int ID)
		{
			var result = string.Empty;
			var links = Sungero.Commons.ExternalEntityLinks.GetAll()
				.Where(e => e.EntityId == ID)
				.OrderByDescending(e => e.Id)
				.ToList();
			
			foreach (var link in links)
			{
				if (new System.Guid(link.EntityType).GetBaseRootGuid().GetTypeByGuid().Name == "IElectronicDocument")
				{
					if (link.IsDeleted.Value)
						result = "Документ был удален на стороне Парус 8. Дальнейшая синхронизация возможна только после снятия отметки об удалении в справочнике синхронизации";
					else
						result = link.ExtEntityId;
					break;
				}
			}
			
			return !string.IsNullOrWhiteSpace(result) ? result : string.Empty;
		}
		
		/// <summary>
		/// Возвращает ID по RN из таблицы ExternalLink
		/// </summary>
		/// <param name="RN"></param>
		/// <param name="ID"></param>
		/// <returns></returns>
		private static bool TryID_By_RN(string RN, out int ID)
		{
			var result = true;
			ID = ID_By_RN(RN);
			
			if (ID == 0)
				result = false;
			
			Logger.DebugFormat("TryID_By_RN: {0} {1}", "Поиск вернул запись справочника с ID", ID.ToString());
			
			return result;
		}
		
		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="RN"></param>
		/// <returns></returns>
		private static bool TryRN_By_Entity(IEntity entity, out string RN)
		{
			decimal dRN = 0;
			var result = false;
			RN = RN_By_Entity(entity);
			if (Decimal.TryParse(RN, out dRN))
			{
				result = true;
			}
			
			Logger.DebugFormat("TryRN_By_Entity: {0} {1}", "Поиск вернул запись справочника с RN", RN);
			
			return result;
		}

		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="RN"></param>
		/// <returns></returns>
		private static bool TryRN_By_ID_Ref(int ID, out string RN)
		{
			decimal dRN = 0;
			var result = false;
			RN = RN_By_ID_Ref(ID);
			if (Decimal.TryParse(RN, out dRN))
			{
				result = true;
			}
			
			Logger.DebugFormat("TryRN_By_ID_Ref: {0} {1}", "Поиск вернул запись справочника с RN", RN);
			
			return result;
		}
		
		/// <summary>
		/// Возвращает RN по ID из таблицы ExternalLink
		/// </summary>
		/// <param name="ID"></param>
		/// <param name="RN"></param>
		/// <returns></returns>
		private static bool TryRN_By_ID_Doc(int ID, out string RN)
		{
			decimal dRN = 0;
			var result = false;
			RN = RN_By_ID_Doc(ID);
			
			if (Decimal.TryParse(RN, out dRN))
			{
				result = true;
			}
			
			Logger.DebugFormat("TryRN_By_ID_Doc: {0} {1}", "Record found документа с RN", RN);
			
			return result;
		}
		
//		[Remote]
//		public static string TrySinc(int ID)
//		{
//			var result = string.Empty;
//			var RN = string.Empty;
//			var links = Sungero.Commons.ExternalEntityLinks.GetAll()
//				.Where(e => e.Id == ID)
//				.FirstOrDefault();
//
//			if (links != null)
//			{
		////				 Sungero.Content.ElectronicDocuments.
		////				result += Sungero.Content.IElectronicDocument.Equals(new System.Guid(links.EntityType).GetBaseRootGuid().GetTypeByGuid()).ToString();
//				result += (new System.Guid(links.EntityType).GetBaseRootGuid().GetTypeByGuid().FullName != "Sungero.Content.IElectronicDocument").ToString();
//				result += new System.Guid(links.EntityType).GetTypeByGuid().FullName + " | ";
		////				result += new System.Guid(links.EntityType).GetBaseRootGuid().GetTypeByGuid().Equals().FullName + " | ";
//				result += new System.Guid(links.EntityType).GetOriginalTypeGuid().GetTypeByGuid().FullName + " | ";
		////				result += new System.Guid(links.EntityType).GetTypeByGuid().BaseType.FullName + " | ";
//				RN = links.ExtEntityId;
//			}
//			else
//			{
		////				result = false;
//			}
//
//			return result;
//		}
		
		/// <summary>
		/// Создание и обновление связи объектов в ExternalEntityLink
		/// </summary>
		/// <param name="ID">ID синхронизируемого объекта</param>
		/// <param name="Type"></param>
		/// <param name="RN"></param>
		/// <param name="ExType"></param>
		public static void Sync_ExternalEntityLink(IEntity entity, string RN, bool IsDeleted = false)
		{
			var metadata = entity.GetEntityMetadata();
			
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(e => e.ExtEntityId == RN);
			if (link == null)
			{
				link = Sungero.Commons.ExternalEntityLinks.Create();
			}
			
			link.EntityType = metadata.NameGuid.ToString();
			link.EntityId = entity.Id;
			link.ExtEntityType = metadata.GetSingularDisplayName();
			link.ExtEntityId = RN;
			link.IsDeleted = IsDeleted;
			
			var syncDate = Calendar.Now;
			var syncDateProperty = metadata.Properties.FirstOrDefault(e => e.Name == "SyncDateKS");
			if (syncDateProperty != null)
			{
				syncDateProperty.SetPropertyValue(entity, syncDate);
				entity.Save();
			}
			link.SyncDate = syncDate;
			link.Save();
			
			Logger.DebugFormat("Sync_ExternalEntityLink: {0} {1}", "Create entity", link.Id.ToString());
		}

		/// <summary>
		/// Создание и обновление связи объектов в ExternalEntityLink
		/// </summary>
		/// <param name="ID">ID синхронизируемого объекта</param>
		/// <param name="Type"></param>
		/// <param name="RN"></param>
		/// <param name="ExType"></param>
		public static void Sync_ExternalEntityLink1(int ID, string Type, string RN, string ExType, bool IsDeleted = false)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(e => e.ExtEntityId == RN);
			if (link == null)
			{
				link = Sungero.Commons.ExternalEntityLinks.Create();
			}
			
			link.EntityType = Type;
			link.EntityId = ID;
			link.ExtEntityType = ExType;
			link.ExtEntityId = RN;
			link.IsDeleted = IsDeleted;
			
			link.SyncDate = Calendar.Now;
			link.Save();
			
			Logger.DebugFormat("Sync_ExternalEntityLink: {0} {1}", "Create entity", link.Id.ToString());
		}

		/// <summary>
		/// Записывает в справочник ExternalEntityLink время синхронизации объекта
		/// </summary>
		/// <param name="ID">ID синхронизируемого объекта</param>
		public static void Sync_ExternalEntityLink(string RN)
		{
			var link = Sungero.Commons.ExternalEntityLinks.GetAll().FirstOrDefault(e => e.ExtEntityId == RN);
			if (link != null)
			{
				link.SyncDate = Calendar.Now;
				link.Save();
			}
			
			Logger.DebugFormat("Sync_ExternalEntityLink: {0} {1}", "Sync date", link.Id.ToString());
		}

		/// <summary>
		/// Вернуть или создать категорию договора при ее отсутствии
		/// </summary>
		/// <param name="Category"></param>
		/// <returns></returns>
		[Remote(PackResultEntityEagerly = true)]
		public static Sungero.Contracts.IContractCategory Create_Category(string Category)
		{
			IContractCategory category = Sungero.Contracts.ContractCategories.GetAll().FirstOrDefault(x => String.Equals(x.Name,Category,StringComparison.CurrentCultureIgnoreCase));
			if (category != null) //Если есть категория, возьмём её
			{
				return category;
			}
			else
			{
				category = Sungero.Contracts.ContractCategories.Create();
				category.Name = Category;
				category.Save();
				
				return category;
			}
		}
		
		/// <summary>
		/// Создание или обновление документа Акта выполненных работ в системе Парус 8 (Приходной ордер)
		/// </summary>
		/// <param name="Document"></param>
		[Remote, Public]
		public static string Sync_AccountingDocument_Parus8(Sungero.Docflow.IAccountingDocumentBase Document, bool InSession)
		{
			return Sync_AccountingDocument_Parus8(Document, InSession, null);
		}
		
		/// <summary>
		/// Создание или обновление документа Акта выполненных работ в системе Парус 8 (Приходной ордер)
		/// </summary>
		/// <param name="Document"></param>
		public static string Sync_AccountingDocument_Parus8(Sungero.Docflow.IAccountingDocumentBase Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
		{
			Logger.DebugFormat("AccountingDocument_Parus8: {0} from ID {1}", "Start function synchronization ", Document.Id.ToString());
			var RN = string.Empty;
			var contractRN = string.Empty;
			var counterpartyRN = string.Empty;
			var businessUnitRN = string.Empty;
			var incomingInvoiceRN = string.Empty;
			var personAccount = string.Empty;
			var ident = string.Empty;
			var type = string.Empty;
			OracleConnection connection = null;
			
			if (TryRN_By_Entity(Document, out RN) || string.IsNullOrEmpty(RN))
			{
				if (Connection != null)
				{
					connection = Connection;
				}
				else
				{
					connection = new OracleConnection();
					connection.ConnectionString = GetSettingString("ConnectionString");
					connection.Open();
					Logger.DebugFormat("AccountingDocument_Parus8: {0}", "Open connection");
				}
				
				if (InSession)
				{
					StartSession_Parus8(connection);
				}
				
				try
				{
					if (Document.LeadingDocument != null && !TryRN_By_Entity(Document.LeadingDocument, out contractRN))
					{
						contractRN = Sync_Contract_Parus8(Document.LeadingDocument, false, connection);
						if (!Functions.Module.CheckRN(contractRN))
							return contractRN;
					}
					
					if (!TryRN_By_Entity(Document.Counterparty, out counterpartyRN))
					{
						counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
						if (!Functions.Module.CheckRN(counterpartyRN))
							return counterpartyRN;
					}
					
					if (!TryRN_By_Entity(Document.BusinessUnit.Company, out businessUnitRN))
					{
						businessUnitRN  = Sync_Organization_Parus8(Document.BusinessUnit.Company, false, connection);
						if (!Functions.Module.CheckRN(businessUnitRN))
							return businessUnitRN;
					}
					
					if (syscon.SC.ContractStatements.Is(Document))
					{
						type = "Акт";
						var documentCS = syscon.SC.ContractStatements.As(Document);
						
						if (documentCS.IncInvoiceCSKS != null)
						{
							var incInvoice = syscon.SC.IncomingInvoices.As(documentCS.IncInvoiceCSKS);
							personAccount = incInvoice.PersonAccountKS;
						}
					}
					
					if (syscon.SC.Waybills.Is(Document))
					{
						type = "Накладная";
						var documentWB = syscon.SC.Waybills.As(Document);
						
						if (documentWB.IncInvoiceWBKS != null)
						{
							var incInvoice = syscon.SC.IncomingInvoices.As(documentWB.IncInvoiceWBKS);
							personAccount = incInvoice.PersonAccountKS;
						}
					}
					
					var author = Employees.As(Document.Author);
					var authorINN = (author != null && author.Person != null) ? author.Person.TIN : string.Empty;
					
//			Document.Note = counterpartyRN + " | " + businessUnitRN + " | " + contractRN;
//			Document.Save();
					
					using (OracleCommand command = new OracleCommand())
					{
						command.Connection = connection;
						command.CommandType = System.Data.CommandType.StoredProcedure;
						command.CommandText = "UP_RX_ADD_INORDERS";
						
						command.Parameters.Add("NUM", Document.RegistrationNumber);
						command.Parameters.Add("DATEDOC", Document.RegistrationDate);
						command.Parameters.Add("ORG", counterpartyRN);
						command.Parameters.Add("SUM1", Document.TotalAmount);
						command.Parameters.Add("OURORG", businessUnitRN);
						command.Parameters.Add("TYPE1", type);
						command.Parameters.Add("CONTRACT", contractRN);
						command.Parameters.Add("AUTHOR", authorINN);
						command.Parameters.Add("PERSACC", personAccount);

						if (!string.IsNullOrWhiteSpace(RN))
							command.Parameters.Add("NRN", OracleDbType.Decimal, RN, ParameterDirection.InputOutput);
						else
							command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						command.Parameters.Add("nIDENT", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
						
						try
						{
							command.ExecuteNonQuery();
							
							RN = command.Parameters["NRN"].Value.ToString();
							
							ident = command.Parameters["nIDENT"].Value != null ? command.Parameters["nIDENT"].Value.ToString() : string.Empty;
							
//						Document.Note = RN + " | " + ident;
//						Document.Save();
							
							if (!string.IsNullOrWhiteSpace(ident) && ident != "null")
							{
								var errMsg = GetErrMsgLog_Parus8(ident, false, connection);
								var regex = new Regex(@". * Not found. * from update RN =. *");
								if (regex.IsMatch(errMsg))
									Sync_ExternalEntityLink(Document, RN, true);
								RN = errMsg;
							}
							else
								Sync_ExternalEntityLink(Document, RN);
						}
						catch (Exception ex)
						{
							RN = string.Format("Возникло необработанное исключение при синхронизации документа:\r\n{0}\r\nОбратитесь к системному администратору", ex.Message);
						}
					}
				}
				finally
				{
					if (InSession)
					{
						StopSession_Parus8(connection);
					}
					
					if (Connection == null)
					{
						OracleConnection.ClearPool(connection);
						connection.Close();
						connection.Dispose();
						Logger.DebugFormat("AccountingDocument_Parus8: {0}", "Clearing the connection pool and closing it");
					}
				}
			}
			
			return RN;
		}
		
//		/// <summary>
//		/// Создание или обновление документа накладной в системе Парус 8
//		/// </summary>
//		/// <param name="Document"></param>
//		[Remote, Public]
//		public static string Sync_Waybill_Parus8(IWaybill Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
//		{
//			var RN = string.Empty;
//			var contractRN = string.Empty;
//			var counterpartyRN = string.Empty;
//			var businessUnitRN = string.Empty;
//			OracleConnection connection = null;
//
//			if (Connection != null)
//			{
//				connection = Connection;
//			}
//			else
//			{
//				connection = new OracleConnection();
//				connection.ConnectionString = GetSettingString("ConnectionString");
//				connection.Open();
//			}
//
//			if (InSession)
//			{
//				StartSession_Parus8(connection);
//			}
//
//			if (!TryRN_By_ID_Doc(Document.LeadingDocument.Id,out contractRN))
//				contractRN = Sync_Contract_Parus8(Document.LeadingDocument, false, connection);
//
//			if (!TryRN_By_ID_Ref(Document.Counterparty.Id, out counterpartyRN))
//				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
//
//			if (!TryRN_By_ID_Ref(Document.BusinessUnit.Id, out businessUnitRN))
//				businessUnitRN  = Sync_OurOrganization_Parus8(Document.BusinessUnit, false, connection);
//
//			using (OracleCommand command = new OracleCommand())
//			{
//				command.Connection = connection;
//				command.CommandType = System.Data.CommandType.StoredProcedure;
//				command.CommandText = "XXX";
//
//				command.Parameters.Add("sParam0", Document.RegistrationNumber);
//				command.Parameters.Add("sParam1", Document.RegistrationDate);
//				command.Parameters.Add("sParam2", Document.TotalAmount);
//				command.Parameters.Add("sParam3", contractRN);
//				command.Parameters.Add("sParam4", counterpartyRN);
//				command.Parameters.Add("sParam5", businessUnitRN);
//
//				if (TryRN_By_ID_Doc(Document.Id, out RN))
//					command.Parameters.Add("nRN", OracleDbType.Decimal, Decimal.Parse(RN), ParameterDirection.InputOutput);
//				else
//					command.Parameters.Add("nRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
//
//				command.ExecuteNonQuery();
//
//				RN = command.Parameters["nRN"].Value.ToString();
//
//				Sync_ExternalEntityLink(Document.Id, "167d0859-1aa3-4d7b-934a-bfc4b0a8c7a1", RN, "Накладная");
//			}
//
//			if (InSession)
//			{
//				StopSession_Parus8(connection);
//			}
//
//			if (Connection == null)
//			{
//				OracleConnection.ClearPool(connection);
//				connection.Close();
//				connection.Dispose();
//			}
//
//			return RN;
//		}
		
		/// <summary>
		/// Создание или обновление документа накладной в системе Directum RX
		/// </summary>
		/// <param name="Document"></param>
		[Remote(PackResultEntityEagerly = true)]
		public static IWaybill Sync_Waybill_RX(string RN, string RegNum, string DateDoc,
		                                       string OrgRN, string OurOrgRN, string ContractRN, string File)
		{
			Logger.DebugFormat("Waybill_RX: RN - {0}, RegNum - {1} DateDoc - {2}", RN, RegNum, DateDoc);
			DateTime date0;
			Nullable<DateTime> date = Calendar.TryParseDate(DateDoc, out date0) ? date0 : (DateTime?)null;
			IWaybill waybill = Waybills.GetAll().Where(e => e.RegistrationNumber == RegNum && Equals(e.RegistrationDate, date)).FirstOrDefault();
			int ID;
			
			if (TryID_By_RN(RN, out ID) || waybill != null) //Если есть акт обновляем, иначе создаём нового
			{
				waybill = waybill ?? Waybills.Get(ID);
				Logger.DebugFormat("Waybill_RX: {0} {1}", "Record found ", waybill.Id.ToString());
			}
			else
			{
				waybill = Waybills.Create();
				Logger.DebugFormat("Waybill_RX: {0} {1}", "Record created ", waybill.Id.ToString());
			}
			
			if (!waybill.RegistrationState.HasValue || waybill.RegistrationState.Value == Sungero.FinancialArchive.Waybill.RegistrationState.NotRegistered)
			{
				waybill.RegistrationNumber = RegNum;
				
				if (date.HasValue)
					waybill.RegistrationDate = date.Value;
				
				if (!waybill.InternalApprovalState.HasValue
				    || waybill.InternalApprovalState.Value == Sungero.FinancialArchive.Waybill.InternalApprovalState.Aborted
				    || waybill.InternalApprovalState.Value == Sungero.FinancialArchive.Waybill.InternalApprovalState.OnRework)
					waybill.Counterparty = GetOrg_By_RN(OrgRN);
				
				waybill.BusinessUnit = GetOurOrg_By_RN(OurOrgRN);
			}
			
//			int totalAmount;
//			if (int.TryParse(Sum, out totalAmount))
//				contractStatement.TotalAmount = totalAmount;
			
			waybill.LeadingDocument = GetContract_By_RN(ContractRN);
			waybill.Save();
			
			Logger.DebugFormat("Waybill_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(waybill, RN);
			
			return waybill;
		}
		
//		/// <summary>
//		/// Создание или обновление УПД в системе Парус 8
//		/// </summary>
//		/// <param name="Document"></param>
//		[Remote, Public]
//		public static string Sync_UniversalTransferDocument_Parus8(IUniversalTransferDocument Document, bool InSession, Oracle.DataAccess.Client.OracleConnection Connection)
//		{
//			var RN = string.Empty;
//			var contractRN = string.Empty;
//			var counterpartyRN = string.Empty;
//			var businessUnitRN = string.Empty;
//			OracleConnection connection = null;
//
//			if (Connection != null)
//			{
//				connection = Connection;
//			}
//			else
//			{
//				connection = new OracleConnection();
//				connection.ConnectionString = GetSettingString("ConnectionString");
//				connection.Open();
//			}
//
//			if (InSession)
//			{
//				StartSession_Parus8(connection);
//			}
//
//			if (!TryRN_By_ID_Doc(Document.Contact.Id,out contractRN))
//				contractRN = Sync_Contract_Parus8(Document.LeadingDocument, false, connection);
//
//			if (!TryRN_By_ID_Ref(Document.Counterparty.Id, out counterpartyRN))
//				counterpartyRN = Sync_Organization_Parus8(Document.Counterparty, false, connection);
//
//			if (!TryRN_By_ID_Ref(Document.BusinessUnit.Id, out businessUnitRN))
//				businessUnitRN  = Sync_OurOrganization_Parus8(Document.BusinessUnit, false, connection);
//
		////			Document.Id;
		////			Document.RegistrationNumber;
		////			Document.RegistrationDate.ToString("d");
		////			Document.Counterparty;
		////			Document.BusinessUnit;
		////			Document.LeadingDocument;
//
//			using (OracleCommand command = new OracleCommand())
//			{
//				command.Connection = connection;
//				command.CommandType = System.Data.CommandType.StoredProcedure;
//				command.CommandText = "XXX";
//
//				command.Parameters.Add("sParam0", Document.RegistrationNumber);
//				command.Parameters.Add("sParam1", Document.RegistrationDate);
//				command.Parameters.Add("sParam2", Document.TotalAmount);
//				command.Parameters.Add("sParam3", contractRN);
//				command.Parameters.Add("sParam4", counterpartyRN);
//				command.Parameters.Add("sParam5", businessUnitRN);
//
//				if (TryRN_By_ID_Doc(Document.Id, out RN))
//					command.Parameters.Add("NRN", OracleDbType.Decimal, Decimal.Parse(RN), ParameterDirection.InputOutput);
//				else
//					command.Parameters.Add("NRN", OracleDbType.Decimal, System.Data.ParameterDirection.Output);
//
//				command.ExecuteNonQuery();
//
//				RN = command.Parameters["NRN"].Value.ToString();
//
//				Sync_ExternalEntityLink(Document.Id, "bc6b4e2e-6299-4508-ad08-8745ab02ae8a", RN, "Универсальный передаточный документ");
//			}
//
//			if (InSession)
//			{
//				StopSession_Parus8(connection);
//			}
//
//			if (Connection == null)
//			{
//				OracleConnection.ClearPool(connection);
//				connection.Close();
//				connection.Dispose();
//			}
//
//			return RN;
//		}

		/// <summary>
		/// Создание или обновление УПД в системе Directum RX
		/// </summary>
		/// <param name="Document"></param>
		[Remote(PackResultEntityEagerly = true)]
		public static IUniversalTransferDocument Sync_UniversalTransferDocument_RX(string RN, string RegNum, string DateDoc,
		                                                                           string OrgRN, string OurOrgRN, string ContractRN, string File)
		{
			IUniversalTransferDocument universalTransferDocument = null;
			int ID;
			
			if (TryID_By_RN(RN, out ID)) //Если есть УПД обновляем, иначе создаём нового
				universalTransferDocument = UniversalTransferDocuments.Get(ID);
			else
				universalTransferDocument = UniversalTransferDocuments.Create();
			
			universalTransferDocument.RegistrationNumber = RegNum;
			
			DateTime date;
			if (Calendar.TryParseDate(DateDoc, out date))
				universalTransferDocument.RegistrationDate =  date;
			
//			int totalAmount;
//			if (int.TryParse(Sum, out totalAmount))
//				contractStatement.TotalAmount = totalAmount;
			
			universalTransferDocument.Counterparty = GetOrg_By_RN(OrgRN);
			universalTransferDocument.BusinessUnit = GetOurOrg_By_RN(OurOrgRN);
			universalTransferDocument.LeadingDocument = GetContract_By_RN(ContractRN);
			universalTransferDocument.Save();
			
			Sync_ExternalEntityLink(universalTransferDocument, RN);
			
			return universalTransferDocument;
		}
		
		/// <summary>
		/// Создание акта выполненных работ в Directum RX
		/// </summary>
		/// <param name="RN">RN контрагента</param>
		[Remote(PackResultEntityEagerly = true)]
		public static IContractStatement Sync_ContractStatement_RX(string RN, string RegNum, string DateDoc,
		                                                           string OrgRN, string OurOrgRN, string ContractRN, string File)
		{
			Logger.DebugFormat("ContractStatement_RX: RN - {0}, RegNum - {1} DateDoc - {2}", RN, RegNum, DateDoc);
			DateTime date0;
			Nullable<DateTime> date = Calendar.TryParseDate(DateDoc, out date0) ? date0 : (DateTime?)null;
			IContractStatement contractStatement = ContractStatements.GetAll().Where(e => e.RegistrationNumber == RegNum && Equals(e.RegistrationDate, date)).FirstOrDefault();
			int ID;
			
			if (TryID_By_RN(RN, out ID) || contractStatement != null) //Если есть акт обновляем, иначе создаём нового
			{
				contractStatement = contractStatement ?? ContractStatements.Get(ID);
				Logger.DebugFormat("ContractStatement_RX: {0} {1}", "Record found ", contractStatement.Id.ToString());
			}
			else
			{
				contractStatement = ContractStatements.Create();
				Logger.DebugFormat("ContractStatement_RX: {0} {1}", "Record created ", contractStatement.Id.ToString());
			}
			
//			int totalAmount;
//			if (int.TryParse(Sum, out totalAmount))
//				contractStatement.TotalAmount = totalAmount;
			
			if (!contractStatement.RegistrationState.HasValue || contractStatement.RegistrationState.Value == Sungero.FinancialArchive.ContractStatement.RegistrationState.NotRegistered)
			{
				contractStatement.RegistrationNumber = RegNum;
				
				if (date.HasValue)
					contractStatement.RegistrationDate =  date.Value;
				
				if (!contractStatement.InternalApprovalState.HasValue
				    || contractStatement.InternalApprovalState.Value == Sungero.FinancialArchive.ContractStatement.InternalApprovalState.Aborted
				    || contractStatement.InternalApprovalState.Value == Sungero.FinancialArchive.ContractStatement.InternalApprovalState.OnRework)
					contractStatement.Counterparty = GetOrg_By_RN(OrgRN);
				
				contractStatement.BusinessUnit = GetOurOrg_By_RN(OurOrgRN);
			}
			
			contractStatement.LeadingDocument = GetContract_By_RN(ContractRN);
			contractStatement.Save();
			
			Logger.DebugFormat("ContractStatement_RX: {0}", "Record saved");
			
			Sync_ExternalEntityLink(contractStatement, RN);
			
			return contractStatement;
		}
		
	}
}