﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using SC.Business.RolesCampari;

namespace SC.Business.Server
{
	partial class RolesCampariFunctions
	{
		public override Sungero.Company.IEmployee GetRolePerformer(Sungero.Docflow.IApprovalTask task)
		{
			var maintask = SC.CampariSolution.ApprovalTasks.As(task);
			if (_obj.Type == SC.Business.RolesCampari.Type.NetworkAccount)
				return maintask.NetworkAccountantSC;
			
			if (_obj.Type == SC.Business.RolesCampari.Type.BrandManager)
				return maintask.BrandManagerSSC;
			
			if (_obj.Type == SC.Business.RolesCampari.Type.ResponsibleMana)
				return maintask.ResponsibleManagerSC;
			return base.GetRolePerformer(task);
		}
	}
}