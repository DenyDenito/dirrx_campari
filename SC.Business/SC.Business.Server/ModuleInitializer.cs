﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace SC.Business.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			CreateDocumentTypes();
			CreateDocumentKinds();
			CreateApprovalRole(SC.Business.RolesCampari.Type.BrandManager, "Бренд-менеджер.");
			CreateApprovalRole(SC.Business.RolesCampari.Type.ResponsibleMana, "Ответственный менеджер коммерческого отдела.");
			CreateApprovalRole(SC.Business.RolesCampari.Type.NetworkAccount, "Бухгалтер ответственный за сеть.");
		}
		
		/// <summary>
		/// Создание типа документа "Финансовый документ" в служебном справочнике "Типы документов".
		/// </summary>
		public static void CreateDocumentTypes()
		{
			Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentType("Финансовый документ", FinancialDocument.ClassTypeGuid, Sungero.Docflow.DocumentType.DocumentFlow.Contracts, true);
			InitializationLogger.Debug("Создан тип финансового документа");
		}
		
		/// <summary>
		/// Создание видов документов для договора закупки.
		/// </summary>
		public static void CreateDocumentKinds()
		{
			Sungero.Docflow.PublicInitializationFunctions.Module.CreateDocumentKind("Финансовый документ", "Финансовый документ",
			                                                                        Sungero.Docflow.DocumentKind.NumberingType.Registrable,
			                                                                        Sungero.Docflow.DocumentType.DocumentFlow.Contracts,
			                                                                        true, false,  FinancialDocument.ClassTypeGuid,
			                                                                        new Sungero.Domain.Shared.IActionInfo[] { Sungero.Docflow.OfficialDocuments.Info.Actions.SendForApproval,
			                                                                        	Sungero.Docflow.OfficialDocuments.Info.Actions.SendForFreeApproval,
			                                                                        	Sungero.Docflow.OfficialDocuments.Info.Actions.SendActionItem
			                                                                        },
			                                                                        Constants.Module.FinancialDocumentKind);
			InitializationLogger.Debug("Создан вид финансового документа");
		}

		/// <summary>

		/// Создание роли.

		/// </summary>

		public static void CreateApprovalRole(Enumeration roleType, string description)
		{
			var role = RolesCamparis.Get().Where(r => Equals(r.Type,roleType)).FirstOrDefault();
			// Проверяет наличие роли.
			if (role == null)
			{
				role = RolesCamparis.Create();
				role.Type = roleType;
			}
			role.Description = description;
			role.Save();
		}
		
	}
}
