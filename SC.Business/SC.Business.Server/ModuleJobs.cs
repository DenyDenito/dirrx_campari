﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.Content;
using Sungero.CoreEntities;
using iTextSharp.text.pdf;
using System.IO;
using Sungero.Domain.Shared;
using Sungero.Metadata;

namespace SC.Business.Server
{
	public class ModuleJobs
	{

		/// <summary>
		/// Отправка сводного PDF на эл. почту
		/// </summary>
		public virtual void SendDocsPDFToMail()
		{
			var docsList = new List<IElectronicDocument>();
			
			var actDocs = SC.CampariSolution.ContractStatements.GetAll().Where(e => e.DatetosscSC.HasValue && Calendar.Equals(e.DatetosscSC.Value.Date, Calendar.Today.Date)).ToList<IElectronicDocument>();
			var utdDocs = SC.CampariSolution.UniversalTransferDocuments.GetAll().Where(e => e.DatetosscSC.HasValue && Calendar.Equals(e.DatetosscSC.Value.Date, Calendar.Today.Date)).ToList<IElectronicDocument>();
			var wbDocs = SC.CampariSolution.Waybills.GetAll().Where(e => e.DatetosscSC.HasValue && Calendar.Equals(e.DatetosscSC.Value.Date, Calendar.Today.Date)).ToList<IElectronicDocument>();
			var finDocs = Business.FinancialDocuments.GetAll().Where(e => e.DatetosscSC.HasValue && Calendar.Equals(e.DatetosscSC.Value.Date, Calendar.Today.Date)).ToList<IElectronicDocument>();
			
			docsList = actDocs.Concat(utdDocs).Concat(wbDocs).Concat(finDocs).ToList();
			
			Logger.DebugFormat("JSON: {{\"operation\": \"SendDocsPDFToMail\", \"DocCount\": {0}}}", docsList.Count.ToString());

			var i = 0;
			while (docsList.Count > 0)
			{
				try
				{
					var relatedDocs = docsList[i].Relations.GetRelated().Union(docsList[i].Relations.GetRelatedFrom()).ToList();
					relatedDocs = relatedDocs.Prepend(docsList[i]).ToList();
					relatedDocs = relatedDocs.OrderByDescending(e => Sungero.Contracts.IncomingInvoices.Is(e))
						.ThenByDescending(e => Sungero.Contracts.IncomingInvoices.Is(e))
						.ThenByDescending(e => Sungero.FinancialArchive.Waybills.Is(e))
						.ThenByDescending(e => Sungero.FinancialArchive.ContractStatements.Is(e)).ToList();
					
					var pdfDocs = relatedDocs.Where(e => Functions.Module.GeneratePDFForDocument(e)).ToList();
					var stream = Functions.Module.MergeDocsPDF(pdfDocs, docsList[i]);
					
					var emailConstant = SC.Settings.Settings.GetAll().FirstOrDefault(e => e.Name == "EmailToSendPDF");
					
					try
					{
						Functions.Module.SendDocToMail(stream, docsList[i].Name + ".pdf", emailConstant != null ? emailConstant.ParamValue : string.Empty);
					}
					finally
					{
						stream.Close();
						stream.Dispose();
						stream = null;
					}
					
					relatedDocs.ForEach(e => docsList.Remove(e));
				}
				catch {
					Logger.ErrorFormat("JSON: {{\"operation\": \"SendDocsPDFToMail\", \"DocID\": {0}}}", docsList[i].Id);
					throw;
				}
			}
		}

	}
}