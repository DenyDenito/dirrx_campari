﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.Content;
using Sungero.CoreEntities;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Sungero.Domain.Shared;
using Sungero.Metadata;

namespace SC.Business.Server
{
	public class ModuleFunctions
	{

		/// <summary>
		/// Отправка документа по электронной почте
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="fileName"></param>
		/// <param name="to"></param>
		public static void SendDocToMail(System.IO.Stream stream, string fileName, string to)
		{
			using (new Sungero.Domain.Session())
				using (var mailClient = new System.Net.Mail.SmtpClient())
					using (var mail = new System.Net.Mail.MailMessage
					       {
					       	Body = string.Empty,
					       	IsBodyHtml = true,
					       	Subject = fileName.Replace('\r', ' ').Replace('\n', ' '),
					       	HeadersEncoding = System.Text.Encoding.UTF8,
					       	SubjectEncoding = System.Text.Encoding.UTF8,
					       	BodyEncoding = System.Text.Encoding.UTF8
					       })
			{
				if (!string.IsNullOrEmpty(to))
					mail.To.Add(to);
				
//					foreach (var email in cc.Where(e => !string.IsNullOrEmpty(e)))
//						mail.CC.Add(email);

//					mail.Attachments.AddRange(this.GetMailAttachments(assignment));
//					if (assignment.Importance == Workflow.AssignmentBase.Importance.High)
//						mail.Priority = System.Net.Mail.MailPriority.High;
//					else if (assignment.Importance == Workflow.AssignmentBase.Importance.Low)
//						mail.Priority = System.Net.Mail.MailPriority.Low;
				
				var contentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
				var reportAttachment = new System.Net.Mail.Attachment(stream, contentType);
				reportAttachment.ContentDisposition.FileName = fileName;
				
				mail.Attachments.Add(reportAttachment);
				
				Logger.DebugFormat("JSON: {{\"operation\": \"SendDocToMail\", \"Text\": \"Ready to send\", \"FileName\": \"{0}\", \"Email\": \"{1}\"}}", fileName, to);
				
//				mailClient.Host = "smtp.yandex.ru";
//				mailClient.Port = 587; // Обратите внимание что порт 587
//				mailClient.EnableSsl = true;
//				mailClient.Credentials = new System.Net.NetworkCredential("test@test.ru", ""); // Ваши логин и пароль
				
				mailClient.Send(mail);
				
				Logger.DebugFormat("JSON: {{\"operation\": \"SendDocToMail\", \"Text\": \"Successful sending\", \"FileName\": \"{0}\", \"Email\": \"{1}\"}}", fileName, to);
			}
		}

		/// <summary>
		/// Склейка PDF версий списка документов
		/// </summary>
		/// <param name="docsList"></param>
		/// <param name="mainDoc"></param>
		/// <returns></returns>
		public static System.IO.Stream MergeDocsPDF(List<IElectronicDocument> docsList, IElectronicDocument mainDoc)
		{
			var filePath = string.Empty;
			var docName = mainDoc.Name;
			MemoryStream stream = null;
			
			foreach (char badChar in Path.GetInvalidPathChars())
			{
				docName = docName.Replace(badChar, '_');
			}

			if (docsList.Count > 0)
			{
				stream = new MemoryStream();
//				using (FileStream stream = new FileStream(@"C:\DRX\Temp\" + docName + ".pdf", FileMode.Create))
//				{
				Document document = new Document();
				Document documentStamp = new Document();
				
				PdfCopy pdf = new PdfCopy(document, stream);
				pdf.CloseStream = false;
				
				PdfReader reader = null;
				PdfReader readerStamp = null;
				
				try
				{
					document.Open();
					
					foreach (var doc in docsList)
					{
						var version = doc.LastVersion;
						if (version != null && version.BodyAssociatedApplication.Extension.ToLower() == "pdf")
						{
							using (var memory = new MemoryStream())
							{
								version.Body.Read().CopyTo(memory);

								try
								{
									reader = new PdfReader(memory.ToArray());
									pdf.AddDocument(reader);
									
									pdf.FreeReader(reader);
								}
								finally
								{
									if (reader != null)
										reader.Close();
								}
							}
						}
					}
					
					using (var memoryStamp = new MemoryStream())
					{
						var properties = mainDoc.GetEntityMetadata().Properties;
						
						var typeOfFlowSC = (Enumeration?) properties.FirstOrDefault(e => e.Name == "TypeOfFlowSC").GetDynamicPropertyValue(mainDoc);
						var noBonusAgreementSC = properties.FirstOrDefault(e => e.Name == "NoBonusAgreementSC").GetPropertyValue<string>(mainDoc);
						var noFIDocumentSC = properties.FirstOrDefault(e => e.Name == "NoFIDocumentSC").GetPropertyValue<string>(mainDoc);
						var taxCodeSC = properties.FirstOrDefault(e => e.Name == "TaxCodeSC").GetPropertyValue<string>(mainDoc);
						var textSC = properties.FirstOrDefault(e => e.Name == "TextSC").GetPropertyValue<string>(mainDoc);
						var senderSC = properties.FirstOrDefault(e => e.Name == "SenderSC").GetPropertyValue<Sungero.Company.IEmployee>(mainDoc);
						var noPurchasingDocumentSC = properties.FirstOrDefault(e => e.Name == "NoPurchasingDocumentSC").GetPropertyValue<string>(mainDoc);
						var noLineSC = properties.FirstOrDefault(e => e.Name == "NoLineSC").GetPropertyValue<string>(mainDoc);
						var statusSC = (Enumeration?) properties.FirstOrDefault(e => e.Name == "StatusSC").GetDynamicPropertyValue(mainDoc);
						var advertisingSC = (Enumeration?) properties.FirstOrDefault(e => e.Name == "AdvertisingSC").GetDynamicPropertyValue(mainDoc);
						
						PdfWriter writer = PdfWriter.GetInstance(documentStamp, memoryStamp);
						documentStamp.Open();
						
						BaseFont baseFont = BaseFont.CreateFont(@"C:\Windows\Fonts\Arial.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
						Font font = new iTextSharp.text.Font(baseFont, 10f, Font.NORMAL);
						
						documentStamp.NewPage();
						
						PdfPTable table = new PdfPTable(2);
//						PdfPCell cell = table.AddCell(new Phrase("Тип расхода", font));
						table.AddCell(new Phrase("Тип расхода/Expense type", font));
						table.AddCell(new Phrase(SC.Business.FinancialDocuments.Info.Properties.TypeOfFlowSC.GetLocalizedValue(typeOfFlowSC), font));
						table.AddCell(new Phrase("№ бонус Эгримент/№ bonus agreement", font));
						table.AddCell(new Phrase(noBonusAgreementSC, font));
						table.AddCell(new Phrase("№ FI документ/№ FI document", font));
						table.AddCell(new Phrase(noFIDocumentSC, font));
						table.AddCell(new Phrase("Код налога/Tax code", font));
						table.AddCell(new Phrase(taxCodeSC, font));
						table.AddCell(new Phrase("Текст/Text", font));
						table.AddCell(new Phrase(textSC, font));
						table.AddCell(new Phrase("Отправитель/Sender", font));
						table.AddCell(new Phrase(senderSC != null && !string.IsNullOrEmpty(senderSC.Email) ? senderSC.Email.Split('@').FirstOrDefault() : string.Empty, font));
						table.AddCell(new Phrase("№ документа закупки/№ PO", font));
						table.AddCell(new Phrase(noPurchasingDocumentSC, font));
						table.AddCell(new Phrase("№ линии/№ Line", font));
						table.AddCell(new Phrase(noLineSC, font));
						table.AddCell(new Phrase("Статус/Status", font));
						table.AddCell(new Phrase(SC.Business.FinancialDocuments.Info.Properties.StatusSC.GetLocalizedValue(statusSC), font));
						table.AddCell(new Phrase("Реклама/Advertising type", font));
						table.AddCell(new Phrase(SC.Business.FinancialDocuments.Info.Properties.AdvertisingSC.GetLocalizedValue(advertisingSC), font));
						
						table.SetWidths(new int[] {45, 55});
						
						documentStamp.Add(table);
						
//						documentStamp.Add(CreateParagraph("Тип расхода", SC.Business.FinancialDocuments.Info.Properties.TypeOfFlowSC.GetLocalizedValue(typeOfFlowSC), font));
//						documentStamp.Add(CreateParagraph("№ бонус Эгримент", noBonusAgreementSC, font));
//						documentStamp.Add(CreateParagraph("№ FI документ", noFIDocumentSC, font));
//						documentStamp.Add(CreateParagraph("Код налога", taxCodeSC, font));
//						documentStamp.Add(CreateParagraph("Текст", textSC, font));
//						documentStamp.Add(CreateParagraph("Отправитель", senderSC != null && !string.IsNullOrEmpty(senderSC.Email) ? senderSC.Email.Split('@').FirstOrDefault() : string.Empty, font));
//						documentStamp.Add(CreateParagraph("№ документа закупки", noPurchasingDocumentSC, font));
//						documentStamp.Add(CreateParagraph("№ линии", noLineSC, font));
//						documentStamp.Add(CreateParagraph("Статус", SC.Business.FinancialDocuments.Info.Properties.StatusSC.GetLocalizedValue(statusSC), font));
//						documentStamp.Add(CreateParagraph("Реклама", SC.Business.FinancialDocuments.Info.Properties.AdvertisingSC.GetLocalizedValue(advertisingSC), font));
						
						documentStamp.Close();
						
						readerStamp = new PdfReader(memoryStamp.ToArray());
						pdf.AddDocument(readerStamp);
						
						pdf.FreeReader(readerStamp);
					}
					
					Logger.DebugFormat("JSON: {{\"operation\": \"MergeDocsPDF\", \"Text\": \"Successful merging\", \"DocID\": \"{0}\"}}", mainDoc.Id.ToString());
					filePath = @"C:\DRX\Temp\" + docName + ".pdf";
				}
				finally
				{
					if (readerStamp != null)
						readerStamp.Close();
					
					if (documentStamp != null)
						documentStamp.Close();
					
					if (document != null)
						document.Close();
				}
//				}
			}
			
			//Возвращаем позицию в потоке на начальную, чтобы избежать проблем при отправке
			stream.Position = 0;
//			return filePath;
			return stream;
		}
		
		/// <summary>
		/// Создание параграфа для добавления в PDF документ (iTextSharp)
		/// </summary>
		/// <param name="paramName"></param>
		/// <param name="ParamValue"></param>
		/// <param name="font"></param>
		/// <returns></returns>
		private static Paragraph CreateParagraph(string paramName, string ParamValue, Font font)
		{
			var paragraph = new Paragraph();
//			var paramNameFont = font;
//			font.SetStyle(Font.BOLD);
			paragraph.Add(new Chunk(paramName, font));
			paragraph.TabSettings = new TabSettings(150f);
			paragraph.Add(Chunk.TABBING);
			paragraph.Add(new Chunk(ParamValue, font));
			
			return paragraph;
		}
		
		/// <summary>
		///Генерация publicbody для неформализованных документов.
		/// </summary>
		/// <param name="document"></param>
		/// <param name="versionId"></param>
		[Public, Remote]
		public static bool GeneratePDFForDocument(IElectronicDocument document)
		{
			if (document == null)
				return false;
			
			var version = document.LastVersion;
			if (version == null)
				return false;
			
			// Формат не поддерживается.
			var versionExtension = version.BodyAssociatedApplication.Extension.ToLower();
			var versionExtensionIsSupported = Sungero.AsposeExtensions.Converter.CheckIfExtensionIsSupported(versionExtension);
			
			if (versionExtension == "pdf")
				return true;
			else if (!versionExtensionIsSupported)
				return false;
			
			var pdfConverter = new Sungero.AsposeExtensions.Converter();
			
			System.IO.Stream pdfDocumentStream = null;
			
			using (var inputStream = new System.IO.MemoryStream())
			{
				version.Body.Read().CopyTo(inputStream);
				try
				{
					pdfDocumentStream = pdfConverter.GeneratePdf(inputStream, version.BodyAssociatedApplication.Extension);
				}
				catch (Sungero.AsposeExtensions.PdfConvertException e)
				{
					Logger.Error(Sungero.Docflow.Resources.PdfConvertErrorFormat(document.Id), e.InnerException);
				}
			}
			
			// Если не сгенерировалась pdf - значит не поддерживается формат.
			if (pdfDocumentStream == null)
				return false;
			
			version = document.Versions.AddNew();
			version.Body.Write(pdfDocumentStream);
			version.AssociatedApplication = Sungero.Content.AssociatedApplications.GetByExtension("pdf");
			
			document.Save();
			pdfDocumentStream.Close();
			
			return true;
		}

	}
}