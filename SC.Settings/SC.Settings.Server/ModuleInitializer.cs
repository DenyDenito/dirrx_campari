﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Domain.Initialization;

namespace SC.Settings.Server
{
	public partial class ModuleInitializer
	{

		public override void Initializing(Sungero.Domain.ModuleInitializingEventArgs e)
		{
			CreateSetting("EmailToSendPDF", "test@test.ru");
		}
		
		/// <summary>
		/// Создание настройки
		/// </summary>
		/// <param name="name">Гаименование</param>
		/// <param name="paramValue">Значение</param>
		/// <param name="Note">Примечание</param>
		public static void CreateSetting(string name, string paramValue, string Note = "")
		{
			if (Settings.GetAll().Where(e => e.Name == name).Count() == 0)
			{
				var setting = Settings.Create();
				
				setting.Name = name;
				setting.ParamValue = paramValue;
				setting.Note = Note;
				
				setting.Save();
				
				InitializationLogger.DebugFormat("Создана настройка: {0}", name);
			}
		}
	}
}
