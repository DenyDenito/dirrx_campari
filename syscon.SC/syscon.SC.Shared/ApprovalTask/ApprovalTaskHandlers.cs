using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalTask;

namespace syscon.SC
{

	partial class ApprovalTaskRolesSysconsysconSharedHandlers
	{
		public virtual void RolesSysconsysconEmpPropertyChanged(syscon.SC.Shared.ApprovalTaskRolesSysconsysconEmpPropertyChangedEventArgs e)
		{
			var task = _obj.ApprovalTask;
			_obj.ApprovalTask.ApprovalRule = _obj.ApprovalTask.ApprovalRule;
			task.State.Controls.Control.Refresh();
		}
	}

	partial class ApprovalTaskSharedHandlers
	{

		public override void ApprovalRuleChanged(Sungero.Docflow.Shared.ApprovalTaskApprovalRuleChangedEventArgs e)
		{
			var Rule = e.NewValue;
			if (Rule != null && e.OldValue != e.NewValue)
			{
				_obj.RolesSysconsyscon.Clear();
				foreach (var stage in Rule.Stages)
				{
					var stage_sc = syscon.SC.ApprovalStages.As(stage.Stage);
					if (stage_sc != null && stage_sc.FilledByUsersyscon == true)
					{
						if (stage_sc.ApprovalRoles.Count > 0)
						{
							foreach (var role in stage_sc.ApprovalRoles)
							{
								if (role.ApprovalRole.Type != syscon.Parus8.Role.Type.Initiator)
								{
									var s = _obj.RolesSysconsyscon.AddNew();
									s.RoleProperty = role.ApprovalRole;
									if (stage_sc.Requiredsyscon != null)
										s.Required = stage_sc.Requiredsyscon;
									else
										s.Required = false;
								}
							}
						}
						else
						{
							if (stage_sc.ApprovalRole != null)
							{
								var s = _obj.RolesSysconsyscon.AddNew();
								s.RoleProperty = stage_sc.ApprovalRole;
								if (stage_sc.Requiredsyscon != null)
									s.Required = stage_sc.Requiredsyscon;
								else
									s.Required = false;
							}
						}
					}
				}
			}
			base.ApprovalRuleChanged(e);
		}

		public virtual void RegGroupKSChanged(syscon.SC.Shared.ApprovalTaskRegGroupKSChangedEventArgs e)
		{
			var regGroup = RegistrationGroups.As(_obj.RegGroupKS);
			
			if (regGroup != null)
			{
				for (int i = 0; i < _obj.RolesSysconsyscon.Count; i++)
				{
					var role1 = _obj.RolesSysconsyscon.ToList()[i];
					Sungero.Company.IEmployee employee = null;
					foreach (var roleRegGroup in regGroup.Rolessyscon)
					{
						if (Equals(role1.RoleProperty,roleRegGroup.Role))
						{
							employee = roleRegGroup.EmpRole;
						}
					}
					
					var maindoc = _obj.DocumentGroup.OfficialDocuments.First();
					//var emp2 = syscon.Parus8.PublicFunctions.Module.Remote.ReturnEmpForRole3(role1.RoleProperty,_obj);
					var employee_from_RoleCalc = syscon.Parus8.PublicFunctions.Module.Remote.ReturnEmpForRole2(role1.RoleProperty,maindoc, _obj.RegGroupKS);
					//var emp2 = Functions.ApprovalTask.Remote.ReturnEmpForRole(_obj,role1.RoleProperty,maindoc);
					
					if (employee_from_RoleCalc != null)
					{
						employee = Sungero.Company.Employees.As(employee_from_RoleCalc.RoleRecKS);
					}
					role1.EmpProperty = employee;
					
				}
			}
		}

	}
}