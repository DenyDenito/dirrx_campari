﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ExternalEntityLink;

namespace syscon.SC
{
	partial class ExternalEntityLinkSharedHandlers
	{

		public override void IsDeletedChanged(Sungero.Domain.Shared.BooleanPropertyChangedEventArgs e)
		{
			base.IsDeletedChanged(e);
			
			if (e.OldValue != null && e.NewValue != null
			    && e.NewValue != e.OldValue
			    && !e.NewValue.Value)
				_obj.ExtEntityId = string.Empty;
		}

	}
}