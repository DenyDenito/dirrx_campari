﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.Condition;

namespace syscon.SC.Shared
{
  partial class ConditionFunctions
  {
  	public override void ChangePropertiesAccess()
		{
			base.ChangePropertiesAccess();
			if (_obj.ConditionType ==ConditionType.RolesComparer)
			{
				_obj.State.Properties.RoleComparerKS.IsVisible = true;
			}
			else
			{
				_obj.State.Properties.RoleComparerKS.IsVisible = false;
			}
		}
		
		public override void ClearHiddenProperties()
		{
			base.ClearHiddenProperties();
//			if (!_obj.State.Properties.PurchaseKind.IsVisible)
//				_obj.PurchaseKind = null;
		}
		
		public override Sungero.Docflow.Structures.ConditionBase.ConditionResult CheckCondition(Sungero.Docflow.IOfficialDocument document, Sungero.Docflow.IApprovalTask task)
		{
			if (_obj.RoleComparerKS == true)
			{
				var main_task = syscon.SC.ApprovalTasks.As(task);
				Sungero.Company.IEmployee emp_role_div_head = null;
				foreach (var role in main_task.RolesSysconsyscon)
				{
					if (role.RoleProperty.Type == syscon.Parus8.Role.Type.DivisionHead)
					{
						emp_role_div_head = role.EmpProperty;
					}
				}
				if (Equals(task.Signatory, emp_role_div_head))
				    {
				    	return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(true, string.Empty);
				    }
				    else
				    {
				    	return Sungero.Docflow.Structures.ConditionBase.ConditionResult.Create(false, string.Empty);
				    }		
			}
			return base.CheckCondition(document, task);
		}

  }
}