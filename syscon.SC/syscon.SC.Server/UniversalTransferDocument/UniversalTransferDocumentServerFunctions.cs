﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.UniversalTransferDocument;

namespace syscon.SC.Server
{
	partial class UniversalTransferDocumentFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote]
		public static string GetFileNameFormalizedDocument(IUniversalTransferDocument doc)
		{
			var fullFileName = doc.Name + ".xml";
			
			var sellerTitle = doc.Versions
				.Where(x => x.Id == doc.SellerTitleId)
				.FirstOrDefault();
			
			sellerTitle = sellerTitle ?? doc.Versions.FirstOrDefault();
			
			var xdoc = System.Xml.Linq.XDocument.Load(sellerTitle.Body.Read());
			Sungero.Exchange.PublicFunctions.Module.RemoveNamespaces(xdoc);
			var docElement = xdoc.Element("Файл");
			var fileName = docElement.Attribute("ИдФайл").Value;
			
			fullFileName = fileName + ".xml";
			
			return fullFileName;
		}

	}
}