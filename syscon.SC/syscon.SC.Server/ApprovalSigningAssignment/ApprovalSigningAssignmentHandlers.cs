﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalSigningAssignment;

namespace syscon.SC
{
	partial class ApprovalSigningAssignmentServerHandlers
	{

		public override void BeforeComplete(Sungero.Workflow.Server.BeforeCompleteEventArgs e)
		{
			Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>0<<<<<<<<<<<<<<<<<<<");
			if ( Equals(_obj.Result.Value, Sungero.Docflow.ApprovalSigningAssignment.Result.Sign))
			{
				Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>1<<<<<<<<<<<<<<<<<<<");
				var apptask = Sungero.Docflow.ApprovalTasks.As(_obj.Task);
				if (apptask != null && apptask.DeliveryMethod != null && apptask.DeliveryMethod.Id == 1)
				{
					Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>2<<<<<<<<<<<<<<<<<<<");
					var doc = apptask.DocumentGroup.OfficialDocuments.FirstOrDefault();
					if ( doc != null && doc.BusinessUnit != null && doc.BusinessUnit.TIN != null)
					{
						Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>3<<<<<<<<<<<<<<<<<<<");
						Logger.Debug("!!!!!!!!!!!!!!>>>>>>>>>>>>>>>" + doc.LastVersion.Note + "<<<<<<<<<<<<<<<!!!!!!!!!");
						string inn = doc.BusinessUnit.TIN;
						Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>" + doc.Name + " ИНН:"+ doc.BusinessUnit.TIN + "<<<<<<<<<<<<<<<<<<<");
						var signs = Signatures.Get(doc.LastVersion).Where(x => x.Signatory != null && x.SigningDate != null && x.Signatory.Equals(Users.Current)).OrderByDescending(y => y.SigningDate).FirstOrDefault();
						Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>" + signs.ToString() + "<<<<<<<<<<<<<<<<<<<");
						if (signs != null)
						{
							Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>4<<<<<<<<<<<<<<<<<<<");
							if (signs.SignCertificate != null && signs.SignCertificate.Subject != null && signs.SignCertificate.Subject.IndexOf(inn) < 0)
							{
								Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>5<<<<<<<<<<<<<<<<<<<");
								e.AddError("Подпишите документ, сертификатом '" + doc.BusinessUnit.Name + "'");
							}
							else
							{
								Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>6<<<<<<<<<<<<<<<<<<<");
								if (signs.SignCertificate == null)
								{
									Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>7<<<<<<<<<<<<<<<<<<<");
									e.AddError("Подпишите документ электронным сертификатом!");
								}
							}
						}
						else
						{
							Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>8<<<<<<<<<<<<<<<<<<<");
							e.AddError("Подпишите документ электронным сертификатом");
						}
					}
				}
			}
			Logger.Debug(">>>>>>>>>>>>>>>>>>>>>>9<<<<<<<<<<<<<<<<<<<");
			base.BeforeComplete(e);
		}
	}

}