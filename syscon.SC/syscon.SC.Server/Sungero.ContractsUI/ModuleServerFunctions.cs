﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.SC.Module.ContractsUI.Server
{
	partial class ModuleFunctions
	{

		/// <summary>
		/// 
		/// </summary>
		[Remote, Public]
		public static List<IIncomingInvoice> Function2()
		{
			var listID = new List<int>();
			
			var query = IncomingInvoices.GetAll().Where(d => d.HasRelations);
			foreach (var document in query)
			{
				var result = document.Relations
					.GetRelated()
					.Union(document.Relations.GetRelatedFrom())
					.Distinct()
					.Any(r => SC.ContractStatements.Is(r)
					     || SC.Waybills.Is(r)
					     || SC.UniversalTransferDocuments.Is(r));
				if (result)
					listID.Add(document.Id);
			}
			
			return query.Where(d => listID.Contains(d.Id)).ToList();
		}

	}
}