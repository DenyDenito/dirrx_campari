﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.SC.Module.ContractsUI.Server
{
	partial class WithoutRelatedIncomingInvoicesTIKSFolderHandlers
	{
		private bool check_type_doc_nds(IEnumerable<Sungero.Content.IElectronicDocument> list)
		{
			var res = false;
			
			foreach (var link_doc in list)
			{
//				bool sf = SC.IncomingTaxInvoices.Is(link_doc);
//				bool upd = SC.UniversalTransferDocuments.Is(link_doc);
				
				res = SC.IncomingTaxInvoices.Is(link_doc) || SC.UniversalTransferDocuments.Is(link_doc);
				if (res)
					break;
			}
			
			return res;
		}

		public virtual IQueryable<syscon.SC.IIncomingInvoice> WithoutRelatedIncomingInvoicesTIKSDataQuery(IQueryable<syscon.SC.IIncomingInvoice> query)
		{
			query = query.Where(d => d.AmountVATKS > 0 && (!d.HasRelations || !Sungero.Content.DocumentRelations.
			                                               GetAll().Any(x => x.Source.Id == d.Id
			                                                            && (SC.IncomingTaxInvoices.Is(x.Target) || SC.UniversalTransferDocuments.Is(x.Target))
			                                                           )
			                                               && !Sungero.Content.DocumentRelations.
			                                               GetAll().Any(x => x.Target.Id == d.Id
			                                                            && (SC.IncomingTaxInvoices.Is(x.Source) || SC.UniversalTransferDocuments.Is(x.Source))
			                                                           )
			                                              ));
			return query;
		}
	}

	partial class WithoutRelatedIncomingInvoicesKSFolderHandlers
	{
		private bool check_type_doc(IEnumerable<Sungero.Content.IElectronicDocument> list)
		{
			var res = false;
			
			foreach (var link_doc in list)
			{
				res = SC.ContractStatements.Is(link_doc) || SC.Waybills.Is(link_doc) || SC.UniversalTransferDocuments.Is(link_doc);
				if (res)
					break;
			}
			
			return res;
		}
		
		public virtual IQueryable<syscon.SC.IIncomingInvoice> WithoutRelatedIncomingInvoicesKSDataQuery(IQueryable<syscon.SC.IIncomingInvoice> query)
		{
			query = query.Where(d => !d.HasRelations || !Sungero.Content.DocumentRelations.
			                    GetAll().Any(x => x.Source.Id == d.Id
			                                 && (SC.IncomingTaxInvoices.Is(x.Target) || SC.ContractStatements.Is(x.Target) || SC.Waybills.Is(x.Target) || SC.UniversalTransferDocuments.Is(x.Target))
			                                )
			                    && !Sungero.Content.DocumentRelations.
			                    GetAll().Any(x => x.Target.Id == d.Id
			                                 && (SC.IncomingTaxInvoices.Is(x.Source) || SC.ContractStatements.Is(x.Source) || SC.Waybills.Is(x.Source) || SC.UniversalTransferDocuments.Is(x.Source))
			                                )
			                   );
			return query;
		}
	}

	partial class ContractsUIHandlers
	{
	}
}