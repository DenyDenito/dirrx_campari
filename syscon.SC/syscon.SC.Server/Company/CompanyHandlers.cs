using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.Company;
using System.Text.RegularExpressions;

namespace syscon.SC
{
	partial class CompanyServerHandlers
	{

		public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
		{
//			      base.BeforeSave(e);
			
			if (!string.IsNullOrEmpty(_obj.PSRN))
			{
				var newPsrn = _obj.PSRN.Trim();
				if (!_obj.PSRN.Equals(newPsrn, StringComparison.InvariantCultureIgnoreCase))
					_obj.PSRN = newPsrn;
			}
			
			if (!string.IsNullOrEmpty(_obj.TRRC))
			{
				var newTrrc = _obj.TRRC.Trim();
				if (!_obj.TRRC.Equals(newTrrc, StringComparison.InvariantCultureIgnoreCase))
					_obj.TRRC = newTrrc;
			}
			
			if (_obj.State.Properties.HeadCompany.IsChanged && _obj.HeadCompany != null)
			{
				var headCompany = _obj.HeadCompany;
				
				while (headCompany != null)
				{
					if (Equals(headCompany, _obj))
					{
						e.AddError(_obj.Info.Properties.HeadCompany, Sungero.Parties.CompanyBases.Resources.HeadCompanyCyclicReference, _obj.Info.Properties.HeadCompany);
						break;
					}
					
					headCompany = headCompany.HeadCompany;
				}
			}
			
			//------------------------------------------------------------------------------------------------------------------------------------

			if (!string.IsNullOrWhiteSpace(_obj.Code))
			{
				_obj.Code = _obj.Code.Trim();
				if (Regex.IsMatch(_obj.Code, @"\s"))
					e.AddError(_obj.Info.Properties.Code, Sungero.Company.Resources.NoSpacesInCode);
			}
			
			// Проверить код на пробелы, если свойство изменено.
			if (!string.IsNullOrEmpty(_obj.Code))
			{
				// При изменении кода e.AddError сбрасывается.
				var codeIsChanged = _obj.State.Properties.Code.IsChanged;
				_obj.Code = _obj.Code.Trim();
				
				if (codeIsChanged && Regex.IsMatch(_obj.Code, @"\s"))
					e.AddError(_obj.Info.Properties.Code, Sungero.Company.Resources.NoSpacesInCode);
			}
			
			if (!_obj.AccessRights.CanChangeCard())
			{
				var exchangeBoxesProp = _obj.State.Properties.ExchangeBoxes;
				var canExchangeProp = _obj.State.Properties.CanExchange;
				
				if (_obj.State.Properties
				    .Where(x => !Equals(x, exchangeBoxesProp) && !Equals(x, canExchangeProp))
				    .Select(x => x as Sungero.Domain.Shared.IPropertyState)
				    .Where(x => x != null)
				    .Any(x => x.IsChanged))
				{
					e.AddError(Sungero.Parties.Counterparties.Resources.NoRightsToChangeCard);
				}
			}
			
			if (!string.IsNullOrEmpty(_obj.TIN))
				_obj.TIN = _obj.TIN.Trim();

			// Проверка корректности ИНН.
			var tin = _obj.TIN;
//			if (_obj.Nonresident != true)
//			{
//				var result = Functions.Counterparty.CheckTin(_obj, tin);
//				if (!string.IsNullOrEmpty(result))
//					e.AddError(_obj.Info.Properties.TIN, result);
//			}

			// Проверка дубликации ИНН.
			if (!string.IsNullOrWhiteSpace(tin))
			{
				var warningText = Sungero.Parties.PublicFunctions.Counterparty.GetCounterpartyWithSameTinWarning(_obj);
				if (!string.IsNullOrEmpty(warningText))
					e.AddWarning(warningText, _obj.Info.Actions.ShowDuplicates);
			}
			
			foreach (var box in _obj.ExchangeBoxes.Select(x => x.Box).Distinct())
			{
				var boxLines = _obj.ExchangeBoxes.Where(x => Equals(x.Box, box)).ToList();
				if (boxLines.All(x => x.IsDefault == false))
				{
					foreach (var boxLine in boxLines)
						e.AddError(boxLine, _obj.Info.Properties.ExchangeBoxes.Properties.IsDefault,
						           Sungero.Parties.Counterparties.Resources.NoDefaultBoxServiceFormat(boxLine.Box),
						           _obj.Info.Properties.ExchangeBoxes.Properties.IsDefault);
				}
			}
		}
	}

}