﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.IncomingInvoice;

namespace syscon.SC
{
	partial class IncomingInvoiceFilteringServerHandler<T>
	{

		private bool check_type_doc(IEnumerable<Sungero.Content.IElectronicDocument>  list)
		{
			bool res = true;
			foreach (var link_doc in list)
			{
				bool act = SC.ContractStatements.Is(link_doc);
				bool nacl = SC.Waybills.Is(link_doc);
				bool upd = SC.UniversalTransferDocuments.Is(link_doc);
				res = upd || nacl || upd;
			}
			return res;
		}
		
		private bool check_type_doc_nds(IEnumerable<Sungero.Content.IElectronicDocument>  list)
		{
			bool res = true;
			foreach (var link_doc in list)
			{
				bool sf = SC.IncomingTaxInvoices.Is(link_doc);
				bool upd = SC.UniversalTransferDocuments.Is(link_doc);
				res = sf || upd;
			}
			return res;
		}
		
		public override IQueryable<T> Filtering(IQueryable<T> query, Sungero.Domain.FilteringEventArgs e)
		{
			var accountingDocumentsRelationName = Sungero.Contracts.PublicConstants.Module.AccountingDocumentsRelationName;

			query = base.Filtering(query, e);
			
//			if (_filter != null)
//			{
//				//Нет закрывающих документов
//				if (_filter.FlagKS)
//				{
////					query = query.Where(d => 1==1).ToList().Where(d => check_type_doc(d.Relations.GetRelated()) == false).AsQueryable();
//					
//					var listID = new List<int>();
//					
//					query = query.Where(d => d.HasRelations);
//					foreach (var document in query)
//					{
//						var result = с
//							.Any(r => SC.ContractStatements.Is(r)
//							     || SC.Waybills.Is(r)
//							     || SC.UniversalTransferDocuments.Is(r));
//						if (!result)
//							listID.Add(document.Id);
//					}
//					
//					query = query.Where(d => listID.Contains(d.Id));
//				}
//				
//				//Есть НДС и нет СФ или УПД
//				if (_filter.Flag1KS)
//				{
////					query = query.Where(d => d.AmountVATKS > 0).ToList().Where(d => check_type_doc_nds(d.Relations.GetRelated()) == false).AsQueryable();
//					
//					var listID = new List<int>();
//					
//					query = query.Where(d => d.AmountVATKS > 0 && d.HasRelations);
//					foreach (var document in query)
//					{
//						var result = document.Relations
//							.GetRelated()
//							.Union(document.Relations.GetRelatedFrom())
//							.Distinct()
//							.Any(r => SC.IncomingTaxInvoices.Is(r)
//							     || SC.UniversalTransferDocuments.Is(r));
//						if (!result)
//							listID.Add(document.Id);
//					}
//					
//					query = query.Where(d => listID.Contains(d.Id));
//				}
//			}			

			return query;
		}
	}

}