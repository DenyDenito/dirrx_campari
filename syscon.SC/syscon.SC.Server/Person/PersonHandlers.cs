﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.Person;

namespace syscon.SC
{
  partial class PersonServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
      if (_obj.TIN == null && syscon.SC.Employees.GetAll(x => x.Person.Equals(_obj)).Count() > 0)
			{
				e.AddError("Для сотрудников наших организаций обязательное заполнение ИНН");
			}
    }
  }

}