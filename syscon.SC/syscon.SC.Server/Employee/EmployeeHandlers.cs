using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.Employee;

namespace syscon.SC
{



  partial class EmployeeServerHandlers
  {

    public override void BeforeSave(Sungero.Domain.BeforeSaveEventArgs e)
    {
      base.BeforeSave(e);
      if (_obj.Person.TIN == null)
      {
      	e.AddError("У персоны не заполнен ИНН");
      }
    }
  }

}