﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using Sungero.Workflow;
using syscon.SC.ApprovalTask;

namespace syscon.SC.Server
{
	partial class ApprovalTaskRouteHandlers
	{

		public override void CompleteAssignment27(Sungero.Docflow.IApprovalCheckReturnAssignment assignment, Sungero.Docflow.Server.ApprovalCheckReturnAssignmentArguments e)
		{
			base.CompleteAssignment27(assignment, e);
			
			foreach (var attachment in assignment.Attachments)
			{
				foreach (var performer in e.Block.Performers)
				{
					Logger.Debug(">>>>>>>>" + performer.Name + "<<<<<<<<");
					if (!attachment.AccessRights.CanRead(performer) && attachment.AccessRights.CanGrant(DefaultAccessRightsTypes.Read))
						attachment.AccessRights.Grant(performer, DefaultAccessRightsTypes.Read);
				}
				attachment.Save();
			}
		}

		public override bool Decision38Result()
		{
			return base.Decision38Result();
		}
	}


}