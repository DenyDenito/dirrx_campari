﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalTask;

namespace syscon.SC.Server
{
	partial class ApprovalTaskFunctions
	{
		[Remote]
		public syscon.Parus8.IRoleCalcKS ReturnEmpForRole(Sungero.Docflow.IApprovalRoleBase role, Sungero.Docflow.IOfficialDocument maindoc)
		{
			var emp = syscon.Parus8.RoleCalcKSes.GetAll().FirstOrDefault(x => Equals(role,x.RoleKS) && Equals(x.OurOrgKS, maindoc.BusinessUnit) && Equals(x.RegGroupKS,_obj.RegGroupKS) && Equals(x.AuthorDepKS, maindoc.Department));
				return emp;
		}
	}
}