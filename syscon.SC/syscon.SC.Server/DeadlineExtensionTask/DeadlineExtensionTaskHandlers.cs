﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.DeadlineExtensionTask;

namespace syscon.SC
{
	partial class DeadlineExtensionTaskAssigneePropertyFilteringServerHandler<T>
	{

		public override IQueryable<T> AssigneeFiltering(IQueryable<T> query, Sungero.Domain.PropertyFilteringEventArgs e)
		{
			if (Sungero.Docflow.ApprovalCheckReturnAssignments.Is(_obj.ParentAssignment))
			{
				var assignment = Sungero.Docflow.ApprovalCheckReturnAssignments.As(_obj.ParentAssignment);
				var task = Sungero.Docflow.ApprovalTasks.As(assignment.Task);
				var rule = task.ApprovalRule;
				var stage = rule.Stages.Single(s => s.Number == assignment.StageNumber);
				var approvalStage = ApprovalStages.As(stage.Stage);
				var recipients = approvalStage.SampleRecipientsKS.Select(r => r.Recipient);
				
				var users = new List<IUser>();
				
				if (_obj.Assignee != null)
					users.Add(_obj.Assignee);
				
				foreach (var recipient in recipients)
				{
					if (Groups.Is(recipient))
					{
						var gp = Groups.As(recipient);
						users.AddRange(Groups.GetAllUsersInGroup(gp));
					}
					else
					{
						users.Add(Users.As(recipient));
					}
//					users = users.Distinct().ToList();
				}
//
//				if (!users.Contains(_obj.Assignee))
//					_obj.Assignee = null;
//
				query = query.Where(x => users.Contains(x));
			}
			return query;
		}
	}

}