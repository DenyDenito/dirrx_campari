﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.SC.Client
{
	public class ModuleFunctions
	{
		
	}
	
	partial class ApprovalTaskAnyChildEntityCollectionActions
	{
		public override bool CanDeleteChildEntity(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
		{
			// Дизейбл грида.
			var entity = ApprovalTasks.As(e.RootEntity);
			var test = _all as Sungero.Domain.Shared.IChildEntityCollection<syscon.SC.IApprovalTaskRolesSysconsyscon>;
//			return entity != null && _all == entity.RolesSysconsyscon
			return entity != null && test != null
				? false
				: base.CanDeleteChildEntity(e);
		}
	}
	
	partial class ApprovalTaskAnyChildEntityActions
	{	
		public override bool CanAddChildEntity(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
		{
			// Дизейбл грида.
			var entity = ApprovalTasks.As(e.RootEntity);
			var test = _all as Sungero.Domain.Shared.IChildEntityCollection<syscon.SC.IApprovalTaskRolesSysconsyscon>;
			return entity != null && test != null
				? false
				: base.CanAddChildEntity(e);
		}
		
		public override bool CanCopyChildEntity(Sungero.Domain.Client.CanExecuteChildCollectionActionArgs e)
		{
			// Дизейбл грида.
			var entity = ApprovalTasks.As(e.RootEntity);
			var test = _all as Sungero.Domain.Shared.IChildEntityCollection<syscon.SC.IApprovalTaskRolesSysconsyscon>;
			return entity != null && test != null
				? false
				: base.CanCopyChildEntity(e);
		}
	}
}