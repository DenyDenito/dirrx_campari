﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.ClientExtensions;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.UniversalTransferDocument;

namespace syscon.SC.Client
{
	partial class UniversalTransferDocumentActions
	{
		public virtual void SyncParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			decimal rn = 0;
			
			_obj.Save();
			var message = Parus8.PublicFunctions.Module.Remote.Sync_UniversalTransferDocument_Parus8(_obj, true);
			if (Decimal.TryParse(message, out rn))
				e.AddInformation(string.Format("Синхронизация прошла успешно. Идентификатор в Парус 8: {0}", message));
			else
				e.AddWarning(message);
		}

		public virtual bool CanSyncParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public virtual void SyncFormalizedParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			decimal rn = 0;
			
			_obj.Save();
			var message = Parus8.PublicFunctions.Module.Remote.Sync_FormalizedDocument_Parus8(_obj, true);
			if (Decimal.TryParse(message, out rn))
				e.AddInformation(string.Format("Синхронизация прошла успешно. Идентификатор в Парус 8: {0}", message));
			else
				e.AddWarning(message);
		}

		public virtual bool CanSyncFormalizedParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public virtual void ActionKS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var folderPath = @"C:\DRX\ExportFormalized";
			var file = Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFile.Create();
			
			var sellerTitle = _obj.Versions
				.Where(x => x.Id == _obj.SellerTitleId)
				.FirstOrDefault();
			
			sellerTitle = sellerTitle ?? _obj.Versions.FirstOrDefault();
			
			if (sellerTitle != null)
			{
				var fullFileName = Functions.UniversalTransferDocument.Remote.GetFileNameFormalizedDocument(_obj);
				
				using (var memory = new System.IO.MemoryStream())
				{
					sellerTitle.Body.Read().CopyTo(memory);
					file = Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFile.Create(sellerTitle.ElectronicDocument.Id, fullFileName, memory.ToArray(), null, null);
				}
				
				if (!string.IsNullOrEmpty(file.FileName))
				{
					try
					{
						var folderInfo = IO.CreateDirectory(folderPath);
						var filePath = IO.Combine(folderInfo, file.FileName);
						if (file.Body != null)
							IO.WriteAllBytes(filePath, file.Body);
						else
						{
							#warning Нелегал на сервис хранилищ, см 62340
							dynamic resolver = Activator.CreateInstance(Type.GetType("Sungero.Domain.Client.StorageServiceUriResolver,Sungero.Domain.Client"));
							var fullPath = resolver.Resolve(file.ServicePath);
							var storageStreamMethod = Type.GetType("Sungero.StorageServices.Shared.StorageServiceReadStream, Sungero.StorageServices.Shared")
								.GetConstructors().Single();
							var tokenProviderMethod = Type.GetType("Sungero.StorageServices.Shared.StorageServiceTokenProvider, Sungero.StorageServices.Shared")
								.GetConstructors().Single(c => c.GetParameters().Length == 2);
							var tokenProvider = tokenProviderMethod.Invoke(new[] { file.Token, null });
							using (var storageStream = (System.IO.Stream)storageStreamMethod.Invoke(new[] { fullPath, tokenProvider }))
							{
								using (var memoryStream  = new System.IO.MemoryStream())
								{
									storageStream.CopyTo(memoryStream);
									var a = memoryStream.ToArray();
								}
								
								using (var fileStream = System.IO.File.Create(filePath))
								{
									storageStream.CopyTo(fileStream);
								}
							}
						}
					}
					catch
					{
						var filePath = IO.Combine(folderPath, file.FileName);
						IO.DeleteFileAndEmptyDirectory(filePath, folderPath);
					}
				}
			}
			
//			var exportedDocument = Sungero.Docflow.Structures.AccountingDocumentBase.ExportedDocument
//				.Create(_obj.Id, _obj.IsFormalized ?? false, true, string.Empty, false, false, string.Empty, Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFolder
//				        .Create(string.Empty, new List<Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFile>(),
//				                new List<Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFolder>()), _obj.Name, null);
//
//			var folder = exportedDocument.Folder;
//
//			using (var memory = new System.IO.MemoryStream())
//			{
//				sellerTitle.Body.Read().CopyTo(memory);
//				var file = Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFile.Create(sellerTitle.ElectronicDocument.Id, fullFileName, memory.ToArray(), null, null);
//				folder.Files.Add(file);
//			}
//
//			if (folder.Files.Any())
//			{
//				try
//				{
//					foreach (var file in folder.Files.Where(f => f.Id <= 0 || f.Id == exportedDocument.Id))
//					{
//						var folderInfo = IO.CreateDirectory(folderPath);
//						var filePath = IO.Combine(folderInfo, file.FileName);
//						if (file.Body != null)
//							IO.WriteAllBytes(filePath, file.Body);
//						else
//						{
//							#warning Нелегал на сервис хранилищ, см 62340
//							dynamic resolver = Activator.CreateInstance(Type.GetType("Sungero.Domain.Client.StorageServiceUriResolver,Sungero.Domain.Client"));
//							var fullPath = resolver.Resolve(file.ServicePath);
//							var storageStreamMethod = Type.GetType("Sungero.StorageServices.Shared.StorageServiceReadStream, Sungero.StorageServices.Shared")
//								.GetConstructors().Single();
//							var tokenProviderMethod = Type.GetType("Sungero.StorageServices.Shared.StorageServiceTokenProvider, Sungero.StorageServices.Shared")
//								.GetConstructors().Single(c => c.GetParameters().Length == 2);
//							var tokenProvider = tokenProviderMethod.Invoke(new[] { file.Token, null });
//							using (var storageStream = (System.IO.Stream)storageStreamMethod.Invoke(new[] { fullPath, tokenProvider }))
//							{
//								using (var fileStream = System.IO.File.Create(filePath))
//								{
//									storageStream.CopyTo(fileStream);
//								}
//							}
//						}
//					}
//				}
//				catch
//				{
//					foreach (var file in folder.Files.Where(f => f.Id <= 0 || f.Id == exportedDocument.Id))
//					{
//						var filePath = IO.Combine(folderPath, file.FileName);
//						IO.DeleteFileAndEmptyDirectory(filePath, folderPath);
//					}
//					throw;
//				}
//			}
		}

		public virtual bool CanActionKS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public virtual void Action1KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			var sellerTitle = _obj.Versions.Where(x => x.Id == _obj.SellerTitleId).FirstOrDefault();
			if (sellerTitle == null)
			{
				Dialogs.ShowMessage("Нет фрмдока продавца");
			}
			else
				sellerTitle.Export();
			
//			Dialogs.ShowMessage("1");
//			var xdoc = System.Xml.Linq.XDocument.Load(sellerTitle.Body.Read());
//			Dialogs.ShowMessage("2");
//			Sungero.Exchange.PublicFunctions.Module.RemoveNamespaces(xdoc);
//			Dialogs.ShowMessage("3");
//			var docElement = xdoc.Element("Файл");
//			Dialogs.ShowMessage("4");
//			var fileName = docElement.Attribute("ИдФайл").Value;
//			Dialogs.ShowMessage("5");
//
//			var fullFileName = fileName + ".xml";
			
//			sellerTitle.Export();
			
//			using (var memory = new System.IO.MemoryStream())
//			{
//				sellerTitle.Body.Read().CopyTo(memory);
//				var file = Sungero.Docflow.Structures.AccountingDocumentBase.ExportedFile.Create(sellerTitle.ElectronicDocument.Id, fullFileName, memory.ToArray(), null, null);
//				folder.Files.Add(file);
//			}
		}

		public virtual bool CanAction1KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

	}


}