﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;

namespace syscon.SC.Module.Docflow.Client
{
	partial class ModuleFunctions
	{
		[Public]
		public override void ChangeDocumentType(Sungero.Docflow.IOfficialDocument document, List<Sungero.Domain.Shared.IEntityInfo> types, Sungero.Domain.Client.ExecuteActionArgs e)
		{
			base.ChangeDocumentType(document,types,e);
		}
	}
}