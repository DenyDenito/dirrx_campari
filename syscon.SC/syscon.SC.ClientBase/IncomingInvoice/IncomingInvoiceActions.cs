﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.IncomingInvoice;

namespace syscon.SC.Client
{
	partial class IncomingInvoiceActions
	{

		public virtual void SyncParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
//			if (Employees.Current.Login.LoginName == "mda")
//			{
				decimal rn = 0;
				
				_obj.Save();
				var message = Parus8.PublicFunctions.Module.Remote.Sync_IncomingInvoice_Parus8(_obj, true);
				if (Decimal.TryParse(message, out rn))
					e.AddInformation(string.Format("Синхронизация прошла успешно. Идентификатор в Парус 8: {0}", message));
				else
					e.AddWarning(message);
//			}
		}

		public virtual bool CanSyncParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

	}

}