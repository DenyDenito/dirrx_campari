﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.Waybill;

namespace syscon.SC.Client
{
	partial class WaybillActions
	{
		public virtual void SyncFormalizedParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			decimal rn = 0;
			
			_obj.Save();
			var message = Parus8.PublicFunctions.Module.Remote.Sync_FormalizedDocument_Parus8(_obj, true);
			if (Decimal.TryParse(message, out rn))
				e.AddInformation(string.Format("Синхронизация прошла успешно. Идентификатор в Парус 8: {0}", message));
			else
				e.AddWarning(message);
		}

		public virtual bool CanSyncFormalizedParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

		public virtual void SyncParus8KS(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			decimal rn = 0;
			
			_obj.Save();
			var message = Parus8.PublicFunctions.Module.Remote.Sync_AccountingDocument_Parus8(_obj, true);
			if (Decimal.TryParse(message, out rn))
				e.AddInformation(string.Format("Синхронизация прошла успешно. Идентификатор в Парус 8: {0}", message));
			else
				e.AddWarning(message);
		}

		public virtual bool CanSyncParus8KS(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return true;
		}

	}

}