﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.ApprovalCheckReturnAssignment;
using Sungero.Docflow;

namespace syscon.SC.Client
{
	partial class ApprovalCheckReturnAssignmentActions
	{
		public override void ExtendDeadline(Sungero.Domain.Client.ExecuteActionArgs e)
		{
//			e.Params.Add("");
			base.ExtendDeadline(e);
		}

		public override bool CanExtendDeadline(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanExtendDeadline(e);
		}


		public override void Signed(Sungero.Workflow.Client.ExecuteResultActionArgs e)
		{
			base.Signed(e);
			
			var task = ApprovalTasks.As(_obj.Task);
			if(task.ApprovalRule.DocumentKinds.Count > 0 && task.ApprovalRule.DocumentKinds.All(dk => Equals(dk.DocumentKind.Name, "Входящий счет на оплату") || Equals(dk.DocumentKind.Name, "Простой документ")) && _obj.OtherGroup.All.Count == 0)
			{
				e.AddError("Необходимо вложить закрывающий документ");
				return;
			}
		}

		public override bool CanSigned(Sungero.Workflow.Client.CanExecuteResultActionArgs e)
		{
			return base.CanSigned(e);
		}

	}

}