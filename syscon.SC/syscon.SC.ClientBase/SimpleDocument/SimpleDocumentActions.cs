﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sungero.Core;
using Sungero.CoreEntities;
using syscon.SC.SimpleDocument;

namespace syscon.SC.Client
{
	partial class SimpleDocumentActions
	{
		public override void ChangeDocumentType(Sungero.Domain.Client.ExecuteActionArgs e)
		{
			// Для смены типа необходимо отменить регистрацию.
			if (_obj.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Registered &&
			    _obj.DocumentKind.NumberingType != Sungero.Docflow.DocumentKind.NumberingType.Numerable ||
			    _obj.RegistrationState == Sungero.Docflow.OfficialDocument.RegistrationState.Reserved)
			{
				// Используем диалоги, чтобы хинт не пробрасывался в задачу, в которую он вложен.
				Dialogs.ShowMessage(SimpleDocuments.Resources.NeedCancelRegistration,
				                    MessageType.Error);
				return;
			}
//			else if (  Sungero.Docflow.Functions.SimpleDocument.Remote.AnyApprovalTasksWithCurrentDocument(_obj))
//			{
//				// Для смены типа необходимо остановить все активные задачи согласования по регламенту.
//				Dialogs.ShowMessage(SimpleDocuments.Resources.NeedAbortApproval,
//				                    MessageType.Error);
//				return;
//			}
			else
			{
				var types = new List<Sungero.Domain.Shared.IEntityInfo>();
				types.Add(Sungero.Docflow.Addendums.Info);
				types.Add(Sungero.Docflow.Memos.Info);
				types.Add(Sungero.Meetings.Minuteses.Info);
				types.Add(Sungero.Contracts.Contracts.Info);
				types.Add(Sungero.FinancialArchive.ContractStatements.Info);
				types.Add(Sungero.Contracts.IncomingInvoices.Info);
				types.Add(Sungero.Contracts.SupAgreements.Info);
				types.Add(Sungero.Projects.ProjectDocuments.Info);
				types.Add(Sungero.RecordManagement.IncomingLetters.Info);
				types.Add(Sungero.RecordManagement.OutgoingLetters.Info);
				types.Add(Sungero.RecordManagement.Orders.Info);
				types.Add(Sungero.RecordManagement.CompanyDirectives.Info);
				types.Add(Sungero.Docflow.CounterpartyDocuments.Info);
				
				syscon.SC.Module.Docflow.Functions.Module.ChangeDocumentType(_obj, types, e);
				//base.ChangeDocumentType(e);
			}
		}

		public override bool CanChangeDocumentType(Sungero.Domain.Client.CanExecuteActionArgs e)
		{
			return base.CanChangeDocumentType(e);
		}

	}

}